{-# OPTIONS --without-K #-}
module PfProp where

open import Agda.Primitive
private
  variable
    i j k l : Level
    A B C D : Set i

open import Lib
open ≡-Reasoning

isPropd : {D : Set i} → (D → Set j) → Set (i ⊔ j)
isPropd {D = D} A = {d : D} → (a₀ a₁ : A d) → a₀ ≡ a₁

isProp : Set i → Set i
isProp A = isPropd {D = ⊤} (λ _ → A)

isPfPropd : {D : Set i} → (D → Set j) → Set (i ⊔ j)
isPfPropd {D = D} A =
  _≡_ {A = (d : D) → A d → A d → A d} (λ _ a a' → a) (λ _ a a' → a')

isPfProp : Set i → Set i
isPfProp A = isPfPropd {D = ⊤} (λ _ → A)
-- isPfProp A = _≡_ {A = A → A → A} (λ a a' → a) (λ a a' → a')

toPropd : {D : Set i}{A : D → Set j} → isPfPropd A → isPropd A
toPropd Aₚ = λ a₀ a₁ → cong (λ z → z _ a₀ a₁) Aₚ
-- toProp Aₚ {d} a₀ a₁ = cong (λ z → z d a₀ a₁) Aₚ

toProp : {A : Set j} → isPfProp A → isProp A
toProp Aₚ = toPropd {D = ⊤} Aₚ

module CwFₛ where

  Conₛ : (i : Level) → Set (lsuc i)
  Conₛ i = Set i

  variable
    Γₛ Δₛ Θₛ : Conₛ i

  Subₛ : Conₛ i → Conₛ j → Set (i ⊔ j)
  Subₛ Δₛ Γₛ = Δₛ → Γₛ

  variable
    γₛ δₛ : Subₛ Δₛ Γₛ

  idₛ : {Γₛ : Conₛ i} → Subₛ Γₛ Γₛ
  idₛ = λ x → x

  infixl 6 _∘ₛ_
  _∘ₛ_ : (γₛ : Subₛ Δₛ Γₛ) → (δₛ : Subₛ Θₛ Δₛ) → Subₛ Θₛ Γₛ
  _∘ₛ_ = λ γₛ δₛ θₛ → γₛ (δₛ θₛ)

  Tyₛ : Conₛ i → (j : Level) → Set (i ⊔ lsuc j)
  Tyₛ Γₛ j = Γₛ → Set j

  variable
    Aₛ Bₛ : Tyₛ Γₛ i

  _[_]Tₛ : {Γₛ : Set k} (Aₛ : Tyₛ Γₛ i) → (γₛ : Subₛ Δₛ Γₛ) → Tyₛ Δₛ i
  Aₛ [ γₛ ]Tₛ = Aₛ ∘ γₛ

  infixl 5 _▹ₛ_
  _▹ₛ_ : (Γₛ : Conₛ i) → (Aₛ : Tyₛ Γₛ j) → Conₛ (i ⊔ j)
  Γₛ ▹ₛ Aₛ = Σ Γₛ Aₛ

  Tyₚ : Conₛ i → (j : Level) → Set (i ⊔ lsuc j)
  Tyₚ Γ j = Σ (Tyₛ Γ j) isPfPropd

  _^ₛ : (γₛ : Subₛ Δₛ Γₛ) → Subₛ (Δₛ ▹ₛ Aₛ [ γₛ ]Tₛ) (Γₛ ▹ₛ Aₛ)
  (γₛ ^ₛ) (δₛ , aₛ) = γₛ δₛ , aₛ


open CwFₛ

-- -- _[_]Tₚ∙ : {Γ : Set i}{Δ : Set j}{A : Γ → Set k} → isPfPropd A → (γ : Δ → Γ) → isPfPropd (A ∘ γ)
-- -- A∙ [ γ ]Tₚ∙ = cong (λ z → z ∘ γ) A∙
_[_]Tₚ : isPfPropd {D = Γₛ} Aₛ → (γₛ : Subₛ Δₛ Γₛ) → isPfPropd {D = Δₛ} (Aₛ [ γₛ ]Tₛ)
Aₚ [ γ ]Tₚ = cong (_∘ γ) Aₚ

[id]Tₚ : {Aₚ : isPfPropd Aₛ} → Aₚ [ id ]Tₚ ≡ Aₚ
[id]Tₚ {Aₚ = Aₚ} = cong-id Aₚ

[id]T'ₚ : {Aₚ : isPfPropd Aₛ} → Aₚ ≡ Aₚ [ id ]Tₚ
[id]T'ₚ {Aₚ = Aₚ} = cong-id' Aₚ

[][]Tₚ : {Aₚ : isPfPropd Aₛ} → Aₚ [ γₛ ]Tₚ [ δₛ ]Tₚ ≡ Aₚ [ γₛ ∘ δₛ ]Tₚ
[][]Tₚ {γₛ = γₛ} {δₛ = δₛ} {Aₚ = Aₚ} = cong-cong {f = λ z d → z (δₛ d)} {g = λ z y → z (γₛ y)} Aₚ
-- [][]Tₚ {Aₚ = Aₚ} = cong-cong Aₚ

[∘]Tₚ : {Aₚ : isPfPropd Aₛ} → Aₚ [ γₛ ∘ δₛ ]Tₚ ≡ Aₚ [ γₛ ]Tₚ [ δₛ ]Tₚ
[∘]Tₚ {Aₚ = Aₚ} = cong-∘ Aₚ

--

⊤ₚ : isPfProp ⊤
⊤ₚ = refl

-- Chapter #3 ------------------------------------------------------------------

{- === Proposition 1. === -}
×ₚ : (Aₚ : isPfProp A) → (Bₚ : isPfProp B) → isPfProp (A × B)
-- ×ₚ Aₚ Bₚ = trans
--   (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → z _ a₀ a₁ , b₀ }) Aₚ)
--   (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → a₁ , z _ b₀ b₁ }) Bₚ)
×ₚ Aₚ Bₚ =
  (λ { d (a₀ , b₀) (a₁ , b₁) → (a₀ , b₀) })
    ≡⟨ (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → z _ a₀ a₁ , b₀ }) Aₚ) ⟩
  (λ { d (a₀ , b₀) (a₁ , b₁) → (a₁ , b₀) })
    ≡⟨ (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → a₁ , z _ b₀ b₁ }) Bₚ) ⟩
  (λ { d (a₀ , b₀) (a₁ , b₁) → (a₁ , b₁) })
    ∎

-- Have :
--   ∙ λ a₀ a₁ → a₀ = λ a₀ a₁ → a₁
--   ∙ λ b₀ b₁ → b₀ = λ b₀ b₁ → b₁
-- Need :
--   ∙ λ ab₀ ab₁ → ab₀ = λ ab₀ ab₁ → ab₁

-- λ a₀b₀ a₁b₁ → a₀b₀ ≡⟨Aₚ⟩
-- λ a₀b₀ a₁b₁ → a₁b₀ ≡⟨Bₚ⟩
-- λ a₀b₀ a₁b₁ → a₁b₁

Σₚ : {A : Set i}{B : A → Set j} →
     (Aₚ : isPfProp A) → (Bₚ : isPfPropd B) → isPfProp (Σ A B)
Σₚ {A = A} {B = B} Aₚ Bₚ =
  let
    aₚ : (a₀ a₁ : A) → a₀ ≡ a₁
    aₚ = toProp Aₚ
  in
  (λ { d (a₀ , b₀) (a₁ , b₁) → (a₀ , b₀) })
    ≡⟨ (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → a₀ , z _ b₀ (subst B (aₚ a₀ a₀) b₀) }) Bₚ) ⟩
  (λ { d (a₀ , b₀) (a₁ , b₁) → (a₀ , subst B (aₚ a₀ a₀) b₀) })
    ≡⟨ (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → z _ a₀ a₁ , subst B (aₚ a₀ (z _ a₀ a₁)) b₀ }) Aₚ) ⟩
  (λ { d (a₀ , b₀) (a₁ , b₁) → (a₁ , subst B (aₚ a₀ a₁) b₀) })
    ≡⟨ (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → a₁ , z _ (subst B (aₚ a₀ a₁) b₀) b₁ }) Bₚ) ⟩
  (λ { d (a₀ , b₀) (a₁ , b₁) → (a₁ , b₁) })
    ∎


{- === Proposition 2. === -}
Σdₚ : {A : D → Set i}{B : Σ D A → Set j} →
      (Aₚ : isPfPropd A) → (Bₚ : isPfPropd {D = Σ D A} B) →
      isPfPropd (λ d → (Σ (A d) (λ a → B (d , a))))
Σdₚ {D = D} {A = A} {B = B} Aₚ Bₚ =
  let
    daₚ : {d : D} → (a₀ a₁ : A d) → (d , a₀) ≡ (d , a₁)
    daₚ {d} a₀ a₁ = cong (d ,_) (toPropd Aₚ a₀ a₁)
  in
  (λ { d (a₀ , b₀) (a₁ , b₁) → (a₀ , b₀) })
    ≡⟨ (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → a₀ , z _ b₀ (subst B (daₚ a₀ a₀) b₀) }) Bₚ) ⟩
  (λ { d (a₀ , b₀) (a₁ , b₁) → (a₀ , subst B (daₚ a₀ a₀) b₀) })
    ≡⟨ (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → z _ a₀ a₁ , subst B (daₚ a₀ (z _ a₀ a₁)) b₀ }) Aₚ) ⟩
  (λ { d (a₀ , b₀) (a₁ , b₁) → (a₁ , subst B (daₚ a₀ a₁) b₀) })
    ≡⟨ (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → a₁ , z _ (subst B (daₚ a₀ a₁) b₀) b₁ }) Bₚ) ⟩
  (λ { d (a₀ , b₀) (a₁ , b₁) → (a₁ , b₁) })
    ∎

-- Should `Tyₚ` be used?
Σdₚ' : (A@(Aₛ , Aₚ) : Tyₚ Γₛ i)(B@(Bₛ , Bₚ) : Tyₚ (Γₛ ▹ₛ Aₛ) j) →
      isPfPropd (λ γ → (Σ (Aₛ γ) (λ a → Bₛ (γ , a))))
Σdₚ' {Γₛ = Γₛ} (Aₛ , Aₚ) (Bₛ , Bₚ) =
  let
    daₚ : {γ : Γₛ} → (a₀ a₁ : Aₛ γ) → (γ , a₀) ≡ (γ , a₁)
    daₚ {γ} a₀ a₁ = cong (γ ,_) (toPropd Aₚ a₀ a₁)
  in
  (λ { _ (a₀ , b₀) (a₁ , b₁) → (a₀ , b₀) })
    ≡⟨ (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → a₀ , z _ b₀ (subst Bₛ (daₚ a₀ a₀) b₀) }) Bₚ) ⟩
  (λ { _ (a₀ , b₀) (a₁ , b₁) → (a₀ , subst Bₛ (daₚ a₀ a₀) b₀) })
    ≡⟨ (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → z _ a₀ a₁ , subst Bₛ (daₚ a₀ (z _ a₀ a₁)) b₀ }) Aₚ) ⟩
  (λ { _ (a₀ , b₀) (a₁ , b₁) → (a₁ , subst Bₛ (daₚ a₀ a₁) b₀) })
    ≡⟨ (cong (λ { z _ (a₀ , b₀) (a₁ , b₁) → a₁ , z _ (subst Bₛ (daₚ a₀ a₁) b₀) b₁ }) Bₚ) ⟩
  (λ { _ (a₀ , b₀) (a₁ , b₁) → (a₁ , b₁) })
    ∎

      -- (Aₚ : isPfPropd A) → (Bₚ : isPfPropd {D = Σ D A} B) →
-- Σₚ : {A : D → Set i}{B : (d : D) → A d → Set j} →
--      (Aₚ : isPfPropd A) → (Bₚ : isPfPropd {D = Σ D A} (λ { (d , a) → B d a })) → isPfProp (Σ D (λ d → (Σ (A d) (B d))))
--      (Aₚ : isPfPropd A) → (Bₚ : isPfPropd {D = Σ D A} (λ { (d , a) → B d a })) → isPfProp ((d : D) → (Σ (A d) (B d)))
  -- (λ { d₀ ab₀ ab₁ → (λ d₁ → π₁ (ab₀ d₁) , π₂ (ab₀ d₁)) })

-- Σd[]ₚ : {A@(Aₛ , Aₚ) : Tyₚ Γₛ i}{B@(Bₛ , Bₚ) : Tyₚ (Γₛ ▹ₛ Aₛ) j} →
--         {γₛ : Subₛ Δₛ Γₛ} →
--         (Σdₚ A B) [ γₛ ]Tₚ ≡ {! Σdₚ (Aₚ [ γₛ ]Tₚ) ?  !}
-- Σd[]ₚ = {!   !}

-- Σd[]ₚ : {Aₛ : Γₛ → Set i}     {Bₛ : Σ Γₛ Aₛ → Set j} →
--         (Aₚ : isPfPropd Aₛ) → (Bₚ : isPfPropd {D = Σ Γₛ Aₛ} Bₛ) →
--         {γₛ : Subₛ Δₛ Γₛ} →
--         (Σdₚ Aₚ Bₚ) [ γₛ ]Tₚ ≡ Σdₚ (Aₚ [ γₛ ]Tₚ) (Bₚ [ γₛ ^ₛ ]Tₚ)
-- Σd[]ₚ Aₚ Bₚ = {!   !}


{- === Corollary 5. === -}
Πₚ : {A : Set i}{B : A → Set j} → (Bₚ : isPfPropd B) → isPfProp ((a : A) → B a)
Πₚ Bₚ = cong (λ z _ f₀ f₁ a → z a (f₀ a) (f₁ a)) Bₚ

Πiₚ : {A : Set i}{B : A → Set j} → (Bₚ : isPfPropd B) → isPfProp ({a : A} → B a)
Πiₚ Bₚ = cong (λ z _ f₀ f₁ {a} → z a (f₀ {a}) (f₁ {a})) Bₚ

-- Πdₚ : {A : D → Set i}{B : (d : D) → A d → Set j} →
--       (Bₚ : isPfPropd {D = Σ D A} (λ { (d , a) → B d a })) →
--       isPfProp ((d : D) → (a : A d) → B d a)
-- Πdₚ Bₚ = cong (λ z _ f₀ f₁ d a → z (d , a) (f₀ d a) (f₁ d a)) Bₚ

Πdₚ : {A : D → Set i}{B : (d : D) → A d → Set j} →
      (Bₚ : isPfPropd {D = Σ D A} (λ { (d , a) → B d a })) →
      isPfPropd (λ d → (a : A d) → B d a)
Πdₚ Bₚ = cong (λ z d f₀ f₁ a → z (d , a) (f₀ a) (f₁ a)) Bₚ

Πdiₚ : {A : D → Set i}{B : (d : D) → A d → Set j} →
      (Bₚ : isPfPropd {D = Σ D A} (λ { (d , a) → B d a })) →
      isPfPropd (λ d → {a : A d} → B d a)
Πdiₚ Bₚ = cong (λ z d f₀ f₁ {a} → z (d , a) (f₀ {a}) (f₁ {a})) Bₚ
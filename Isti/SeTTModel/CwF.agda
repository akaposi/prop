{-# OPTIONS --without-K #-}
module SeTTModel.CwF where

open import Agda.Primitive
open import Lib hiding (id ; _∘_) renaming (_,_ to _,ₗ_)
open ≡-Reasoning

open import PfProp

variable
  i j k l m n : Level

module Category where

  module Conᵀ (i : Level) where
    ∣Γ∣ᵀ : Set (lsuc i)
    ∣Γ∣ᵀ = Set i

    Γ~ᵀ : ∣Γ∣ᵀ → Set (lsuc i)
    Γ~ᵀ ∣Γ∣ = ∣Γ∣ × ∣Γ∣ → Set i

    ~pᵀ : (∣Γ∣ : ∣Γ∣ᵀ) → Γ~ᵀ ∣Γ∣ → Set i
    ~pᵀ ∣Γ∣ Γ~ = isPfPropd Γ~

    RΓᵀ : (∣Γ∣ : ∣Γ∣ᵀ) → Γ~ᵀ ∣Γ∣ → Set i
    RΓᵀ ∣Γ∣ Γ~ = (γ : ∣Γ∣) → Γ~ (γ ,ₗ γ)

    SΓᵀ : (∣Γ∣ : ∣Γ∣ᵀ) → Γ~ᵀ ∣Γ∣ → Set i
    SΓᵀ ∣Γ∣ Γ~ = {γ₀ γ₁ : ∣Γ∣} → Γ~ (γ₀ ,ₗ γ₁) → Γ~ (γ₁ ,ₗ γ₀)

    TΓᵀ : (∣Γ∣ : ∣Γ∣ᵀ) → Γ~ᵀ ∣Γ∣ → Set i
    TΓᵀ ∣Γ∣ Γ~ = {γ₀ γ₁ γ₂ : ∣Γ∣} → Γ~ (γ₀ ,ₗ γ₁) → Γ~ (γ₁ ,ₗ γ₂) → Γ~ (γ₀ ,ₗ γ₂)
  open Conᵀ

  record Con i : Set (lsuc i) where
    constructor mkCon
    field
      ∣Γ∣ : ∣Γ∣ᵀ i
      Γ~  : Γ~ᵀ i ∣Γ∣
      ~p  : ~pᵀ i ∣Γ∣ Γ~
      RΓ  : RΓᵀ i ∣Γ∣ Γ~
      SΓ  : SΓᵀ i ∣Γ∣ Γ~
      TΓ  : TΓᵀ i ∣Γ∣ Γ~
  open Con public renaming
    ( ∣Γ∣ to infix 4 ∣_∣
    ; Γ~  to infix 5 _~
    ; RΓ  to R
    ; SΓ  to S
    ; TΓ  to T
    )

  Con≡ :
    {∣Γ₀∣ ∣Γ₁∣ : ∣Γ∣ᵀ i} →
    (∣Γ₍₀₁₎∣ : ∣Γ₀∣ ≡ ∣Γ₁∣) →

    {Γ₀~ : Γ~ᵀ i ∣Γ₀∣}{Γ₁~ : Γ~ᵀ i ∣Γ₁∣} →
    (Γ₍₀₁₎~ : _≡_ {A = Γ~ᵀ i ∣Γ₁∣} (subst (Γ~ᵀ i) ∣Γ₍₀₁₎∣ Γ₀~) Γ₁~) →

    {~p₀ : ~pᵀ i ∣Γ₀∣ Γ₀~}{~p₁ : ~pᵀ i ∣Γ₁∣ Γ₁~} →
    (~p₍₀₁₎ : _≡_ {A = ~pᵀ i ∣Γ₁∣ Γ₁~} (subst2 (~pᵀ i) ∣Γ₍₀₁₎∣ Γ₍₀₁₎~ ~p₀) ~p₁) →

    {RΓ₀ : RΓᵀ i ∣Γ₀∣ Γ₀~}{RΓ₁ : RΓᵀ i ∣Γ₁∣ Γ₁~} →
    (RΓ₍₀₁₎ : _≡_ {A = RΓᵀ i ∣Γ₁∣ Γ₁~} (subst2 (RΓᵀ i) ∣Γ₍₀₁₎∣ Γ₍₀₁₎~ RΓ₀) RΓ₁) →

    {SΓ₀ : SΓᵀ i ∣Γ₀∣ Γ₀~}{SΓ₁ : SΓᵀ i ∣Γ₁∣ Γ₁~} →
    (SΓ₍₀₁₎ : _≡_ {A = SΓᵀ i ∣Γ₁∣ Γ₁~} (subst2 (SΓᵀ i) ∣Γ₍₀₁₎∣ Γ₍₀₁₎~ SΓ₀) SΓ₁) →

    {TΓ₀ : TΓᵀ i ∣Γ₀∣ Γ₀~}{TΓ₁ : TΓᵀ i ∣Γ₁∣ Γ₁~} →
    (TΓ₍₀₁₎ : _≡_ {A = TΓᵀ i ∣Γ₁∣ Γ₁~} (subst2 (TΓᵀ i) ∣Γ₍₀₁₎∣ Γ₍₀₁₎~ TΓ₀) TΓ₁) →

    _≡_ {A = Con i}
      (mkCon ∣Γ₀∣ Γ₀~ ~p₀ RΓ₀ SΓ₀ TΓ₀)
      (mkCon ∣Γ₁∣ Γ₁~ ~p₁ RΓ₁ SΓ₁ TΓ₁)
  Con≡ refl refl refl refl refl refl = refl

  Con≡' :
    {∣Γ₀∣ ∣Γ₁∣ : ∣Γ∣ᵀ i} →
    (∣Γ₍₀₁₎∣ : ∣Γ₀∣ ≡ ∣Γ₁∣) →

    {Γ₀~ : Γ~ᵀ i ∣Γ₀∣}{Γ₁~ : Γ~ᵀ i ∣Γ₁∣} →
    (Γ₍₀₁₎~ : _≡_ {A = Γ~ᵀ i ∣Γ₁∣} (subst (Γ~ᵀ i) ∣Γ₍₀₁₎∣ Γ₀~) Γ₁~) →

    {~p₀ : ~pᵀ i ∣Γ₀∣ Γ₀~}{~p₁ : ~pᵀ i ∣Γ₁∣ Γ₁~} →
    (~p₍₀₁₎ : _≡_ {A = ~pᵀ i ∣Γ₁∣ Γ₁~} (subst2 (~pᵀ i) ∣Γ₍₀₁₎∣ Γ₍₀₁₎~ ~p₀) ~p₁) →

    {RΓ₀ : RΓᵀ i ∣Γ₀∣ Γ₀~}{RΓ₁ : RΓᵀ i ∣Γ₁∣ Γ₁~} →
    {SΓ₀ : SΓᵀ i ∣Γ₀∣ Γ₀~}{SΓ₁ : SΓᵀ i ∣Γ₁∣ Γ₁~} →
    {TΓ₀ : TΓᵀ i ∣Γ₀∣ Γ₀~}{TΓ₁ : TΓᵀ i ∣Γ₁∣ Γ₁~} →

    _≡_ {A = Con i}
      (mkCon ∣Γ₀∣ Γ₀~ ~p₀ RΓ₀ SΓ₀ TΓ₀)
      (mkCon ∣Γ₁∣ Γ₁~ ~p₁ RΓ₁ SΓ₁ TΓ₁)
  Con≡' refl refl {~p₀ = ~p} refl = Con≡ refl refl refl
    (toProp (Πₚ $ cong (λ z _ r₀ r₁ → z _ r₀ r₁) ~p) _ _)
    (toProp (Πdiₚ $ Πdiₚ $ Πdₚ $ cong (λ z _ s₀ s₁ → z _ s₀ s₁) ~p) _ _)
    (toProp (Πdiₚ $ Πdiₚ $ Πdiₚ $ Πdₚ $  Πdₚ $ cong (λ z _ t₀ t₁ → z _ t₀ t₁) ~p) _ _)

  variable
    Γ Δ Θ Ω Ξ : Con i

  module Subᵀ (Δ : Con i) (Γ : Con j) where
    ∣γ∣ᵀ : Set (i ⊔ j)
    ∣γ∣ᵀ = (δₓ : ∣ Δ ∣) → ∣ Γ ∣

    γ~ᵀ : ∣γ∣ᵀ → Set (i ⊔ j)
    γ~ᵀ ∣γ∣ = {δ₀ δ₁ : ∣ Δ ∣} → (δ₍₀₁₎ : (Δ ~) (δ₀ ,ₗ δ₁)) → (Γ ~) (∣γ∣ δ₀ ,ₗ ∣γ∣ δ₁)
  open Subᵀ

  record Sub (Δ : Con i) (Γ : Con j) : Set (i ⊔ j) where
    constructor mkSub
    field
      ∣γ∣ : ∣γ∣ᵀ Δ Γ
      γ~  : γ~ᵀ  Δ Γ ∣γ∣
  open Sub public renaming
    ( ∣γ∣ to infix 4 ∣_∣
    ; γ~  to infix 5 _~
    )

  Sub≡ : {∣γ₀∣ ∣γ₁∣ : ∣γ∣ᵀ Δ Γ} →
         (∣γ₍₀₁₎∣ : ∣γ₀∣ ≡ ∣γ₁∣) →
         {γ₀~ : γ~ᵀ Δ Γ ∣γ₀∣}{γ₁~ : γ~ᵀ Δ Γ ∣γ₁∣} →
         (γ₍₀₁₎~ : _≡_ {A = γ~ᵀ Δ Γ ∣γ₁∣} (subst (γ~ᵀ Δ Γ) ∣γ₍₀₁₎∣ γ₀~) γ₁~) →
         _≡_ {A = Sub Δ Γ} (mkSub ∣γ₀∣ γ₀~) (mkSub ∣γ₁∣ γ₁~)
  Sub≡ refl refl = refl

  Sub≡' : {∣γ₀∣ ∣γ₁∣ : ∣γ∣ᵀ Δ Γ} →
          (∣γ₍₀₁₎∣ : ∣γ₀∣ ≡ ∣γ₁∣) →
          {γ₀~ : γ~ᵀ Δ Γ ∣γ₀∣}{γ₁~ : γ~ᵀ Δ Γ ∣γ₁∣} →
          _≡_ {A = Sub Δ Γ} (mkSub ∣γ₀∣ γ₀~) (mkSub ∣γ₁∣ γ₁~)
  Sub≡' {Γ = Γ} refl = Sub≡ refl
    (toProp (Πiₚ $ Πdiₚ $ Πdₚ $ cong (λ z _ γ₍₀₁₎₀ γ₍₀₁₎₁ → z _ γ₍₀₁₎₀ γ₍₀₁₎₁) (~p Γ)) _ _)

  variable
    σ δ γ ν : Sub Δ Γ

  infixl 6 _∘_
  _∘_ : (γ : Sub Δ Γ) → (δ : Sub Θ Δ) → Sub Θ Γ
  ∣ σ ∘ δ ∣ γ     = ∣ σ ∣ (∣ δ ∣ γ)
  (σ ∘ δ ~) γ₍₀₁₎ = (σ ~) ((δ ~) γ₍₀₁₎)

  id : Sub Γ Γ
  ∣ id ∣ γ     = γ
  (id ~) γ₍₀₁₎ = γ₍₀₁₎

  ass : (λ {Γ : Con i}{Δ : Con j}{Θ : Con k}{Ω : Con l}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ω Θ} → (γ ∘ δ) ∘ θ) ≡
        (λ {Γ : Con i}{Δ : Con j}{Θ : Con k}{Ω : Con l}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ω Θ} → γ ∘ (δ ∘ θ))
  ass = refl

  -- ass : (σ ∘ δ) ∘ γ ≡ σ ∘ (δ ∘ γ)
  -- ass = refl

  idl : (λ {Γ : Con i}{Δ : Con j}{γ : Sub Δ Γ} → id ∘ γ) ≡
        (λ {Γ : Con i}{Δ : Con j}{γ : Sub Δ Γ} → γ)
  idl = refl

  -- idl : id ∘ σ ≡ σ
  -- idl = refl

  idr : (λ {Γ : Con i}{Δ : Con j}{γ : Sub Δ Γ} → γ ∘ id) ≡
        (λ {Γ : Con i}{Δ : Con j}{γ : Sub Δ Γ} → γ)
  idr = refl

  -- idr : σ ∘ id ≡ σ
  -- idr = refl

module TerminalObject where
  open Category

  ◆ : Con lzero
  ∣ ◆ ∣     = ⊤
  (◆ ~) _   = ⊤
  ~p ◆      = refl
  R ◆ _     = tt
  S ◆ _     = tt
  T ◆ _ _   = tt

  ε : Sub Γ ◆
  ∣ ε ∣ _ = tt
  (ε ~) _ = tt

  ◆η : (λ {Γ : Con i}{σ : Sub Γ ◆} → σ) ≡ (λ {Γ : Con i}{σ : Sub Γ ◆} → ε)
  ◆η = refl

  -- ◆η : {σ : Sub Γ ◆} → σ ≡ ε
  -- ◆η = refl

module Families where
  open Category

  module Tyᵀ (Γ : Con i) (j : Level) where
    ∣A∣ᵀ : Set (i ⊔ lsuc j)
    ∣A∣ᵀ = (γₓ : ∣ Γ ∣) → Set j

    A×Aᵀ : ∣A∣ᵀ → Set (i ⊔ j)
    A×Aᵀ ∣A∣ = (Σ ∣ Γ ∣ (λ γ₀ → Σ ∣ Γ ∣ (λ γ₁ → Σ ((Γ ~) (γ₀ ,ₗ γ₁)) (λ γ₀₁ → ∣A∣ γ₀ × ∣A∣ γ₁))))

    A~ᵀ : ∣A∣ᵀ → Set (i ⊔ lsuc j)
    A~ᵀ ∣A∣ = A×Aᵀ ∣A∣ → Set j

    ~pᵀ : (∣A∣ : ∣A∣ᵀ) → A~ᵀ ∣A∣ → Set (i ⊔ j)
    ~pᵀ ∣A∣ A~ = isPfPropd A~

    RAᵀ : (∣A∣ : ∣A∣ᵀ) → A~ᵀ ∣A∣ → Set (i ⊔ j)
    RAᵀ ∣A∣ A~ = {γₓ : ∣ Γ ∣} → (a : ∣A∣ γₓ) → A~ (γₓ ,ₗ γₓ ,ₗ R Γ γₓ ,ₗ a ,ₗ a)

    SAᵀ : (∣A∣ : ∣A∣ᵀ) → A~ᵀ ∣A∣ → Set (i ⊔ j)
    SAᵀ ∣A∣ A~ = {γ₀ γ₁ : ∣ Γ ∣}{γ₍₀₁₎ : (Γ ~) (γ₀ ,ₗ γ₁)}{a₀ : ∣A∣ γ₀}{a₁ : ∣A∣ γ₁} →
                 (a₍₀₁₎ : A~ (_ ,ₗ _ ,ₗ γ₍₀₁₎ ,ₗ a₀ ,ₗ a₁)) → A~ (_ ,ₗ _ ,ₗ S Γ γ₍₀₁₎ ,ₗ a₁ ,ₗ a₀)

    TAᵀ : (∣A∣ : ∣A∣ᵀ) → A~ᵀ ∣A∣ → Set (i ⊔ j)
    TAᵀ ∣A∣ A~ = {γ₀ γ₁ γ₂ : ∣ Γ ∣}{γ₍₀₁₎ : (Γ ~) (γ₀ ,ₗ γ₁)}{γ₍₁₂₎ : (Γ ~) (γ₁ ,ₗ γ₂)} →
                 {a₀ : ∣A∣ γ₀}{a₁ : ∣A∣ γ₁}{a₂ : ∣A∣ γ₂} →
                 (a₍₀₁₎ : A~ (_ ,ₗ _ ,ₗ γ₍₀₁₎ ,ₗ a₀ ,ₗ a₁)) →
                 (a₍₁₂₎ : A~ (_ ,ₗ _ ,ₗ γ₍₁₂₎ ,ₗ a₁ ,ₗ a₂)) →
                 A~ (_ ,ₗ _ ,ₗ (T Γ) γ₍₀₁₎ γ₍₁₂₎ ,ₗ a₀ ,ₗ a₂)

    coeAᵀ : ∣A∣ᵀ → Set (i ⊔ j)
    coeAᵀ ∣A∣ = {γ₀ γ₁ : ∣ Γ ∣} → (γ₍₀₁₎ : (Γ ~) (γ₀ ,ₗ γ₁)) → (a₀ : ∣A∣ γ₀) → ∣A∣ γ₁

    cohAᵀ : (∣A∣ : ∣A∣ᵀ) → A~ᵀ ∣A∣ → (coeA : coeAᵀ ∣A∣) → Set (i ⊔ j)
    cohAᵀ ∣A∣ A~ coeA = {γ₀ γ₁ : ∣ Γ ∣} → (γ₍₀₁₎ : (Γ ~) (γ₀ ,ₗ γ₁)) →
                        (a₀ : ∣A∣ γ₀) → (A~ (γ₀ ,ₗ γ₁ ,ₗ γ₍₀₁₎ ,ₗ a₀ ,ₗ coeA γ₍₀₁₎ a₀))

  open Tyᵀ

  record Ty (Γ : Con i) (j : Level) : Set (i ⊔ lsuc j) where -- TODO: Set (lsuc (i ⊔ j))?
    constructor mkTy
    field
      ∣A∣  : ∣A∣ᵀ Γ j
      A~   : A~ᵀ Γ j ∣A∣
      ~p   : ~pᵀ Γ j ∣A∣ A~
      RA   : RAᵀ Γ j ∣A∣ A~
      SA   : SAᵀ Γ j ∣A∣ A~
      TA   : TAᵀ Γ j ∣A∣ A~
      coeA : coeAᵀ Γ j ∣A∣
      cohA : cohAᵀ Γ j ∣A∣ A~ coeA
  open Ty public renaming
    ( ∣A∣  to ∣_∣
    ; A~   to _~
    ; RA   to R
    ; SA   to S
    ; TA   to T
    ; coeA to coe
    ; cohA to coh
    )

  Ty≡ :
    {∣A₀∣ ∣A₁∣ : ∣A∣ᵀ Γ i} →
    (∣A₍₀₁₎∣ : ∣A₀∣ ≡ ∣A₁∣) →

    {A₀~ : A~ᵀ Γ i ∣A₀∣}{A₁~ : A~ᵀ Γ i ∣A₁∣} →
    (A₍₀₁₎~ : _≡_ {A = A~ᵀ Γ i ∣A₁∣} (subst (A~ᵀ Γ i) ∣A₍₀₁₎∣ A₀~) A₁~) →

    {~p₀ : ~pᵀ Γ i ∣A₀∣ A₀~}{~p₁ : ~pᵀ Γ i ∣A₁∣ A₁~} →
    (~p₍₀₁₎ : _≡_ {A = ~pᵀ Γ i ∣A₁∣ A₁~} (subst2 (~pᵀ Γ i) ∣A₍₀₁₎∣ A₍₀₁₎~ ~p₀) ~p₁) →

    {RA₀ : RAᵀ Γ i ∣A₀∣ A₀~}{RA₁ : RAᵀ Γ i ∣A₁∣ A₁~} →
    (RA₍₀₁₎ : _≡_ {A = RAᵀ Γ i ∣A₁∣ A₁~} (subst2 (RAᵀ Γ i) ∣A₍₀₁₎∣ A₍₀₁₎~ RA₀) RA₁) →

    {SA₀ : SAᵀ Γ i ∣A₀∣ A₀~}{SA₁ : SAᵀ Γ i ∣A₁∣ A₁~} →
    (SA₍₀₁₎ : _≡_ {A = SAᵀ Γ i ∣A₁∣ A₁~} (subst2 (SAᵀ Γ i) ∣A₍₀₁₎∣ A₍₀₁₎~ SA₀) SA₁) →

    {TA₀ : TAᵀ Γ i ∣A₀∣ A₀~}{TA₁ : TAᵀ Γ i ∣A₁∣ A₁~} →
    (TA₍₀₁₎ : _≡_ {A = TAᵀ Γ i ∣A₁∣ A₁~} (subst2 (TAᵀ Γ i) ∣A₍₀₁₎∣ A₍₀₁₎~ TA₀) TA₁) →

    {coeA₀ : coeAᵀ Γ i ∣A₀∣}{coeA₁ : coeAᵀ Γ i ∣A₁∣} →
    (coeA₍₀₁₎ : _≡_ {A = coeAᵀ Γ i ∣A₁∣} (subst (coeAᵀ Γ i) ∣A₍₀₁₎∣ coeA₀) coeA₁) →

    {cohA₀ : cohAᵀ Γ i ∣A₀∣ A₀~ coeA₀}{cohA₁ : cohAᵀ Γ i ∣A₁∣ A₁~ coeA₁} →
    (cohA₍₀₁₎ : _≡_ {A = cohAᵀ Γ i ∣A₁∣ A₁~ coeA₁} (subst2' (cohAᵀ Γ i) ∣A₍₀₁₎∣ A₍₀₁₎~ coeA₍₀₁₎ cohA₀) cohA₁) →

    _≡_ {A = Ty Γ i}
      (mkTy ∣A₀∣ A₀~ ~p₀ RA₀ SA₀ TA₀ coeA₀ cohA₀)
      (mkTy ∣A₁∣ A₁~ ~p₁ RA₁ SA₁ TA₁ coeA₁ cohA₁)
  Ty≡ refl refl refl refl refl refl refl refl = refl

  Ty≡' :
    {∣A₀∣ ∣A₁∣ : ∣A∣ᵀ Γ i} →
    (∣A₍₀₁₎∣ : ∣A₀∣ ≡ ∣A₁∣) →

    {A₀~ : A~ᵀ Γ i ∣A₀∣}{A₁~ : A~ᵀ Γ i ∣A₁∣} →
    (A₍₀₁₎~ : _≡_ {A = A~ᵀ Γ i ∣A₁∣} (subst (A~ᵀ Γ i) ∣A₍₀₁₎∣ A₀~) A₁~) →

    {~p₀ : ~pᵀ Γ i ∣A₀∣ A₀~}{~p₁ : ~pᵀ Γ i ∣A₁∣ A₁~} →
    (~p₍₀₁₎ : _≡_ {A = ~pᵀ Γ i ∣A₁∣ A₁~} (subst2 (~pᵀ Γ i) ∣A₍₀₁₎∣ A₍₀₁₎~ ~p₀) ~p₁) →

    {RA₀ : RAᵀ Γ i ∣A₀∣ A₀~}{RA₁ : RAᵀ Γ i ∣A₁∣ A₁~} →
    {SA₀ : SAᵀ Γ i ∣A₀∣ A₀~}{SA₁ : SAᵀ Γ i ∣A₁∣ A₁~} →
    {TA₀ : TAᵀ Γ i ∣A₀∣ A₀~}{TA₁ : TAᵀ Γ i ∣A₁∣ A₁~} →

    {coeA₀ : coeAᵀ Γ i ∣A₀∣}{coeA₁ : coeAᵀ Γ i ∣A₁∣} →
    (coeA₍₀₁₎ : _≡_ {A = coeAᵀ Γ i ∣A₁∣} (subst (coeAᵀ Γ i) ∣A₍₀₁₎∣ coeA₀) coeA₁) →

    {cohA₀ : cohAᵀ Γ i ∣A₀∣ A₀~ coeA₀}{cohA₁ : cohAᵀ Γ i ∣A₁∣ A₁~ coeA₁} →

    _≡_ {A = Ty Γ i}
      (mkTy ∣A₀∣ A₀~ ~p₀ RA₀ SA₀ TA₀ coeA₀ cohA₀)
      (mkTy ∣A₁∣ A₁~ ~p₁ RA₁ SA₁ TA₁ coeA₁ cohA₁)
  Ty≡' {Γ = Γ} refl refl {~p₀ = ~p} refl {RA₀} {RA₁} {SA₀} {SA₁} {TA₀} {TA₁} refl {cohA₀} {cohA₁} =
    Ty≡ refl refl refl
      -- (cong (λ z {γₓ} a → z _ (RA₀ {γₓ} a) (RA₁ {γₓ} a)) ~p)
      -- (cong (λ z {γₓ} a → z _ (RA₀ a) (RA₁ a)) ~p)
      -- (cong (λ z {γ₀} {γ₁} {a₀} {a₁} γ₍₀₁₎ a₍₀₁₎ → z _ (SA₀ γ₍₀₁₎ a₍₀₁₎) (SA₁ γ₍₀₁₎ a₍₀₁₎)) ~p)
      -- (cong (λ z {γ₀} {γ₁} {γ₂} {a₀} {a₁} {a₂} γ₍₀₁₎ γ₍₀₂₎ a₍₀₁₎ a₍₀₂₎ → z _ (TA₀ γ₍₀₁₎ γ₍₀₂₎ a₍₀₁₎ a₍₀₂₎) (TA₁ γ₍₀₁₎ γ₍₀₂₎ a₍₀₁₎ a₍₀₂₎)) ~p)
      -- refl
      -- (cong (λ z {γ₀} {γ₁} γ₍₀₁₎ a₀ → z _ (cohA₀ γ₍₀₁₎ a₀) (cohA₁ γ₍₀₁₎ a₀)) ~p)

      (toProp (Πiₚ $ Πdₚ $ cong (λ { z _ r₀ r₁ → z _ r₀ r₁ }) ~p) _ _)
      -- (toProp (Πiₚ $ Πdₚ $ cong (λ { z (d ,ₗ a) r₀ r₁ → z (d ,ₗ d ,ₗ (R Γ d ,ₗ (a ,ₗ a))) r₀ r₁ }) ~p) _ _)
      (toProp (Πiₚ $ Πdiₚ $ Πdiₚ $ Πdiₚ $ Πdiₚ $ Πdₚ (cong (λ z _ s₀ s₁ → z _ s₀ s₁) ~p)) _ _)
      -- (toProp (Πiₚ $ Πdiₚ $ Πdiₚ $ Πdiₚ $ Πdiₚ $ Πdiₚ $ Πdₚ $ Πdₚ (cong (λ z d t₀ t₁ a₍₀₁₎ a₍₁₂₎ → z _ (t₀ a₍₀₁₎ a₍₁₂₎) (t₁ a₍₀₁₎ a₍₁₂₎)) ~p)) _ _)
      (toProp (Πiₚ $ Πdiₚ $ Πdiₚ $ Πdiₚ $ Πdiₚ $ Πdiₚ $ Πdiₚ $ Πdiₚ $ Πdₚ $ Πdₚ (cong (λ z _ t₀ t₁ → z _ t₀ t₁) ~p)) _ _)
      refl
      (toProp (Πiₚ $ Πdiₚ $ Πdₚ $ Πdₚ $ cong (λ z _ h₀ h₁ → z _ h₀ h₁) ~p) _ _)

  variable
    A B C : Ty Γ i

  A×Aᵀ[] : (γ : Sub Δ Γ) → A×Aᵀ Δ i (∣ A ∣ CwFₛ.∘ₛ ∣ γ ∣) → A×Aᵀ Γ i ∣ A ∣
  A×Aᵀ[] γ (δ₀ ,ₗ δ₁ ,ₗ δ₍₀₁₎ ,ₗ a₀ ,ₗ a₁) = (∣ γ ∣ δ₀ ,ₗ ∣ γ ∣ δ₁ ,ₗ (γ ~) δ₍₀₁₎ ,ₗ a₀ ,ₗ a₁)

  R[] : {γ : Sub Δ Γ}{δₓ : ∣ Δ ∣} → R Γ (∣ γ ∣ δₓ) ≡ (γ ~) (R Δ δₓ)
  -- R[] {Δ = Δ} {Γ = Γ} {γ = γ} {δₓ = δₓ} = cong (λ x → x (∣ γ ∣ δₓ ,ₗ ∣ γ ∣ δₓ) (R Γ (∣ γ ∣ δₓ)) ((γ ~) (R Δ δₓ))) (~p Γ)
  R[] {Δ = Δ} {Γ = Γ} {γ} = cong (λ z → z _ (R Γ _) ((γ ~) (R Δ _))) (~p Γ)

  S[] : {γ : Sub Δ Γ}{δ₀ δ₁ : ∣ Δ ∣}{δ₍₀₁₎ : (Δ ~) (δ₀ ,ₗ δ₁)} → S Γ ((γ ~) δ₍₀₁₎) ≡ (γ ~) (S Δ δ₍₀₁₎)
  S[] {Δ = Δ} {Γ = Γ} {γ} {δ₍₀₁₎ = δ₍₀₁₎} = cong (λ z → z _ (S Γ ((γ ~) δ₍₀₁₎)) ((γ ~) (S Δ δ₍₀₁₎))) (~p Γ)

  T[] : {γ : Sub Δ Γ}{δ₀ δ₁ δ₂ : ∣ Δ ∣}{δ₍₀₁₎ : (Δ ~) (δ₀ ,ₗ δ₁)}{δ₍₁₂₎ : (Δ ~) (δ₁ ,ₗ δ₂)} →
        T Γ ((γ ~) δ₍₀₁₎) ((γ ~) δ₍₁₂₎) ≡ (γ ~) (T Δ δ₍₀₁₎ δ₍₁₂₎)
  T[] {Δ = Δ} {Γ = Γ} {γ} {δ₍₀₁₎ = δ₍₀₁₎} {δ₍₁₂₎ = δ₍₁₂₎} = cong (λ z → z _ (T Γ ((γ ~) δ₍₀₁₎) ((γ ~) δ₍₁₂₎)) ((γ ~) (T Δ δ₍₀₁₎ δ₍₁₂₎))) (~p Γ)

  _[_]T : (A : Ty Γ i) → (γ : Sub Δ Γ) → Ty Δ i
  ∣_∣ (A [ γ ]T) δₓ = ∣ A ∣ (∣ γ ∣ δₓ)
  _~ (A [ γ ]T) (δ₀ ,ₗ δ₁ ,ₗ δ₍₀₁₎ ,ₗ a₀ ,ₗ a₁) = (A ~) (∣ γ ∣ δ₀ ,ₗ ∣ γ ∣ δ₁ ,ₗ (γ ~) δ₍₀₁₎ ,ₗ a₀ ,ₗ a₁)
  ~p (A [ γ ]T) = (~p A) [ A×Aᵀ[] {A = A} γ ]Tₚ
  -- ~p (A [ γ ]T) = cong
  --   (λ { z (      δ₀ ,ₗ       δ₁ ,ₗ       δ₍₀₁₎ ,ₗ a₀ ,ₗ a₁) a~₀ a~₁ →
  --        z (∣ γ ∣ δ₀ ,ₗ ∣ γ ∣ δ₁ ,ₗ (γ ~) δ₍₀₁₎ ,ₗ a₀ ,ₗ a₁) a~₀ a~₁ }) (~p A)
  -- Have:
  -- (~p A) : λ a₀ a₁ → a₀ ≡{(d : Γ,ₗΓ,ₗ~,ₗA,ₗA) → A~ d → A~ d → A~ d} λ a₀ a₁ → a₁
  -- Need:
  -- ? : λ a₀ a₁ → a₀ ≡{(d : Δ,ₗΔ,ₗγ~,ₗA[γ],ₗA[γ]) → A[γ]~ d → A[γ]~ d → A[γ]~ d} λ a₀ a₁ → a₁
  -- R (_[_]T {Γ = Γ} A γ) {δₓ} a = {!  (R A a)  !}
  -- R (A [ γ ]T) {δₓ} a = subst (λ γ₍₀₁₎ → (A ~) (∣ γ ∣ δₓ ,ₗ ∣ γ ∣ δₓ ,ₗ γ₍₀₁₎ ,ₗ a ,ₗ a)) (R[] {γ = γ}) (R A a)
  R (A [ γ ]T) a = subst (λ γ₍₀₁₎ → (A ~) (_ ,ₗ _ ,ₗ γ₍₀₁₎ ,ₗ _ ,ₗ _)) (R[] {γ = γ}) (R A a)
  -- R (A [ γ ]T) a = subst (λ γ₍₀₁₎ → (A ~) (_ ,ₗ _ ,ₗ γ₍₀₁₎ ,ₗ _ ,ₗ _)) (R[] {γ = γ}) (R A a)
  -- R (_[_]T {Γ = Γ} A γ) {δₓ} a = {! subst (λ γ₍₀₁₎ → (A ~) (γₓ ,ₗ γₓ ,ₗ γ₍₀₁₎ ,ₗ a ,ₗ a)) (~p Γ) (R A a)  !}
  -- R (_[_]T {Γ = Γ} A γ) a = {! subst (λ γ₍₀₁₎ → (A ~) (γ ,ₗ γ ,ₗ γ₍₀₁₎ ,ₗ a ,ₗ a)) (~p Γ) (R A a)  !}
  -- R (A [ γ ]T) a = {! (R A a) !}
  -- R (A [ γ ]T) a = subst (λ { (γ₀ ,ₗ γ₁ ,ₗ γ) → (A ~) (γ₀ ,ₗ γ₁ ,ₗ γ₍₀₁₎ ,ₗ a ,ₗ a) }) {!   !} (R A a)
  -- R (A [ γ ]T) a = subst (λ { (γₓ ,ₗ γₓ ,ₗ γ) → (A ~) (γ₀ ,ₗ γ₁ ,ₗ γ₍₀₁₎ ,ₗ a ,ₗ a) }) {!   !} (R A a)
  S (A [ γ ]T) a₍₀₁₎ = subst (λ γ₍₀₁₎ → (A ~) (_ ,ₗ _ ,ₗ γ₍₀₁₎ ,ₗ _ ,ₗ _)) (S[] {γ = γ}) ((S A) a₍₀₁₎)
  -- S (A [ γ ]T) γ₍₀₁₎ a₍₀₁₎ = {!  S A _ a₍₀₁₎  !}
  T (A [ γ ]T) a₍₀₁₎ a₍₁₂₎ = subst (λ γ₍₀₁₎ → (A ~) (_ ,ₗ _ ,ₗ γ₍₀₁₎ ,ₗ _ ,ₗ _)) (T[] {γ = γ}) ((T A) a₍₀₁₎ a₍₁₂₎)
  coe (A [ γ ]T) δ₍₀₁₎ a₀ = coe A ((γ ~) δ₍₀₁₎) a₀
  coh (A [ γ ]T) δ₍₀₁₎ a₀ = coh A ((γ ~) δ₍₀₁₎) a₀

  [][]T : A [ γ ]T [ δ ]T ≡ A [ γ ∘ δ ]T
  [][]T = Ty≡' refl refl [][]Tₚ refl

  [∘]T : A [ γ ∘ δ ]T ≡ A [ γ ]T [ δ ]T
  [∘]T = Ty≡' refl refl [∘]Tₚ refl

  -- tmp : {A B : Set i}{b₀ b₁ : B} → (b₀ ≡ b₁) → (λ {a : A} → b₀) ≡ (λ {a : A} → b₁)
  -- tmp eq = cong (λ x → λ {a} → x) eq

  -- tmp : {A : Set i}{B : A → Set j}
  --       {f₀ f₁ : (a : A) → B a} →
  --       ({a : A} → f₀ a ≡ f₁ a) →
  --       (λ (a : A) → f₀ a) ≡ (λ (a : A) → f₁ a)
  -- tmp eq = {!   !}

  [id]T : A [ id ]T ≡ A
  [id]T = Ty≡' refl refl [id]Tₚ refl

  [id]T' : A ≡ A [ id ]T
  [id]T' = Ty≡' refl refl [id]T'ₚ refl

  -- [id]T : (λ (Γ : Con i)(A : Ty Γ j) → A [ id ]T) ≡ (λ (Γ : Con i)(A : Ty Γ j) → A)
  -- [id]T = cong (λ f → λ Γ A → {! f  !}) ([id]T') -- Ty≡' refl refl [id]Tₚ refl

  module Tmᵀ (Γ : Con i) (A : Ty Γ j) where
    ∣t∣ᵀ : Set (i ⊔ j)
    ∣t∣ᵀ = (γₓ : ∣ Γ ∣) → ∣ A ∣ γₓ

    t~ᵀ : ∣t∣ᵀ → Set (i ⊔ j)
    t~ᵀ ∣t∣ = {γ₀ γ₁ : ∣ Γ ∣} → (γ₍₀₁₎ : (Γ ~) (γ₀ ,ₗ γ₁)) → (A ~) (_ ,ₗ _ ,ₗ γ₍₀₁₎ ,ₗ (∣t∣ γ₀) ,ₗ (∣t∣ γ₁))
  open Tmᵀ

  record Tm (Γ : Con i) (A : Ty Γ j) : Set (i ⊔ j) where
    constructor mkTm
    field
      ∣t∣ : ∣t∣ᵀ Γ A
      t~  : t~ᵀ Γ A ∣t∣
  open Tm public renaming
    ( ∣t∣ to ∣_∣
    ; t~  to _~
    )

  Tm≡ : {∣t₀∣ ∣t₁∣ : ∣t∣ᵀ Γ A} →
        (∣t₍₀₁₎∣ : ∣t₀∣ ≡ ∣t₁∣) →
        {t₀~ : t~ᵀ Γ A ∣t₀∣}{t₁~ : t~ᵀ Γ A ∣t₁∣} →
        (t₍₀₁₎~ : _≡_ {A = t~ᵀ Γ A ∣t₁∣} (subst (t~ᵀ Γ A) ∣t₍₀₁₎∣ t₀~) t₁~) →
        _≡_ {A = Tm Γ A} (mkTm ∣t₀∣ t₀~) (mkTm ∣t₁∣ t₁~)
  Tm≡ refl refl = refl

  Tm≡' : {∣t₀∣ ∣t₁∣ : ∣t∣ᵀ Γ A} →
         (∣t₍₀₁₎∣ : ∣t₀∣ ≡ ∣t₁∣) →
         {t₀~ : t~ᵀ Γ A ∣t₀∣}{t₁~ : t~ᵀ Γ A ∣t₁∣} →
         _≡_ {A = Tm Γ A} (mkTm ∣t₀∣ t₀~) (mkTm ∣t₁∣ t₁~)
  Tm≡' {A = A} refl = Tm≡ refl
    (toProp (Πiₚ $ Πdiₚ $ Πdₚ $ cong (λ z _ a₍₀₁₎₀ a₍₀₁₎₁ → z _ a₍₀₁₎₀ a₍₀₁₎₁) (~p A)) _ _)

  variable
    t u v : Tm Γ A

  _[_]t : (t : Tm Γ A) → (σ : Sub Δ Γ) → Tm Δ (A [ σ ]T)
  ∣ t [ γ ]t ∣ δₓ = ∣ t ∣ (∣ γ ∣ δₓ)
  ((t [ γ ]t) ~) δ₍₀₁₎ = (t ~) ((γ ~) δ₍₀₁₎)

  tm[] : (a : Tm Δ A) → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
  tm[] {A = A} = _[_]t {A = A}

  syntax tm[] {A = A} a σ = a ⟨ A ⟩[ σ ]t

  -- [∘]t : subst (Tm _) [∘]T (t [ σ ∘ δ ]t) ≡ (t [ σ ]t [ δ ]t)
  -- [∘]t = {!   !}

  -- [∘]t : t [ σ ∘ δ ]t ≡ t [ σ ]t [ δ ]t
  -- [∘]t = {!   !}

  -- [id]t : {t : Tm {j} Γ A} → t [ id ]t ≡ t
  -- [id]t = ?

  ∣subst~∣ : {Γ : Con i}{A : Ty Γ j}{tm : Tm Γ A}{~p₁ : Tyᵀ.~pᵀ Γ j ∣ A ∣ (A ~)} →
          -- (eq : (~p A ≡ ~p₁)) → ∣ subst (Tm Γ) (Ty≡' refl refl eq {RA₁ = R A} {SA₁ = S A} {TA₁ = T A} refl {cohA₁ = coh A}) tm ∣ ≡ ∣ tm ∣
          (eq : (~p A ≡ ~p₁)) → ∣ subst (Tm Γ) (Ty≡ refl refl eq refl refl refl refl refl) tm ∣ ≡ ∣ tm ∣
  ∣subst~∣ refl = refl


module ContextExtension where
  open Category
  open Families

  infixl 5 _▹_
  _▹_ : (Γ : Con i) → (A : Ty Γ j) → Con (i ⊔ j)
  ∣ Γ ▹ A ∣ = Σ ∣ Γ ∣ ∣ A ∣
  ((Γ ▹ A) ~) ((γ₀ ,ₗ a₀) ,ₗ (γ₁ ,ₗ a₁)) = Σ ((Γ ~) (γ₀ ,ₗ γ₁)) λ γ₍₀₁₎ → (A ~) (γ₀ ,ₗ γ₁ ,ₗ γ₍₀₁₎ ,ₗ a₀ ,ₗ a₁)
  ~p (Γ ▹ A) = Σdₚ
    (cong (λ { z ( (d₀ ,ₗ a₀) ,ₗ (d₁ ,ₗ a₁))           d₍₀₁₎₀ d₍₀₁₎₁ → z _ d₍₀₁₎₀ d₍₀₁₎₁ }) (~p Γ))
    (cong (λ { z (((d₀ ,ₗ a₀) ,ₗ (d₁ ,ₗ a₁)) ,ₗ d₍₀₁₎) a₍₀₁₎₀ a₍₀₁₎₁ → z _ a₍₀₁₎₀ a₍₀₁₎₁ }) (~p A))
  R (Γ ▹ A) (γₓ ,ₗ a) = R Γ γₓ ,ₗ (R A) a
  S (Γ ▹ A) (γ₍₀₁₎ ,ₗ a₍₀₁₎) = (S Γ γ₍₀₁₎) ,ₗ (S A) a₍₀₁₎
  T (Γ ▹ A) (γ₍₀₁₎ ,ₗ a₍₀₁₎) (γ₍₁₂₎ ,ₗ a₍₁₂₎) = T Γ γ₍₀₁₎ γ₍₁₂₎ ,ₗ (T A) a₍₀₁₎ a₍₁₂₎

  infixl 5 _,_
  _,_ : (γ : Sub Δ Γ) → (t : Tm Δ (A [ γ ]T)) → Sub Δ (Γ ▹ A)
  ∣ σ , t ∣ δₓ = ∣ σ ∣ δₓ ,ₗ ∣ t ∣ δₓ
  ((σ , t) ~) δ₍₀₁₎ = (σ ~) δ₍₀₁₎ ,ₗ (t ~) δ₍₀₁₎

  con, : (γ : Sub Δ Γ) → (t : Tm Δ (A [ γ ]T)) → Sub Δ (Γ ▹ A)
  con, {A = A} = _,_ {A = A}

  infixl 5 con,
  syntax con, {A = A} σ t = σ ,⟨ A ⟩ t

  p : Sub (Γ ▹ A) Γ
  ∣ p ∣ (γₓ    ,ₗ aₓ)    = γₓ
  (p ~) (γ₍₀₁₎ ,ₗ a₍₀₁₎) = γ₍₀₁₎

  syntax p {A = A} = p⟨ A ⟩

  -- q : Tm (Γ ▹ A) (A [ p {A = A} ]T)
  q : Tm (Γ ▹ A) (A [ p⟨ A ⟩ ]T)
  ∣ q ∣ (γₓ    ,ₗ aₓ)    = aₓ
  (q ~) (γ₍₀₁₎ ,ₗ a₍₀₁₎) = a₍₀₁₎

  syntax q {A = A} = q⟨ A ⟩

  -- ▹β₁ : ({A : Ty Γ i}{σ : Sub Δ Γ} → p⟨ A ⟩ ∘ (σ ,⟨ A ⟩ t)) ≡ ?
  -- ▹β₁ = ?

  ▹β₁ : p⟨ A ⟩ ∘ (σ ,⟨ A ⟩ t) ≡ σ
  ▹β₁ = refl

  -- ▹β₂ : subst (Tm _) (
  --   ((A [ p⟨ A ⟩ ]T) [ σ ,⟨ A ⟩ t ]T)
  --     ≡⟨ sym [∘]T ⟩
  --   (A [ p⟨ A ⟩ ∘ (σ ,⟨ A ⟩ t) ]T)
  --     ≡⟨ cong (A [_]T) ▷β₁ ⟩
  --   (A [ σ ]T)
  --     ∎
  --   ) (q⟨ A ⟩ [ σ ,⟨ A ⟩ t ]t) ≡ t
  -- -- ▹β₂ : subst (Tm _) {! ? ≡⟨ ? ⟩ ? ∎!} (q⟨ A ⟩ [ σ ,⟨ A ⟩ t ]t) ≡ t
  -- ▹β₂ = {!   !}

  ▹η : p⟨ A ⟩ ,⟨ A ⟩ q⟨ A ⟩ ≡ id
  ▹η = refl

  -- ▹η' : (p⟨ A ⟩ ∘ (σ ,⟨ A ⟩ t)) ,⟨ A ⟩ (q⟨ A ⟩ [ σ ,⟨ A ⟩ t ]t) ≡ σ ,⟨ A ⟩ t
  -- ▹η' = refl

  --   ,ₗ∘ : (σ ,⟨ A ⟩ t) ∘ δ ≡ω (σ ∘ δ ,⟨ A ⟩ (t [ δ ]t))
  --   ,ₗ∘ = L.reflω

  --   _^ : (σ : Sub Γ Δ) → Sub (Γ ▹ A [ σ ]T) (Δ ▹ A)
  --   _^ {A = A} σ = (σ ∘ p⟨ A [ σ ]T ⟩) ,⟨ A ⟩ q⟨ A [ σ ]T ⟩

  --   sub^ : (σ : Sub Γ Δ) → Sub (Γ ▹ A [ σ ]T) (Δ ▹ A)
  --   sub^ {A = A} = _^ {A = A}

  --   syntax sub^ {A = A} σ = σ ^⟨ A ⟩

  -- help : {Γ : Con i}{A B : Ty Γ j}{tm : Tm Γ A}{eq : A ≡ B} → ∣ subst (Tm Γ) refl tm ∣ ≡ ∣ tm ∣
  -- help = refl

  -- sajt : ∀ {Γ : Con i} {Δ : Con j} {γ : Sub Δ Γ}
  --        {Θ : Con k} {δ : Sub Θ Δ} {A : Ty Γ l}
  --        {t : Tm Δ (A [ γ ]T)} →
  --        (∣ t [ δ ]t ∣) ≡
  --        (∣ subst (Tm Θ) (Ty≡' refl refl (cong-cong (~p A)) refl) (t [ δ ]t) ∣)
  -- sajt = {!   !}
  -- sajt : ∀ {Γ : Con i} {Δ : Con j} {γ : Sub Δ Γ}
  --        {Θ : Con k} {δ : Sub Θ Δ} {A : Ty Γ l}
  --        {t : Tm Δ (A [ γ ]T)} →
        --  (∣ t [ δ ]t ∣) ≡
        --  (∣ subst (Tm Θ) (Ty≡' refl refl (cong-cong (~p A)) refl) (t [ δ ]t) ∣)
        --  (λ θₓ → ∣ t ∣ (∣ δ ∣ θₓ)) ≡
        --  (λ θₓ → ∣ subst (Tm Θ) (Ty≡' refl refl (cong-cong (~p A)) refl) (t [ δ ]t) ∣ θₓ)
  -- sajt = {!   !}

  -- ,∘ : {Γ : Con i}{Δ : Con j}{Θ : Con k}
  --      {δ : Sub Θ Δ}{γ : Sub Δ Γ}

  -- CORRECT
  -- ,∘ : {A : Ty Γ l} → {t : Tm Δ (A [ γ ]T)} →
  --      (γ ,⟨ A ⟩ t) ∘ δ ≡ ((γ ∘ δ) ,⟨ A ⟩ (subst (Tm Θ) ([][]T {A = A} {γ = γ})) (t [ δ ]t))

  -- ,∘ {γ = γ}{Θ = Θ}{δ = δ}{A = A} =
  --   Sub≡' (cong
  --     (λ (a : (θₓ : ∣ Θ ∣) → ∣ A [ γ ∘ δ ]T ∣ θₓ) θₓ → ∣ γ ∣ (∣ δ ∣ θₓ) ,ₗ a θₓ)
  --     (sym (∣subst~∣ {Γ = Θ} {A = A [ γ ∘ δ ]T} {~p₁ = ~p (A [ γ ]T [ δ ]T)} {! sym (cong-cong (~p A))  !})))

  -- ,∘ : {A : Ty Γ l} → {t : Tm Δ (A [ γ ]T)} →
  --      (γ ,⟨ A ⟩ t) ∘ δ ≡ ((γ ∘ δ) ,⟨ A ⟩ (subst (Tm Θ) (sym ([∘]T {A = A} {γ = γ})) (t [ δ ]t)))

  -- ,∘ {γ = γ}{Θ = Θ}{δ = δ}{A = A} =
  --   Sub≡' (cong
  --     (λ (a : (θₓ : ∣ Θ ∣) → ∣ A [ γ ∘ δ ]T ∣ θₓ) θₓ → ∣ γ ∣ (∣ δ ∣ θₓ) ,ₗ a θₓ)
  --     (sym {!   !}))

  -- ,∘ {γ = γ}{Θ = Θ}{δ = δ}{A = A} =
  --   Sub≡' (cong
  --     (λ (a : (θₓ : ∣ Θ ∣) → ∣ A [ γ ∘ δ ]T ∣ θₓ) θₓ → ∣ γ ∣ (∣ δ ∣ θₓ) ,ₗ a θₓ)
  --     (sym (∣subst~∣ {Γ = Θ} {A = A [ γ ∘ δ ]T} {~p₁ = ~p (A [ γ ]T [ δ ]T)} (sym {! cong-cong ?  !}))))

  -- ,∘ {γ = γ}{Θ = Θ}{δ = δ}{A = A} =
  --   Sub≡' (cong
  --     (λ (a : (θₓ : ∣ Θ ∣) → ∣ A [ γ ∘ δ ]T ∣ θₓ) θₓ → ∣ γ ∣ (∣ δ ∣ θₓ) ,ₗ a θₓ)
  --     (sym {!   !}))

  -- ,∘ {γ = γ}{Θ = Θ}{δ = δ}{A = A} =
  --   Sub≡' (cong
  --     (λ (a : (θₓ : ∣ Θ ∣) → ∣ A [ γ ∘ δ ]T ∣ θₓ) θₓ → ∣ γ ∣ (∣ δ ∣ θₓ) ,ₗ a θₓ)
  --     (sym (∣subst~∣ {Γ = Θ} {A = A [ γ ∘ δ ]T} {~p₁ = {! ~p (A [ γ ]T [ δ ]T)  !}} (sym (cong-cong (~p A))))))

  -- ,∘ {γ = γ}{Θ = Θ}{δ = δ}{A = A} =
  --   Sub≡' (cong
  --     (λ (a : (θₓ : ∣ Θ ∣) → ∣ A [ γ ∘ δ ]T ∣ θₓ) θₓ → ∣ γ ∣ (∣ δ ∣ θₓ) ,ₗ a θₓ)
  --     (sym (help' {Γ = Θ} {A = {!   !}} (cong-cong {f = {! ∣ γ ∣   !}} {g = λ z a → z ({! ∣ δ ∣  !} a)} (~p A)))))

  -- ,∘ {γ = γ}{Θ = Θ}{δ = δ}{A = A} =
  --   Sub≡' (cong (λ (a : (θₓ : ∣ Θ ∣) → ∣ A [ γ ∘ δ ]T ∣ θₓ) θₓ → ∣ γ ∣ (∣ δ ∣ θₓ) ,ₗ a θₓ) (sym {!   !}))

  -- ,∘ {γ = γ}{Θ = Θ}{δ = δ}{A = A} =
  --   Sub≡' (cong (λ (a : (θₓ : ∣ Θ ∣) → ∣ A [ γ ∘ δ ]T ∣ θₓ) θₓ → ∣ γ ∣ (∣ δ ∣ θₓ) ,ₗ a θₓ) (sym (help' (cong-cong (~p A)))))


  -- a : (θₓ : ∣ Θ ∣) → ∣ A [ γ ∘ δ ] ∣ θₓ

-- module Lifting where
--   open CwFₘ.SizeLevelLiftingₘ
--   open Propositionsₘ.SizeLevelLiftingₚₘ

--   open Category
--   open Families

--   Lift : Ty Γ i → Ty Γ (i ⊔ j)
--   ∣ Lift {j = j} A ∣ γ₀                    = Liftₘ  {j = j} (∣ A ∣ γ₀)
--   [ Lift {j = j} A ] γ₍₀₁₎ ⇒ a₀ ~ a₁       = Liftₚₘ {j = j} ([ A ] γ₍₀₁₎ ⇒ lowerₘ a₀ ~ lowerₘ a₁)
--   [ Lift A ]⟳                              = liftₚₘ [ A ]⟳
--   [ Lift A ]↔ γ₍₀₁₎ ⇒ a₀~a₁                = liftₚₘ ([ A ]↔ γ₍₀₁₎ ⇒ lowerₚₘ a₀~a₁)
--   [ Lift A ] γ₍₀₁₎ » γ₁~γ₂ ⇒ a₀~a₁ » a₁~a₂ = liftₚₘ ([ A ] γ₍₀₁₎ » γ₁~γ₂ ⇒ lowerₚₘ a₀~a₁ » lowerₚₘ a₁~a₂)
--   [ Lift A ]↷ γ₍₀₁₎ ⇒ a                    = liftₘ ([ A ]↷ γ₍₀₁₎ ⇒ lowerₘ a)
--   [ Lift A ]≈ γ₍₀₁₎ ⇒ a                    = liftₚₘ ([ A ]≈ γ₍₀₁₎ ⇒ lowerₘ a)

--   Lift[] : Lift {j = j} A [ σ ]T ≡ω Lift {j = j} (A [ σ ]T)
--   Lift[] = L.reflω

--   lift : Tm Γ A → Tm Γ (Lift {j = j} A)
--   ∣ lift t ∣  γ     = liftₘ  (∣ t ∣ γ)
--   [ lift t ]≈ γ₍₀₁₎ = liftₚₘ ([ t ]≈ γ₍₀₁₎)

--   lift[] : lift {j = j} t [ σ ]t ≡ω lift {j = j} (t [ σ ]t)
--   lift[] = L.reflω

--   lower : Tm Γ (Lift {j = j} A) → Tm Γ A
--   ∣ lower t ∣  γ     = lowerₘ  (∣ t ∣ γ)
--   [ lower t ]≈ γ₍₀₁₎ = lowerₚₘ ([ t ]≈ γ₍₀₁₎)

--   Liftβ : lower (lift {A = A} {j = j} t) ≡ω t
--   Liftβ = L.reflω

--   Liftη : lift (lower {j = j} {A = A} t) ≡ω t
--   Liftη = L.reflω

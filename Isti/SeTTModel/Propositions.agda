{-# OPTIONS --without-K #-}
module SeTTModel.Propositions where

open import Agda.Primitive
open import Lib renaming (⊤ to ⊤ₗ ; tt to ttₗ ; _,_ to _,ₗ_)
-- open ≡-Reasoning

open import PfProp
open import SeTTModel.CwF

module Propositions where
  open Category

  module TyPᵀ (Γ : Con i) (j : Level) where
    ∣P∣ᵀ : Set (i ⊔ lsuc j)
    ∣P∣ᵀ = (γₓ : ∣ Γ ∣) → Set j

    ∣pᵀ : (∣P∣ : ∣P∣ᵀ) → Set (i ⊔ j)
    ∣pᵀ ∣P∣ = isPfPropd ∣P∣

    coePᵀ : ∣P∣ᵀ → Set (i ⊔ j)
    coePᵀ ∣P∣ = {γ₀ γ₁ : ∣ Γ ∣} → (γ₍₀₁₎ : (Γ ~) (γ₀ ,ₗ γ₁)) → (a₀ : ∣P∣ γ₀) → ∣P∣ γ₁
  open TyPᵀ

  record TyP (Γ : Con i) (j : Level) : Set (i ⊔ lsuc j) where
    constructor mkTyP
    field
      ∣P∣  : ∣P∣ᵀ Γ j
      ∣p   : ∣pᵀ Γ j ∣P∣
      coeP : coePᵀ Γ j ∣P∣
  open TyP public renaming
    ( ∣P∣  to ∣_∣
    ; coeP to coe
    )

  TyP≡ :
    {∣P₀∣ ∣P₁∣ : ∣P∣ᵀ Γ i} →
    (∣P₍₀₁₎∣ : ∣P₀∣ ≡ ∣P₁∣) →

    {∣p₀ : ∣pᵀ Γ i ∣P₀∣}{∣p₁ : ∣pᵀ Γ i ∣P₁∣} →
    (∣p₍₀₁₎ : _≡_ {A = ∣pᵀ Γ i ∣P₁∣} (subst (∣pᵀ Γ i) ∣P₍₀₁₎∣ ∣p₀) ∣p₁) →

    {coeP₀ : coePᵀ Γ i ∣P₀∣}{coeP₁ : coePᵀ Γ i ∣P₁∣} →
    (coeP₍₀₁₎ : _≡_ {A = coePᵀ Γ i ∣P₁∣} (subst (coePᵀ Γ i) ∣P₍₀₁₎∣ coeP₀) coeP₁) →

    _≡_ {A = TyP Γ i}
      (mkTyP ∣P₀∣ ∣p₀ coeP₀)
      (mkTyP ∣P₁∣ ∣p₁ coeP₁)
  TyP≡ refl refl refl = refl

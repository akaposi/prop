{-# OPTIONS --without-K #-}
module SeTTModel.Typeformers where

open import Agda.Primitive
open import Lib renaming
  (⊤ to ⊤ₗ ; tt to ttₗ ; Σ to Σₗ ; _,_ to _,ₗ_ ; id to idₗ)
-- open ≡-Reasoning

open import PfProp
open import SeTTModel.CwF
open import SeTTModel.Propositions

module Unit where -- Top
  open Category
  open Families

  ⊤ : Ty Γ lzero
  ∣ ⊤ ∣ _   = ⊤ₗ
  (⊤ ~) _   = ⊤ₗ
  ~p ⊤      = refl
  R ⊤   _   = ttₗ
  S ⊤   _   = ttₗ
  T ⊤   _ _ = ttₗ
  coe ⊤ _ _ = ttₗ
  coh ⊤ _ _ = ttₗ

  tt : Tm Γ ⊤
  ∣ tt ∣ _ = ttₗ
  (tt ~) _ = ttₗ

module Sigma where
  open Category
  open Families
  open ContextExtension

  Σ : (A : Ty Γ i) → (B : Ty (Γ ▹ A) j) → Ty Γ (i ⊔ j)
  ∣ Σ A B ∣ γₓ = Σₗ (∣ A ∣ γₓ) (λ a → ∣ B ∣ (γₓ ,ₗ a))
  (Σ A B ~) (γ₀ ,ₗ γ₁ ,ₗ γ₍₀₁₎ ,ₗ (a₀ ,ₗ b₀) ,ₗ (a₁ ,ₗ b₁)) = Σₗ
    ((A ~) (γ₀ ,ₗ γ₁ ,ₗ γ₍₀₁₎ ,ₗ a₀ ,ₗ a₁))
    (λ a₍₀₁₎ → (B ~) (((γ₀ ,ₗ a₀) ,ₗ (γ₁ ,ₗ a₁) ,ₗ (γ₍₀₁₎ ,ₗ a₍₀₁₎) ,ₗ b₀ ,ₗ b₁)))
  ~p (Σ A B) = Σdₚ (cong (λ z _ a₀ a₁ → z _ a₀ a₁) (~p A)) (cong (λ z _ b₀ b₁ → z _ b₀ b₁) (~p B))
  R (Σ A B) (a ,ₗ b) =
    ((R A) a) ,ₗ
    ((R B) b)
  S (Σ A B) (a₍₀₁₎ ,ₗ b₍₀₁₎) =
    ((S A) a₍₀₁₎) ,ₗ
    ((S B) b₍₀₁₎)
  T (Σ A B) (a₍₀₁₎ ,ₗ b₍₀₁₎) (a₍₁₂₎ ,ₗ b₍₁₂₎) =
    ((T A) a₍₀₁₎ a₍₁₂₎) ,ₗ
    ((T B) b₍₀₁₎ b₍₁₂₎)
  coe (Σ A B) γ₍₀₁₎ (a₀ ,ₗ b₀) =
    ((coe A) γ₍₀₁₎ a₀) ,ₗ
    ((coe B) (γ₍₀₁₎ ,ₗ (coh A) γ₍₀₁₎ a₀) b₀)
  coh (Σ A B) γ₍₀₁₎ (a₀ ,ₗ b₀) =
    ((coh A) γ₍₀₁₎ a₀) ,ₗ
    ((coh B) (γ₍₀₁₎ ,ₗ (coh A) γ₍₀₁₎ a₀) b₀)

  -- infix 5 _,Σ_
  -- _,Σ_ : {A : Ty Γ i}{B : Ty (Γ ▹ A) j} →
  --        (a : Tm {j} Γ A) →
  --        (b : Tm Γ (B [ id ,⟨ A ⟩ subst (Tm Γ) [id]T' a ]T)) →
  --       --  (b : Tm Γ (B [ id ,⟨ A ⟩ subst (Tm Γ) (sym [id]T) a ]T)) →
  --        Tm Γ (Σ A B)
  -- ∣ _,Σ_ {Γ = Γ} {A = A} {B = B} a b ∣ γₓ =
  --   (∣ a ∣ γₓ) ,ₗ subst ∣ B ∣ (cong
  --       {A = (γₜ : ∣ Γ ∣) → ∣ A ∣ γₜ}
  --       {a₀ = ∣ subst (Tm Γ) (Ty≡' refl refl (cong-id' (~p A)) refl) a ∣}
  --       {a₁ = ∣ a ∣}
  --       (λ ∣a∣ → γₓ ,ₗ ∣a∣ γₓ)
  --       (∣subst~∣ {A = A} {~p₁ = ~p (A [ id ]T)} {!  (cong-id' (~p A)) !})
  --     ) (∣ b ∣ γₓ)
  -- ((a ,Σ b) ~) γ₍₀₁₎ = ((a ~) γ₍₀₁₎) ,ₗ {!   !}

  -- -- ∣ _,Σ_ {B = B} a b ∣ γₓ = (∣ a ∣ γₓ) ,ₗ subst ∣ B ∣ (cong (λ ∣a∣ → (γₓ ,ₗ ∣ a ∣ γₓ)) (∣subst~∣ {!   !})) (∣ b ∣ γₓ)
  -- -- ∣ _,Σ_ {B = B} a b ∣ γₓ = (∣ a ∣ γₓ) ,ₗ subst ∣ B ∣ {!   !} (∣ b ∣ γₓ)
  -- -- ∣ a ,Σ b ∣ γₓ = (∣ a ∣ γₓ) ,ₗ {! subst ? ? (∣ b ∣ γₓ) !}
  -- ∣ a ,Σ b ∣ γ      =  ∣ a ∣ γ     ,ₗ ∣ b ∣ γ
  -- ((a ,Σ b) ~) γ₀~γ₁ = (a ~) γ₀~γ₁ ,ₗ (b ~) γ₀~γ₁

  -- (γₓ ,ₗ ∣ subst (Tm Γ) (Ty≡' refl refl (cong-id' (~p A)) refl) a ∣ γₓ) ≡ (γₓ ,ₗ ∣ a ∣ γₓ)
  -- (γₓ ,ₗ ∣ subst (Tm Γ) (sym (Ty≡' refl refl (cong-id (~p A)) refl)) a ∣ γₓ) ≡ (γₓ ,ₗ ∣ a ∣ γₓ)
  -- (cong-id (~p A))

module Pi where
  open Category
  open Families
  open ContextExtension

  subst~ : {Γ : Con i}{A : Ty Γ j} →
           {γ₀ γ₁ : ∣ Γ ∣} → {γ₍₀₁₎ γ₍₀₁₎' : (Γ ~) (γ₀ ,ₗ γ₁)} →
           {a₀ : ∣ A ∣ γ₀}{a₁ : ∣ A ∣ γ₁} →
           ((A ~) (γ₀ ,ₗ γ₁ ,ₗ γ₍₀₁₎ ,ₗ a₀ ,ₗ a₁)) →
           ((A ~) (γ₀ ,ₗ γ₁ ,ₗ γ₍₀₁₎' ,ₗ a₀ ,ₗ a₁))
  subst~ {Γ = Γ} {A} {γ₀} {γ₁} {γ₍₀₁₎} {γ₍₀₁₎'} {a₀} {a₁} a₍₀₁₎ =
    subst (λ γ₍₀₁₎'' → (A ~) (γ₀ ,ₗ γ₁ ,ₗ γ₍₀₁₎'' ,ₗ a₀ ,ₗ a₁)) (toPropd (~p Γ) γ₍₀₁₎ γ₍₀₁₎') a₍₀₁₎

  Π : (A : Ty Γ i) → (B : Ty (Γ ▹ A) j) → Ty Γ (i ⊔ j)
  ∣ Π {Γ = Γ} A B ∣ γₓ = Σₗ
    ((a : ∣ A ∣ γₓ) → (∣ B ∣ (γₓ ,ₗ a)))
    (λ f → {a₀ a₁ : ∣ A ∣ γₓ}(a₍₀₁₎ : (A ~) ((γₓ ,ₗ γₓ ,ₗ  (R Γ) γₓ ,ₗ a₀ ,ₗ a₁))) →
      (B ~) ((γₓ ,ₗ a₀) ,ₗ (γₓ ,ₗ a₁) ,ₗ ((R Γ) γₓ ,ₗ a₍₀₁₎) ,ₗ f a₀ ,ₗ f a₁))
  (Π A B ~) (γ₀ ,ₗ γ₁ ,ₗ γ₍₀₁₎ ,ₗ (f₀ ,ₗ f₀~) ,ₗ (f₁ ,ₗ f₁~)) =
    {a₀ : ∣ A ∣ γ₀}{a₁ : ∣ A ∣ γ₁}(a₍₀₁₎ : (A ~) (γ₀ ,ₗ γ₁ ,ₗ γ₍₀₁₎ ,ₗ a₀ ,ₗ a₁)) →
    (B ~) (((γ₀ ,ₗ a₀) ,ₗ (γ₁ ,ₗ a₁) ,ₗ (γ₍₀₁₎ ,ₗ a₍₀₁₎) ,ₗ f₀ a₀ ,ₗ f₁ a₁))
  ~p (Π A B) = Πdiₚ (Πdiₚ (Πdₚ (cong (λ z _ f₀ f₁ → z _ f₀ f₁) (~p B))))
  R (Π A B) (f ,ₗ f~) a₍₀₁₎ = f~ a₍₀₁₎
  S (Π {Γ = Γ} A B) {γ₍₀₁₎ = γ₍₀₁₎} {a₀ = f₀ ,ₗ f₀~} {a₁ = f₁ ,ₗ f₁~} f₍₀₁₎ {a₀ = a₁} {a₁ = a₀} a₍₁₀₎ =
    let
      γ₍₁₀₎ = (S Γ) γ₍₀₁₎
      a₍₀₁₎ = (S A) a₍₁₀₎
      -- a₍₀₁₎' = subst~ {γ₍₀₁₎' = S Γ (S Γ γ₍₀₁₎)} a₍₀₁₎
      a₍₀₁₎₌ = subst (λ γ₍₀₁₎₌ → (A ~) (_ ,ₗ _ ,ₗ γ₍₀₁₎₌ ,ₗ _ ,ₗ _)) (toPropd (~p Γ) _ _) a₍₀₁₎
    in
      subst
      ((λ γa₍₀₁₎₌ → (B ~) (_ ,ₗ _ ,ₗ γa₍₀₁₎₌ ,ₗ f₁ a₁ ,ₗ f₀ a₀)))
      (toPropd (~p (Γ ▹ A)) _ _)
      ((S B) (f₍₀₁₎ a₍₀₁₎₌))
      -- subst
      -- ((λ a₍₀₁₎₌ → (B ~) (_ ,ₗ _ ,ₗ (S Γ γ₍₀₁₎ ,ₗ a₍₀₁₎₌) ,ₗ f₁ a₁ ,ₗ f₀ a₀)))
      -- (toPropd (~p A) _ _)
      -- ((S B) (γ₍₀₁₎ ,ₗ a₍₀₁₎') (f~ a₍₀₁₎'))
  T (Π {Γ = Γ} A B) {γ₍₀₁₎ = γ₍₀₁₎} {γ₍₁₂₎} {a₀ = f₀ ,ₗ f₀~} {a₁ = f₁ ,ₗ f₁~} {a₂ = f₂ ,ₗ f₂~} f₍₀₁₎ f₍₁₂₎ {a₀ = a₀} {a₁ = a₂} a₍₀₂₎ =
    let
      γ₍₁₀₎ = (S Γ) γ₍₀₁₎
      γ₍₀₂₎ = (T Γ) γ₍₀₁₎ γ₍₁₂₎
      a₁ = (coe A) γ₍₀₁₎ a₀
      a₍₀₁₎ = (coh A) γ₍₀₁₎ a₀
      a₍₁₀₎ = (S A) a₍₀₁₎
      a₍₁₂₎ = (T A) a₍₁₀₎ a₍₀₂₎
      a₍₁₂₎₌ = subst (λ γ₌ → (A ~) (_ ,ₗ _ ,ₗ γ₌ ,ₗ _ ,ₗ _)) (toPropd (~p Γ) _ _) a₍₁₂₎
    in
      subst
      ((λ γ₌ → (B ~) (_ ,ₗ _ ,ₗ γ₌ ,ₗ _ ,ₗ _)))
      (toPropd (~p (Γ ▹ A)) _ _)
      ((T B) (f₍₀₁₎ a₍₀₁₎) (f₍₁₂₎ a₍₁₂₎₌))
  coe (Π {Γ = Γ} A B) γ₍₀₁₎ (f₀ ,ₗ f₀~) =
    let
      γ₍₁₀₎ = (S Γ) γ₍₀₁₎
      -- a₀
      -- a₀ = (coe A) γ₍₁₀₎ a₁
    in
    (λ a₁ →
      let
        a₀ = (coe A) γ₍₁₀₎ a₁
        a₍₁₀₎ = (coh A) γ₍₁₀₎ a₁
        a₍₀₁₎ = (S A) a₍₁₀₎
        a₍₀₁₎₌ = subst (λ γ₌ → (A ~) (_ ,ₗ _ ,ₗ γ₌ ,ₗ a₀ ,ₗ a₁)) (toPropd (~p Γ) _ _) a₍₀₁₎
      in
      (coe B) (γ₍₀₁₎ ,ₗ a₍₀₁₎₌) (f₀ a₀))
    ,ₗ
    (λ {a₁₀} {a₁₁} a₁₍₀₁₎ →
      let
        a₀₀ = (coe A) γ₍₁₀₎ a₁₀
        a₍₁₀₎₀ = (coh A) γ₍₁₀₎ a₁₀
        a₍₀₁₎₀ = (S A) a₍₁₀₎₀
        a₍₀₁₎₀ₛ = subst (λ γ₌ → (A ~) (_ ,ₗ _ ,ₗ γ₌ ,ₗ _ ,ₗ _)) (toPropd (~p Γ) _ _) a₍₀₁₎₀

        a₀₁ = (coe A) γ₍₁₀₎ a₁₁
        a₍₁₀₎₁ = (coh A) γ₍₁₀₎ a₁₁
        a₍₀₁₎₁ = (S A) a₍₁₀₎₁
        a₍₀₁₎₁ₛ = subst (λ γ₌ → (A ~) (_ ,ₗ _ ,ₗ γ₌ ,ₗ _ ,ₗ _)) (toPropd (~p Γ) _ _) a₍₀₁₎₁

        a₀₍₀₁₎ = (T A) ((T A) a₍₀₁₎₀ a₁₍₀₁₎) a₍₁₀₎₁
        a₀₍₀₁₎ₛ = subst (λ γ₌ → (A ~) (_ ,ₗ _ ,ₗ γ₌ ,ₗ _ ,ₗ _)) (toPropd (~p Γ) _ _) a₀₍₀₁₎

        b₀₀ = f₀ a₀₀
        b₀₁ = f₀ a₀₁

        -- b₁₀ = (coe B) (γ₍₀₁₎ ,ₗ a₍₀₁₎₀ₛ) b₀₀
        b₍₀₁₎₀ = (coh B) (γ₍₀₁₎ ,ₗ a₍₀₁₎₀ₛ) b₀₀
        b₍₁₀₎₀ = (S B) b₍₀₁₎₀

        -- b₁₁ = (coe B) (γ₍₀₁₎ ,ₗ a₍₀₁₎₁ₛ) b₀₁
        b₍₀₁₎₁ = (coh B) (γ₍₀₁₎ ,ₗ a₍₀₁₎₁ₛ) b₀₁
        -- b₍₁₀₎₁ = (S B) _ b₍₀₁₎₁

        b₀₍₀₁₎ = f₀~ a₀₍₀₁₎ₛ
        b₁₍₀₁₎ = (T B) ((T B) b₍₁₀₎₀ b₀₍₀₁₎) b₍₀₁₎₁
        b₁₍₀₁₎ₛ = subst (λ γ₌ → (B ~) (_ ,ₗ _ ,ₗ γ₌ ,ₗ _ ,ₗ _)) (toPropd (~p (Γ ▹ A)) _ _) b₁₍₀₁₎
      in
      b₁₍₀₁₎ₛ)
{-
  a₀₀ ~ a₀₁  -- f₀~ -->  b₀₀ ~ b₀₁

  a₁₀ ~ a₁₁  -- f₁~ -->  b₁₀ ~ b₁₁


  a₀₍₀₁₎  -- f₀~ -->  b₀₍₀₁₎

  a₁₍₀₁₎  -- f₁~ -->  b₁₍₀₁₎


  Goal: f₁~
-}
  coh (Π {Γ = Γ} A B) γ₍₀₁₎ (f₀ ,ₗ f₀~) {a₀₀} {a₁₁} a₍₀₁₎₍₀₁₎ =
    let
      γ₍₁₀₎ = (S Γ) γ₍₀₁₎

      a₀₁ = (coe A) γ₍₁₀₎ a₁₁
      -- a₁₀ = (coe A) γ₍₀₁₎ a₀₀

      -- a₍₀₁₎₀ = (coh A) γ₍₀₁₎ a₀₀

      a₍₁₀₎₁ = (coh A) γ₍₁₀₎ a₁₁
      a₍₀₁₎₁ = (S A) a₍₁₀₎₁
      a₍₀₁₎₁ₛ = subst (λ γ₌ → (A ~) (_ ,ₗ _ ,ₗ γ₌ ,ₗ _ ,ₗ _)) (toPropd (~p Γ) _ _) a₍₀₁₎₁

      a₀₍₀₁₎ = (T A) a₍₀₁₎₍₀₁₎ a₍₁₀₎₁ -- important!
      a₀₍₀₁₎ₛ = subst (λ γ₌ → (A ~) (_ ,ₗ _ ,ₗ γ₌ ,ₗ _ ,ₗ _)) (toPropd (~p Γ) _ _) a₀₍₀₁₎

      b₀₀ = f₀ a₀₀
      b₀₁ = f₀ a₀₁
      b₁₁ = (coe B) (γ₍₀₁₎ ,ₗ a₍₀₁₎₁ₛ) b₀₁
      b₍₀₁₎₁ = (coh B) (γ₍₀₁₎ ,ₗ a₍₀₁₎₁ₛ) b₀₁

      b₀₍₀₁₎ = f₀~ a₀₍₀₁₎ₛ

      b₍₀₁₎₍₀₁₎ = (T B) b₀₍₀₁₎ b₍₀₁₎₁
      b₍₀₁₎₍₀₁₎ₛ = subst (λ γ₌ → (B ~) (_ ,ₗ _ ,ₗ γ₌ ,ₗ b₀₀ ,ₗ b₁₁)) (toPropd (~p (Γ ▹ A)) _ _) b₍₀₁₎₍₀₁₎
    in
    b₍₀₁₎₍₀₁₎ₛ

  lam : Tm (Γ ▹ A) B → Tm Γ (Π A B)
  ∣ lam {Γ = Γ} t ∣ γₓ = (λ aₓ → ∣ t ∣ (γₓ ,ₗ aₓ)) ,ₗ (λ a₍₀₁₎ → (t ~) ((R Γ) γₓ ,ₗ a₍₀₁₎))
  (lam t ~) γ₍₀₁₎ a₍₀₁₎ = (t ~) (γ₍₀₁₎ ,ₗ a₍₀₁₎)

  app : Tm Γ (Π A B) → Tm (Γ ▹ A) B
  ∣ app f ∣ (γₓ ,ₗ aₓ) = π₁ (∣ f ∣ γₓ) aₓ
  (app f ~) (γ₍₀₁₎ ,ₗ a₍₀₁₎) = (f ~) γ₍₀₁₎ a₍₀₁₎

  Πβ : app {A = A} (lam {A = A} t) ≡ t
  Πβ = refl

  Πη : lam {A = A} {B = B} (app {A = A} t) ≡ t
  Πη {B = B} {t = t} = Tm≡'
    (cong
      (λ z γₓ →
        (λ a → π₁ (∣ t ∣ γₓ) a) ,ₗ
        (λ {a₀} {a₁} a₍₀₁₎ → z _ ((t ~) _ a₍₀₁₎) (π₂ (∣ t ∣ γₓ) a₍₀₁₎)))
      (~p B))

module Identity where
  open Category
  open Families
  open Propositions

  Id : (A : Ty Γ i) → (a : Tm Γ A) → (u : Tm Γ A) → TyP Γ i
  ∣ Id {Γ = Γ} A a₀ a₁ ∣ γₓ = (A ~) (γₓ ,ₗ γₓ ,ₗ (R Γ) γₓ ,ₗ ∣ a₀ ∣ γₓ ,ₗ ∣ a₁ ∣ γₓ)
  ∣p (Id A a₀ a₁) = cong (λ z _ id₀ id₁ → z _ id₀ id₁) (~p A)
  coe (Id {Γ = Γ} A a₀ a₁) {γ₀} {γ₁} γ₍₀₁₎ a₀₍₀₁₎ =
    let
      γ₍₁₀₎ = (S Γ) γ₍₀₁₎

      -- a₀₀ = ∣ a₀ ∣ γ₀
      -- a₁₀ = ∣ a₀ ∣ γ₁

      -- a₀₁ = ∣ a₁ ∣ γ₀
      -- a₁₁ = ∣ a₁ ∣ γ₁

      a₍₀₁₎₁ = (a₁ ~) γ₍₀₁₎
      a₍₁₀₎₀ = (a₀ ~) γ₍₁₀₎
      a₁₍₀₁₎ = (T A) ((T A) a₍₁₀₎₀ a₀₍₀₁₎) a₍₀₁₎₁
      a₁₍₀₁₎ₛ = subst (λ γ₌ → (A ~) (_ ,ₗ _ ,ₗ γ₌ ,ₗ _ ,ₗ _)) (toPropd (~p Γ) _ _) a₁₍₀₁₎
    in
    a₁₍₀₁₎ₛ

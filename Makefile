all: paper.pdf

paper.pdf : paper.tex abbrevs.tex b.bib
	latexmk -pdf paper.tex

clean:
	rm -f *.aux *.bbl *.blg *.fdb_latexmk *.fls *.log *.out *.nav *.snm *.toc *.vtc *.ptb *.xdv *~ paper.pdf

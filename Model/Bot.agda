{-# OPTIONS --without-K #-}

module Model.Bot where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core

⊥' : ∀{i}{Γ : Con i}{j} → Ty Γ j
∣ ⊥' ∣ _ = Lift ⊥
(⊥' ~) _ _ _ = Lift ⊤
~p ⊥' = refl
R ⊥' _ = lift tt
S ⊥' _ = lift tt
T ⊥' _ _ = lift tt
coeT ⊥' _ b = b
cohT ⊥' _ _ = lift tt

exfalso : ∀{i}{Γ : Con i}{j}{k}{A : Ty Γ k} → Tm (Γ ▷ ⊥' {j = j}) (A [ wk {A = ⊥'} ]T)
∣ exfalso ∣ (γₓ ,Σ b) = ⊥-elim (unlift b)
(exfalso ~) {γ₀ ,Σ b₀}{γ₁ ,Σ b₁} (γ~ ,Σ _) = ⊥-elim (unlift b₀)

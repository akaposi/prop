{-# OPTIONS --without-K #-}

module Model.Const where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core

K : ∀{i}(Δ : Con i){j}{Γ : Con j} → Ty Γ i
∣ K Δ ∣ _ = ∣ Δ ∣
(K Δ ~) _ δ₀ δ₁ = (Δ ~) δ₀ δ₁
~p (K Δ) = ap (λ z → λ { (tt ,Σ _ ,Σ _ ,Σ _ ,Σ δ₀ ,Σ δ₁) → z (tt ,Σ δ₀ ,Σ δ₁) }) (~p Δ)
R (K Δ) = R Δ
S (K Δ) = S Δ
T (K Δ) = T Δ
coeT (K Δ) _ δₓ = δₓ
cohT (K Δ) _ = R Δ

mkK : ∀{i}{Δ : Con i}{j}{Γ : Con j} → Tms Γ Δ → Tm Γ (K Δ)
∣ mkK δ ∣ = ∣ δ ∣
mkK δ ~ = δ ~

unK : ∀{i}{Δ : Con i}{j}{Γ : Con j} → Tm Γ (K Δ) → Tms Γ Δ
∣ unK t ∣ = ∣ t ∣
unK t ~ = t ~

Kβ : ∀{i}{Δ : Con i}{j}{Γ : Con j}{δ : Tms Γ Δ} → unK (mkK δ) ≡ δ
Kβ = refl

Kη : ∀{i}{Δ : Con i}{j}{Γ : Con j}{t : Tm Γ (K Δ)} → mkK (unK t) ≡ t
Kη = refl

-- K[] : ∀{i}{Δ : Con i}{j}{Γ : Con j}{k}{Θ : Con k}{γ : Tms Θ Γ} → K Δ [ γ ]T ≡ K Δ
-- K[] = Ty= {!!}

{-# OPTIONS --without-K #-}

module Model.Decl where

open import lib
open import Agda.Primitive

record Con i : Set (lsuc i) where
  field
    ∣_∣ : Set i
    _~  : ∣_∣ → ∣_∣ → Set i
    ~p  : isprop1 {A = ⊤ × ∣_∣ × ∣_∣} (λ (_ ,Σ γ ,Σ γ') → _~ γ γ')
    R   : ∀ γ → _~ γ γ
    S   : ∀{γ γ'} → _~ γ γ' → _~ γ' γ
    T   : ∀{γ γ' γ''} → _~ γ γ' → _~ γ' γ'' → _~ γ γ''
  infix 4 ∣_∣
  infix 5 _~
open Con public

record Tms {i j}(Γ : Con i)(Δ : Con j) : Set (i ⊔ j) where
  field
    ∣_∣ : ∣ Γ ∣ → ∣ Δ ∣
    _~  : {γ γ' : Con.∣ Γ ∣} → (Γ ~) γ γ' → (Δ ~) (∣_∣ γ) (∣_∣ γ')
  infix 4 ∣_∣
  infix 5 _~
open Tms public

~p-deps : ∀{i}(Γ : Con i){j}(∣_∣ : ∣ Γ ∣ → Set j) → Set (i ⊔ j)
~p-deps Γ ∣_∣ = Σ (Σ (Σ (⊤ × Con.∣ Γ ∣ × Con.∣ Γ ∣) λ (_ ,Σ γ ,Σ γ') → (Γ Con.~) γ γ') λ (_ ,Σ γ ,Σ _ ,Σ _) → ∣_∣ γ) λ (_ ,Σ γ' ,Σ _ ,Σ _) → ∣_∣ γ'

record Ty {i}(Γ : Con i) j : Set (i ⊔ lsuc j) where
  field
    ∣_∣  : ∣ Γ ∣ → Set j
    _~   : ∀{γ γ'}(γ~ : (Γ ~) γ γ') → ∣_∣ γ → ∣_∣ γ' → Set j
    ~p   : isprop1 {A = ~p-deps Γ ∣_∣} (λ (_ ,Σ γ~ ,Σ α ,Σ α') → _~ γ~ α α')
    R    : ∀{γ} α → _~ (R Γ γ) α α
    S    : ∀{γ γ'}{γ~ : (Γ Con.~) γ γ'}{α : ∣_∣ γ}{α' : ∣_∣ γ'} →
           _~ γ~ α α' → _~ (S Γ γ~) α' α
    T    : ∀{γ γ' γ''}{γ~ : (Γ Con.~) γ γ'}{γ~' : (Γ Con.~) γ' γ''}
           {α : ∣_∣ γ}{α' : ∣_∣ γ'}{α'' : ∣_∣ γ''} →
           _~ γ~ α α' → _~ γ~' α' α'' → _~ (T Γ γ~ γ~') α α''
    coeT : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣ γ → ∣_∣ γ'
    cohT : {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ Con.~) γ γ')(α : ∣_∣ γ) → _~ γ~ α (coeT γ~ α)
  infix 4 ∣_∣
  infix 5 _~
open Ty public

record Tm {i}(Γ : Con i){j}(A : Ty Γ j) : Set (i ⊔ j) where
  field
    ∣_∣ : (γ : ∣ Γ ∣) → ∣ A ∣ γ
    _~  : {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ ~) γ γ') → (A ~) γ~ (∣_∣ γ) (∣_∣ γ')
  infix 4 ∣_∣
open Tm public

coeT* : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){γ γ' : ∣ Γ ∣} → (Γ ~) γ γ' → ∣ A ∣ γ' → ∣ A ∣ γ
coeT* {_}{Γ} A γ~ α' = coeT A (S Γ γ~) α'

cohT* : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){γ γ' : ∣ Γ ∣}(γ~ : (Γ ~) γ γ')(α' : ∣ A ∣ γ') → (A ~) γ~ (coeT* A γ~ α') α'
cohT* {_}{Γ} A γ~ α' = transport (λ z → (A ~) z (coeT* A γ~ α') α') (toProp1 (~p Γ) _ _) (S A (cohT A (S Γ γ~) α'))

T3 : ∀{i}(Γ : Con i){γ γ' γ'' γ''' : ∣ Γ ∣} → (Γ ~) γ γ' → (Γ ~) γ' γ'' → (Γ ~) γ'' γ''' → (Γ ~) γ γ'''
T3 Γ γ~ γ~' γ~'' = T Γ γ~ (T Γ γ~' γ~'')

TT3 : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){γ γ' γ'' γ''' : ∣ Γ ∣}
  {γ~ : (Γ ~) γ γ'}{γ~' : (Γ ~) γ' γ''}{γ~'' : (Γ ~) γ'' γ'''}
  {α : ∣ A ∣ γ}{α' : ∣ A ∣ γ'}{α'' : ∣ A ∣ γ''}{α''' : ∣ A ∣ γ'''}
  (α~ : (A ~) γ~ α α')(α~' : (A ~) γ~' α' α'')(α~'' : (A ~) γ~'' α'' α''')
  → (A ~) (T3 Γ γ~ γ~' γ~'') α α'''
TT3 A α~ α~' α~'' = T A α~ (T A α~' α~'')

coeR~ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){γ : ∣ Γ ∣}(α : ∣ A ∣ γ) → (A ~) (R Γ γ) (coeT A (R Γ γ) α) α
coeR~ {Γ = Γ} A {γ} α = transport
  (λ z → (A ~) z (coeT A (R Γ γ) α) α)
  (toProp1 (~p Γ) _ _)
  (S A (cohT A (R Γ γ) α))

coeTr~ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){γ₀ γ₁ γ₂ : ∣ Γ ∣}(γ₀₁ : (Γ ~) γ₀ γ₁)(γ₁₂ : (Γ ~) γ₁ γ₂)(α₀ : ∣ A ∣ γ₀) →
  (A ~) (R Γ γ₂) (coeT A (T Γ γ₀₁ γ₁₂) α₀) (coeT A γ₁₂ (coeT A γ₀₁ α₀))
coeTr~ {Γ = Γ} A γ₀₁ γ₁₂ α₀ = transport
  (λ z → (A ~) z (coeT A (T Γ γ₀₁ γ₁₂) α₀) (coeT A γ₁₂ (coeT A γ₀₁ α₀)))
  (toProp1 (~p Γ) _ _)
  (TT3 A
    (S A (cohT A (T Γ γ₀₁ γ₁₂) α₀))
    (cohT A γ₀₁ α₀)
    (cohT A γ₁₂ (coeT A γ₀₁ α₀)))

Ty=' : ∀{i}{Γ : Con i}{j} → 
  {∣_∣   : ∣ Γ ∣ → Set j}
  {_~ : ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣ γ → ∣_∣ γ' → Set j}
  {~p₀ ~p₁ : isprop1 {A = Σ (Σ (Σ (⊤ × Con.∣ Γ ∣ × Con.∣ Γ ∣) λ (_ ,Σ γ ,Σ γ') → (Γ Con.~) γ γ') λ (_ ,Σ γ ,Σ _ ,Σ _) → ∣_∣ γ) λ (_ ,Σ γ' ,Σ _ ,Σ _) → ∣_∣ γ'}
             (λ (_ ,Σ γ~ ,Σ α ,Σ α') → _~ γ~ α α')}
  (~p₀₁ : _≡_ ~p₀ ~p₁)
  {R₀ R₁ : ∀{γ} α → _~ (R Γ γ) α α}
  (R₀₁ : _≡_ {A =  ∀{γ} α → _~ (R Γ γ) α α} R₀ R₁)
  {S₀ S₁ : ∀{γ γ'}{p : (Γ Con.~) γ γ'}{α : ∣_∣ γ}{α' : ∣_∣ γ'}
          → _~ p α α' → _~ (S Γ p) α' α}
  (S₀₁ : _≡_ {A = ∀{γ γ'}{p : (Γ Con.~) γ γ'}{α : ∣_∣ γ}{α' : ∣_∣ γ'} → _~ p α α' → _~ (S Γ p) α' α} S₀ S₁)
  {T₀ T₁ : ∀{γ γ' γ''}{p : (Γ Con.~) γ γ'}{q : (Γ Con.~) γ' γ''}
            {α : ∣_∣ γ}{α' : ∣_∣ γ'}{α'' : ∣_∣ γ''}
          → _~ p α α' → _~ q α' α'' → _~ (T Γ p q) α α''}
  (T₀₁ : _≡_ {A = ∀{γ γ' γ''}{p : (Γ Con.~) γ γ'}{q : (Γ Con.~) γ' γ''}{α : ∣_∣ γ}{α' : ∣_∣ γ'}{α'' : ∣_∣ γ''} → _~ p α α' → _~ q α' α'' → _~ (T Γ p q) α α''} T₀ T₁)
  {coeT    : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣ γ → ∣_∣ γ'}
  {cohT    : {γ γ' : Con.∣ Γ ∣}(p : (Γ Con.~) γ γ')(α : ∣_∣ γ) → _~ p α (coeT p α)} →
  _≡_ {A = Ty Γ j}
      (record {∣_∣ = ∣_∣ ; _~ = _~ ; ~p = ~p₀ ; R = R₀ ; S = S₀ ; T = T₀ ; coeT = coeT ; cohT = cohT})
      (record {∣_∣ = ∣_∣ ; _~ = _~ ; ~p = ~p₁ ; R = R₁ ; S = S₁ ; T = T₁ ; coeT = coeT ; cohT = cohT})
Ty=' refl refl refl refl = refl

Ty= : ∀{i}{Γ : Con i}{j} → 
  {∣_∣   : ∣ Γ ∣ → Set j}
  {_~ : ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣ γ → ∣_∣ γ' → Set j}
  {~p₀ ~p₁ : isprop1 {A = Σ (Σ (Σ (⊤ × Con.∣ Γ ∣ × Con.∣ Γ ∣) λ (_ ,Σ γ ,Σ γ') → (Γ Con.~) γ γ') λ (_ ,Σ γ ,Σ _ ,Σ _) → ∣_∣ γ) λ (_ ,Σ γ' ,Σ _ ,Σ _) → ∣_∣ γ'}
             (λ (_ ,Σ γ~ ,Σ α ,Σ α') → _~ γ~ α α')}
  (~p₀₁ : _≡_ ~p₀ ~p₁)
  {R₀ R₁ : ∀{γ} α → _~ (R Γ γ) α α}
  {S₀ S₁ : ∀{γ γ'}{p : (Γ Con.~) γ γ'}{α : ∣_∣ γ}{α' : ∣_∣ γ'}
          → _~ p α α' → _~ (S Γ p) α' α}
  {T₀ T₁ : ∀{γ γ' γ''}{p : (Γ Con.~) γ γ'}{q : (Γ Con.~) γ' γ''}
            {α : ∣_∣ γ}{α' : ∣_∣ γ'}{α'' : ∣_∣ γ''}
          → _~ p α α' → _~ q α' α'' → _~ (T Γ p q) α α''}
  {coeT    : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣ γ → ∣_∣ γ'}
  {cohT    : {γ γ' : Con.∣ Γ ∣}(p : (Γ Con.~) γ γ')(α : ∣_∣ γ) → _~ p α (coeT p α)} →
  _≡_ {A = Ty Γ j}
      (record {∣_∣ = ∣_∣ ; _~ = _~ ; ~p = ~p₀ ; R = R₀ ; S = S₀ ; T = T₀ ; coeT = coeT ; cohT = cohT})
      (record {∣_∣ = ∣_∣ ; _~ = _~ ; ~p = ~p₁ ; R = R₁ ; S = S₁ ; T = T₁ ; coeT = coeT ; cohT = cohT})
Ty= {_}{Γ}{_}{∣_∣T_}{_T_⊢_~_}{~p₀ = ~p}{~p₁} ~p₀₁ {R₀}{R₁}{S₀}{S₁} = Ty=' ~p₀₁ 
  (toProp (ispropΠi $ ispropΠ1 $ ap (λ z (γ ,Σ α) → z (_ ,Σ _ ,Σ _ ,Σ R Γ γ ,Σ α ,Σ α)) ~p) _ _)
  (toProp (ispropΠi $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠ1 $ ap (λ z (_ ,Σ γ~ ,Σ α ,Σ α' ,Σ _) → z (_ ,Σ S Γ γ~ ,Σ α' ,Σ α)) ~p) _ _)
  (toProp (ispropΠi $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠ1 $ ispropΠ1 $ ap (λ z (_ ,Σ γ~ ,Σ γ~' ,Σ α ,Σ _ ,Σ α'' ,Σ _ ,Σ _) → z (_ ,Σ T Γ γ~ γ~' ,Σ α ,Σ α'')) ~p) _ _)

cohT≡ : ∀{i}{Γ : Con i}{j} → 
  {∣_∣₀ ∣_∣₁   : ∣ Γ ∣ → Set j}(∣_∣₀₁ : ∣_∣₀ ≡ ∣_∣₁)
  {_~₀ : ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣₀ γ → ∣_∣₀ γ' → Set j}
  {_~₁ : ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣₁ γ → ∣_∣₁ γ' → Set j}
  (_~₀₁ : _≡_ {A = ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣₁ γ → ∣_∣₁ γ' → Set j} (transport (λ ∣_∣ → ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣ γ → ∣_∣ γ' → Set j) ∣_∣₀₁ _~₀) _~₁)
  {coeT₀ : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₀ γ → ∣_∣₀ γ'}
  {coeT₁ : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₁ γ → ∣_∣₁ γ'}
  (coeT₀₁ : _≡_ {A = {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₁ γ → ∣_∣₁ γ'}
    (tr2
      (λ ∣_∣ _~ → {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣ γ → ∣_∣ γ')
      ∣_∣₀₁ _~₀₁ coeT₀)
    coeT₁)
  (cohT₀ : {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ Con.~) γ γ')(α : ∣_∣₀ γ) → _~₀ γ~ α (coeT₀ γ~ α))
  (cohT₁ : {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ Con.~) γ γ')(α : ∣_∣₁ γ) → _~₁ γ~ α (coeT₁ γ~ α)) →
  Set (i ⊔ j)
cohT≡ refl refl refl = _≡_

Ty='' : ∀{i}{Γ : Con i}{j} → 
  {∣_∣₀ ∣_∣₁   : ∣ Γ ∣ → Set j}(∣_∣₀₁ : ∣_∣₀ ≡ ∣_∣₁)
  {_~₀ : ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣₀ γ → ∣_∣₀ γ' → Set j}
  {_~₁ : ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣₁ γ → ∣_∣₁ γ' → Set j}
  (_~₀₁ : _≡_ {A = ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣₁ γ → ∣_∣₁ γ' → Set j} (transport (λ ∣_∣ → ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣ γ → ∣_∣ γ' → Set j) ∣_∣₀₁ _~₀) _~₁)
  {~p₀ : isprop1 {A = Σ (Σ (Σ (⊤ × Con.∣ Γ ∣ × Con.∣ Γ ∣) λ (_ ,Σ γ ,Σ γ') → (Γ Con.~) γ γ') λ (_ ,Σ γ ,Σ _ ,Σ _) → ∣_∣₀ γ) λ (_ ,Σ γ' ,Σ _ ,Σ _) → ∣_∣₀ γ'} (λ (_ ,Σ γ~ ,Σ α ,Σ α') → _~₀ γ~ α α')}
  {~p₁ : isprop1 {A = Σ (Σ (Σ (⊤ × Con.∣ Γ ∣ × Con.∣ Γ ∣) λ (_ ,Σ γ ,Σ γ') → (Γ Con.~) γ γ') λ (_ ,Σ γ ,Σ _ ,Σ _) → ∣_∣₁ γ) λ (_ ,Σ γ' ,Σ _ ,Σ _) → ∣_∣₁ γ'} (λ (_ ,Σ γ~ ,Σ α ,Σ α') → _~₁ γ~ α α')}
  (~p₀₁ : _≡_
    (tr2
      (λ ∣_∣ _~ → isprop1 {A = Σ (Σ (Σ (⊤ × Con.∣ Γ ∣ × Con.∣ Γ ∣) λ (_ ,Σ γ ,Σ γ') → (Γ Con.~) γ γ') λ (_ ,Σ γ ,Σ _ ,Σ _) → ∣_∣ γ) λ (_ ,Σ γ' ,Σ _ ,Σ _) → ∣_∣ γ'} (λ (_ ,Σ γ~ ,Σ α ,Σ α') → _~ γ~ α α'))
      ∣_∣₀₁ _~₀₁ ~p₀)
    ~p₁)
  {R₀ : ∀{γ} α → _~₀ (R Γ γ) α α}
  {R₁ : ∀{γ} α → _~₁ (R Γ γ) α α}
  (R₀₁ : _≡_ {A =  ∀{γ} α → _~₁ (R Γ γ) α α} (tr2 (λ ∣_∣ _~ → ∀{γ} α → _~ (R Γ γ) α α) ∣_∣₀₁ _~₀₁ R₀) R₁)
  {S₀ : ∀{γ γ'}{γ~ : (Γ ~) γ γ'}{α : ∣_∣₀ γ}{α' : ∣_∣₀ γ'} → _~₀ γ~ α α' → _~₀ (S Γ γ~) α' α}
  {S₁ : ∀{γ γ'}{γ~ : (Γ ~) γ γ'}{α : ∣_∣₁ γ}{α' : ∣_∣₁ γ'} → _~₁ γ~ α α' → _~₁ (S Γ γ~) α' α}
  (S₀₁ : _≡_ {A = ∀{γ γ'}{γ~ : (Γ ~) γ γ'}{α : ∣_∣₁ γ}{α' : ∣_∣₁ γ'} → _~₁ γ~ α α' → _~₁ (S Γ γ~) α' α}
    (tr2
      (λ ∣_∣ _~ → ∀{γ γ'}{γ~ : (Γ Con.~) γ γ'}{α : ∣_∣ γ}{α' : ∣_∣ γ'} → _~ γ~ α α' → _~ (S Γ γ~) α' α)
      ∣_∣₀₁ _~₀₁ S₀)
    S₁)
  {T₀ : ∀{γ γ' γ''}{γ~ : (Γ Con.~) γ γ'}{γ~' : (Γ Con.~) γ' γ''}{α : ∣_∣₀ γ}{α' : ∣_∣₀ γ'}{α'' : ∣_∣₀ γ''} → _~₀ γ~ α α' → _~₀ γ~' α' α'' → _~₀ (T Γ γ~ γ~') α α''}
  {T₁ : ∀{γ γ' γ''}{γ~ : (Γ Con.~) γ γ'}{γ~' : (Γ Con.~) γ' γ''}{α : ∣_∣₁ γ}{α' : ∣_∣₁ γ'}{α'' : ∣_∣₁ γ''} → _~₁ γ~ α α' → _~₁ γ~' α' α'' → _~₁ (T Γ γ~ γ~') α α''}
  (T₀₁ : _≡_ {A = ∀{γ γ' γ''}{γ~ : (Γ Con.~) γ γ'}{γ~' : (Γ Con.~) γ' γ''}{α : ∣_∣₁ γ}{α' : ∣_∣₁ γ'}{α'' : ∣_∣₁ γ''} → _~₁ γ~ α α' → _~₁ γ~' α' α'' → _~₁ (T Γ γ~ γ~') α α''}
    (tr2
      (λ ∣_∣ _~ → ∀{γ γ' γ''}{γ~ : (Γ Con.~) γ γ'}{γ~' : (Γ Con.~) γ' γ''}{α : ∣_∣ γ}{α' : ∣_∣ γ'}{α'' : ∣_∣ γ''} → _~ γ~ α α' → _~ γ~' α' α'' → _~ (T Γ γ~ γ~') α α'')
      ∣_∣₀₁ _~₀₁ T₀)
    T₁)
  {coeT₀ : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₀ γ → ∣_∣₀ γ'}
  {coeT₁ : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₁ γ → ∣_∣₁ γ'}
  (coeT₀₁ : _≡_ {A = {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₁ γ → ∣_∣₁ γ'}
    (tr2
      (λ ∣_∣ _~ → {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣ γ → ∣_∣ γ')
      ∣_∣₀₁ _~₀₁ coeT₀)
    coeT₁)
  {cohT₀ : {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ Con.~) γ γ')(α : ∣_∣₀ γ) → _~₀ γ~ α (coeT₀ γ~ α)}
  {cohT₁ : {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ Con.~) γ γ')(α : ∣_∣₁ γ) → _~₁ γ~ α (coeT₁ γ~ α)}
  (cohT₀₁ : cohT≡ {Γ = Γ} ∣_∣₀₁ _~₀₁ coeT₀₁ cohT₀ cohT₁) → 
  _≡_ {A = Ty Γ j}
      (record {∣_∣ = ∣_∣₀ ; _~ = _~₀ ; ~p = ~p₀ ; R = R₀ ; S = S₀ ; T = T₀ ; coeT = coeT₀ ; cohT = cohT₀})
      (record {∣_∣ = ∣_∣₁ ; _~ = _~₁ ; ~p = ~p₁ ; R = R₁ ; S = S₁ ; T = T₁ ; coeT = coeT₁ ; cohT = cohT₁})
Ty='' refl refl refl refl refl refl refl refl = refl

Ty=''' : ∀{i}{Γ : Con i}{j} → 
  {∣_∣₀ ∣_∣₁   : ∣ Γ ∣ → Set j}(∣_∣₀₁ : ∣_∣₀ ≡ ∣_∣₁)
  {_~₀ : ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣₀ γ → ∣_∣₀ γ' → Set j}
  {_~₁ : ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣₁ γ → ∣_∣₁ γ' → Set j}
  (_~₀₁ : _≡_ {A = ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣₁ γ → ∣_∣₁ γ' → Set j} (transport (λ ∣_∣ → ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣ γ → ∣_∣ γ' → Set j) ∣_∣₀₁ _~₀) _~₁)
  {~p₀ : isprop1 {A = Σ (Σ (Σ (⊤ × Con.∣ Γ ∣ × Con.∣ Γ ∣) λ (_ ,Σ γ ,Σ γ') → (Γ Con.~) γ γ') λ (_ ,Σ γ ,Σ _ ,Σ _) → ∣_∣₀ γ) λ (_ ,Σ γ' ,Σ _ ,Σ _) → ∣_∣₀ γ'} (λ (_ ,Σ γ~ ,Σ α ,Σ α') → _~₀ γ~ α α')}
  {~p₁ : isprop1 {A = Σ (Σ (Σ (⊤ × Con.∣ Γ ∣ × Con.∣ Γ ∣) λ (_ ,Σ γ ,Σ γ') → (Γ Con.~) γ γ') λ (_ ,Σ γ ,Σ _ ,Σ _) → ∣_∣₁ γ) λ (_ ,Σ γ' ,Σ _ ,Σ _) → ∣_∣₁ γ'} (λ (_ ,Σ γ~ ,Σ α ,Σ α') → _~₁ γ~ α α')}
  (~p₀₁ : _≡_
    (tr2
      (λ ∣_∣ _~ → isprop1 {A = Σ (Σ (Σ (⊤ × Con.∣ Γ ∣ × Con.∣ Γ ∣) λ (_ ,Σ γ ,Σ γ') → (Γ Con.~) γ γ') λ (_ ,Σ γ ,Σ _ ,Σ _) → ∣_∣ γ) λ (_ ,Σ γ' ,Σ _ ,Σ _) → ∣_∣ γ'} (λ (_ ,Σ γ~ ,Σ α ,Σ α') → _~ γ~ α α'))
      ∣_∣₀₁ _~₀₁ ~p₀)
    ~p₁)
  {R₀ : ∀{γ} α → _~₀ (R Γ γ) α α}
  {R₁ : ∀{γ} α → _~₁ (R Γ γ) α α}
  {S₀ : ∀{γ γ'}{γ~ : (Γ ~) γ γ'}{α : ∣_∣₀ γ}{α' : ∣_∣₀ γ'} → _~₀ γ~ α α' → _~₀ (S Γ γ~) α' α}
  {S₁ : ∀{γ γ'}{γ~ : (Γ ~) γ γ'}{α : ∣_∣₁ γ}{α' : ∣_∣₁ γ'} → _~₁ γ~ α α' → _~₁ (S Γ γ~) α' α}
  {T₀ : ∀{γ γ' γ''}{γ~ : (Γ Con.~) γ γ'}{γ~' : (Γ Con.~) γ' γ''}{α : ∣_∣₀ γ}{α' : ∣_∣₀ γ'}{α'' : ∣_∣₀ γ''} → _~₀ γ~ α α' → _~₀ γ~' α' α'' → _~₀ (T Γ γ~ γ~') α α''}
  {T₁ : ∀{γ γ' γ''}{γ~ : (Γ Con.~) γ γ'}{γ~' : (Γ Con.~) γ' γ''}{α : ∣_∣₁ γ}{α' : ∣_∣₁ γ'}{α'' : ∣_∣₁ γ''} → _~₁ γ~ α α' → _~₁ γ~' α' α'' → _~₁ (T Γ γ~ γ~') α α''}
  {coeT₀ : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₀ γ → ∣_∣₀ γ'}
  {coeT₁ : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₁ γ → ∣_∣₁ γ'}
  (coeT₀₁ : _≡_ {A = {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₁ γ → ∣_∣₁ γ'}
    (tr2
      (λ ∣_∣ _~ → {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣ γ → ∣_∣ γ')
      ∣_∣₀₁ _~₀₁ coeT₀)
    coeT₁)
  {cohT₀ : {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ Con.~) γ γ')(α : ∣_∣₀ γ) → _~₀ γ~ α (coeT₀ γ~ α)}
  {cohT₁ : {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ Con.~) γ γ')(α : ∣_∣₁ γ) → _~₁ γ~ α (coeT₁ γ~ α)} →
  _≡_ {A = Ty Γ j}
      (record {∣_∣ = ∣_∣₀ ; _~ = _~₀ ; ~p = ~p₀ ; R = R₀ ; S = S₀ ; T = T₀ ; coeT = coeT₀ ; cohT = cohT₀})
      (record {∣_∣ = ∣_∣₁ ; _~ = _~₁ ; ~p = ~p₁ ; R = R₁ ; S = S₁ ; T = T₁ ; coeT = coeT₁ ; cohT = cohT₁})
Ty=''' {_}{Γ}{_}{∣_∣} refl {_~} refl {~p₀ = ~p}{~p₁} ~p₀₁ {R₀}{R₁}{S₀}{S₁}{T₀}{T₁}{coeT} refl {cohT₀}{cohT₁} = Ty='' refl refl ~p₀₁ 
  (toProp (ispropΠi $ ispropΠ1 $ ap (λ z (γ ,Σ α) → z (_ ,Σ _ ,Σ _ ,Σ R Γ γ ,Σ α ,Σ α)) ~p) _ _)
  (toProp (ispropΠi $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠ1 $ ap (λ z (_ ,Σ γ~ ,Σ α ,Σ α' ,Σ _) → z (_ ,Σ S Γ γ~ ,Σ α' ,Σ α)) ~p) _ _)
  (toProp (ispropΠi $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠ1 $ ispropΠ1 $ ap (λ z (_ ,Σ γ~ ,Σ γ~' ,Σ α ,Σ _ ,Σ α'' ,Σ _ ,Σ _) → z (_ ,Σ T Γ γ~ γ~' ,Σ α ,Σ α'')) ~p) _ _)
  refl
  (toProp (ispropΠi $ ispropΠi1 $ ispropΠ1 $ ispropΠ1 $ ap (λ z (_ ,Σ γ~ ,Σ α) → z (_ ,Σ γ~ ,Σ α ,Σ coeT γ~ α)) ~p) _ _)

∣tr∣t' : ∀{i}{Γ : Con i}{j}
  {∣_∣   : ∣ Γ ∣ → Set j}
  {_~ : ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣ γ → ∣_∣ γ' → Set j}
  {~p₀ ~p₁ : isprop1 {A = Σ (Σ (Σ (⊤ × Con.∣ Γ ∣ × Con.∣ Γ ∣) λ (_ ,Σ γ ,Σ γ') → (Γ Con.~) γ γ') λ (_ ,Σ γ ,Σ _ ,Σ _) → ∣_∣ γ) λ (_ ,Σ γ' ,Σ _ ,Σ _) → ∣_∣ γ'}
             (λ (_ ,Σ γ~ ,Σ α ,Σ α') → _~ γ~ α α')}
  (~p₀₁ : _≡_ ~p₀ ~p₁)
  {R₀ R₁ : ∀{γ} α → _~ (R Γ γ) α α}
  (R₀₁ : _≡_ {A =  ∀{γ} α → _~ (R Γ γ) α α} R₀ R₁)
  {S₀ S₁ : ∀{γ γ'}{p : (Γ Con.~) γ γ'}{α : ∣_∣ γ}{α' : ∣_∣ γ'}
          → _~ p α α' → _~ (S Γ p) α' α}
  (S₀₁ : _≡_ {A = ∀{γ γ'}{p : (Γ Con.~) γ γ'}{α : ∣_∣ γ}{α' : ∣_∣ γ'} → _~ p α α' → _~ (S Γ p) α' α} S₀ S₁)
  {T₀ T₁ : ∀{γ γ' γ''}{p : (Γ Con.~) γ γ'}{q : (Γ Con.~) γ' γ''}
            {α : ∣_∣ γ}{α' : ∣_∣ γ'}{α'' : ∣_∣ γ''}
          → _~ p α α' → _~ q α' α'' → _~ (T Γ p q) α α''}
  (T₀₁ : _≡_ {A = ∀{γ γ' γ''}{p : (Γ Con.~) γ γ'}{q : (Γ Con.~) γ' γ''}{α : ∣_∣ γ}{α' : ∣_∣ γ'}{α'' : ∣_∣ γ''} → _~ p α α' → _~ q α' α'' → _~ (T Γ p q) α α''} T₀ T₁)
  {coeT    : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣ γ → ∣_∣ γ'}
  {cohT    : {γ γ' : Con.∣ Γ ∣}(p : (Γ Con.~) γ γ')(α : ∣_∣ γ) → _~ p α (coeT p α)} →
  {t : Tm Γ (record {∣_∣ = ∣_∣ ; _~ = _~ ; ~p = ~p₀ ; R = R₀ ; S = S₀ ; T = T₀ ; coeT = coeT ; cohT = cohT})} →
  Tm.∣ transport (Tm Γ) (Ty=' ~p₀₁ R₀₁ S₀₁ T₀₁) t ∣ ≡ Tm.∣ t ∣
∣tr∣t' refl refl refl refl = refl

∣tr∣t : ∀{i}{Γ : Con i}{j}
  {∣_∣   : ∣ Γ ∣ → Set j}
  {_~ : ∀{γ γ'}(p : (Γ ~) γ γ') → ∣_∣ γ → ∣_∣ γ' → Set j}
  {~p₀ ~p₁ : isprop1 {A = Σ (Σ (Σ (⊤ × Con.∣ Γ ∣ × Con.∣ Γ ∣) λ (_ ,Σ γ ,Σ γ') → (Γ Con.~) γ γ') λ (_ ,Σ γ ,Σ _ ,Σ _) → ∣_∣ γ) λ (_ ,Σ γ' ,Σ _ ,Σ _) → ∣_∣ γ'}
             (λ (_ ,Σ γ~ ,Σ α ,Σ α') → _~ γ~ α α')}
  (~p₀₁ : _≡_ ~p₀ ~p₁)
  {R₀ R₁ : ∀{γ} α → _~ (R Γ γ) α α}
  {S₀ S₁ : ∀{γ γ'}{p : (Γ Con.~) γ γ'}{α : ∣_∣ γ}{α' : ∣_∣ γ'}
          → _~ p α α' → _~ (S Γ p) α' α}
  {T₀ T₁ : ∀{γ γ' γ''}{p : (Γ Con.~) γ γ'}{q : (Γ Con.~) γ' γ''}
            {α : ∣_∣ γ}{α' : ∣_∣ γ'}{α'' : ∣_∣ γ''}
          → _~ p α α' → _~ q α' α'' → _~ (T Γ p q) α α''}
  {coeT    : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣ γ → ∣_∣ γ'}
  {cohT    : {γ γ' : Con.∣ Γ ∣}(p : (Γ Con.~) γ γ')(α : ∣_∣ γ) → _~ p α (coeT p α)} →
  {t : Tm Γ (record {∣_∣ = ∣_∣ ; _~ = _~ ; ~p = ~p₀ ; R = R₀ ; S = S₀ ; T = T₀ ; coeT = coeT ; cohT = cohT})} →
  Tm.∣ transport (Tm Γ) (Ty= ~p₀₁ {R₀}{R₁}{S₀}{S₁}{T₀}{T₁}) t ∣ ≡ Tm.∣ t ∣
∣tr∣t {_}{Γ}{_}{∣_∣T_}{_T_⊢_~_}{~p₀ = ~p}{~p₁} ~p₀₁ {R₀}{R₁}{S₀}{S₁} = ∣tr∣t' ~p₀₁
  (toProp (ispropΠi $ ispropΠ1 $ ap (λ z (γ ,Σ α) → z (_ ,Σ _ ,Σ _ ,Σ R Γ γ ,Σ α ,Σ α)) ~p) _ _)
  (toProp (ispropΠi $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠ1 $ ap (λ z (_ ,Σ γ~ ,Σ α ,Σ α' ,Σ _) → z (_ ,Σ S Γ γ~ ,Σ α' ,Σ α)) ~p) _ _)
  (toProp (ispropΠi $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠi1 $ ispropΠ1 $ ispropΠ1 $ ap (λ z (_ ,Σ γ~ ,Σ γ~' ,Σ α ,Σ _ ,Σ α'' ,Σ _ ,Σ _) → z (_ ,Σ T Γ γ~ γ~' ,Σ α ,Σ α'')) ~p) _ _)

Tms=' : ∀{i j}{Γ : Con i}{Δ : Con j}
  {∣_∣₀ ∣_∣₁ : ∣ Γ ∣ → ∣ Δ ∣}
  (∣_∣₀₁ : ∣_∣₀ ≡ ∣_∣₁)
  {~s₀ : {γ γ' : ∣ Γ ∣} → (Γ ~) γ γ' → (Δ ~) (∣_∣₀ γ) (∣_∣₀ γ')}
  {~s₁ : {γ γ' : ∣ Γ ∣} → (Γ ~) γ γ' → (Δ ~) (∣_∣₁ γ) (∣_∣₁ γ')}
  (~s₀₁ : _≡_ {A = {γ γ' : ∣ Γ ∣} → (Γ ~) γ γ' → (Δ ~) (∣_∣₁ γ) (∣_∣₁ γ')} (transport (λ ∣_∣ → {γ γ' : Con.∣ Γ ∣} → (Γ ~) γ γ' → (Δ ~) (∣_∣ γ) (∣_∣ γ')) ∣_∣₀₁ ~s₀) ~s₁) →
  _≡_ {A = Tms Γ Δ}
    (record { ∣_∣ = ∣_∣₀ ; _~ = ~s₀})
    (record { ∣_∣ = ∣_∣₁ ; _~ = ~s₁})
Tms=' refl refl = refl

Tms= : ∀{i j}{Γ : Con i}{Δ : Con j}
  {∣_∣₀ ∣_∣₁ : ∣ Γ ∣ → ∣ Δ ∣}
  (∣_∣₀₁ : ∣_∣₀ ≡ ∣_∣₁)
  {~s₀ : {γ γ' : ∣ Γ ∣} → (Γ ~) γ γ' → (Δ ~) (∣_∣₀ γ) (∣_∣₀ γ')}
  {~s₁ : {γ γ' : ∣ Γ ∣} → (Γ ~) γ γ' → (Δ ~) (∣_∣₁ γ) (∣_∣₁ γ')} → 
  _≡_ {A = Tms Γ Δ}
    (record { ∣_∣ = ∣_∣₀ ; _~ = ~s₀})
    (record { ∣_∣ = ∣_∣₁ ; _~ = ~s₁})
Tms= {Δ = Δ}{∣_∣₀}{∣_∣₁} ∣_∣₀₁ = Tms=' ∣_∣₀₁
  (toProp (ispropΠi $ ispropΠi1 $ ispropΠ1 $ ap (λ z (γ ,Σ γ' ,Σ _) → z (_ ,Σ ∣_∣₁ γ ,Σ ∣_∣₁ γ')) (~p Δ)) _ _)

Tm=' : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}
  {∣_∣₀ ∣_∣₁ : (γ : ∣ Γ ∣) → ∣ A ∣ γ}
  (∣_∣₀₁ : ∣_∣₀ ≡ ∣_∣₁)
  {_~₀ : {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ ~) γ γ') → (A ~) γ~ (∣_∣₀ γ) (∣_∣₀ γ')}
  {_~₁ : {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ ~) γ γ') → (A ~) γ~ (∣_∣₁ γ) (∣_∣₁ γ')}
  (_~₀₁ : _≡_ {A = {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ ~) γ γ') → (A ~) γ~ (∣_∣₁ γ) (∣_∣₁ γ')} (transport (λ ∣_∣ → {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ ~) γ γ') → (A ~) γ~ (∣_∣ γ) (∣_∣ γ')) ∣_∣₀₁ _~₀) _~₁) →
  _≡_ {A = Tm Γ A} (record { ∣_∣ = ∣_∣₀ ; _~ = _~₀ }) (record { ∣_∣ = ∣_∣₁ ; _~ = _~₁ })
Tm=' refl refl = refl

Tm= : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}
  {∣_∣₀ ∣_∣₁ : (γ : ∣ Γ ∣) → ∣ A ∣ γ}
  (∣_∣₀₁ : ∣_∣₀ ≡ ∣_∣₁)
  {_~₀ : {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ ~) γ γ') → (A ~) γ~ (∣_∣₀ γ) (∣_∣₀ γ')}
  {_~₁ : {γ γ' : Con.∣ Γ ∣}(γ~ : (Γ ~) γ γ') → (A ~) γ~ (∣_∣₁ γ) (∣_∣₁ γ')} →
  _≡_ {A = Tm Γ A} (record { ∣_∣ = ∣_∣₀ ; _~ = _~₀ }) (record { ∣_∣ = ∣_∣₁ ; _~ = _~₁ })
Tm= {A = A} {∣_∣₀}{∣_∣₁} ∣_∣₀₁ = Tm=' ∣_∣₀₁
  (toProp (ispropΠi $ ispropΠi1 $ ispropΠ1 (ap (λ z (γ ,Σ γ' ,Σ γ~) → z (_ ,Σ γ~ ,Σ (∣_∣₁ γ) ,Σ (∣_∣₁ γ'))) (~p A))) _ _)

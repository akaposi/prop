{-# OPTIONS --without-K #-}

module Model.Func where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core

Π : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▷ A) k) → Ty Γ (j ⊔ k)
∣ Π {Γ = Γ} A B ∣ γ = Σ
  ((α : ∣ A ∣ γ) → ∣ B ∣ (γ ,Σ α)) λ f →
  {α α' : ∣ A ∣ γ}(α~ : (A ~) (R Γ γ) α α') → (B ~) (R Γ γ ,Σ α~) (f α) (f α')
(Π {Γ = Γ} A B ~) {γ}{γ'} γ~ (f ,Σ _) (f' ,Σ _) = {α : ∣ A ∣ γ}{α' : ∣ A ∣ γ'}(α~ : (A ~) γ~ α α') → (B ~) (γ~ ,Σ α~) (f α) (f' α')
~p (Π {Γ = Γ} A B) = ispropΠi1 (ispropΠi1 (ispropΠ1 (ap
  (λ { z (_ ,Σ γ ,Σ γ' ,Σ γ~ ,Σ (f ,Σ _) ,Σ (f' ,Σ _) ,Σ α ,Σ α' ,Σ α~) →
    z (_ ,Σ (γ ,Σ α) ,Σ (γ' ,Σ α') ,Σ (γ~ ,Σ α~) ,Σ f α ,Σ f' α') })
  (~p B))))
R (Π {Γ = Γ} A B) {γ} (f ,Σ f~) = f~
S (Π {Γ = Γ} A B) {γ}{γ'}{γ~}{f}{f'} f~ {α}{α'} α~ = transport
  (λ z → (B ~) (S Γ γ~ ,Σ z) (proj₁ f' α) (proj₁ f α'))
  (toProp1 (~p A) _ _)
  (S B (f~ (transport (λ z → (A ~) z α' α) (toProp1 (~p Γ) _ _) (S A α~))))
T (Π {Γ = Γ} A B) {γ}{γ'}{γ''}{γ~}{γ~'}{f}{f'}{f''} f~ f~' {α}{α''} α~ = transport
  (λ z → (B ~) z (proj₁ f α) (proj₁ f'' α''))
  (toProp1 (~p (Γ ▷ A)) _ _)
  (T B (f~ (cohT A γ~ α)) (f~' (transport (λ z → (A ~) z (coeT A γ~ α) α'') (toProp1 (~p Γ) _ _) (T A (S A (cohT A γ~ α)) α~))))
coeT (Π {Γ = Γ} A B) {γ}{γ'} γ~ (f ,Σ f~) =
  (λ α' → coeT B (γ~ ,Σ cohT* A γ~ α') (f (coeT A (S Γ γ~) α'))) ,Σ
  (λ {α}{α'} α~ → transport
    (λ z → (B ~) z (coeT B (γ~ ,Σ cohT* A γ~ α) (f (coeT A (S Γ γ~) α))) (coeT B (γ~ ,Σ cohT* A γ~ α') (f (coeT A (S Γ γ~) α'))))
    (toProp1 (~p (Γ ▷ A)) _ _)
    (TT3 B
      (S B (cohT B (γ~ ,Σ cohT* A γ~ α) (f (coeT* A γ~ α))))
      (f~ (transport (λ z → (A ~) z (coeT* A γ~ α) (coeT A (S Γ γ~) α')) (toProp1 (~p Γ) _ _) (T A (cohT* A γ~ α) (T A α~ (cohT A (S Γ γ~) α')))))
      (cohT B (γ~ ,Σ cohT* A γ~ α') (f (coeT* A γ~ α')))))
cohT (Π {Γ = Γ} A B) {γ}{γ'} γ~ (f ,Σ f~) {α}{α'} α~ = transport
  (λ z → (B ~) z (f α) (coeT B (γ~ ,Σ cohT* A γ~ α') (f (coeT A (S Γ γ~) α'))))
  (toProp1 (~p (Γ ▷ A)) _ _)
  (T B
    (f~ (transport (λ z → (A ~) z α (coeT A (S Γ γ~) α')) (toProp1 (~p Γ) _ _) (T A α~ (cohT A (S Γ γ~) α'))))
    (cohT B (γ~ ,Σ cohT* A γ~ α') (f (coeT A (S Γ γ~) α'))))

lam : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}(t : Tm (Γ ▷ A) B) → Tm Γ (Π A B)
∣ lam {Γ = Γ} t ∣ γ = (λ α → ∣ t ∣ (γ ,Σ α)) ,Σ (λ α~ → (t ~) (R Γ γ ,Σ α~))
(lam t ~) γ~ α~ = (t ~) (γ~ ,Σ α~)

app : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}(t : Tm Γ (Π A B)) → Tm (Γ ▷ A) B
∣ app t ∣ (γ ,Σ α) = proj₁ (∣ t ∣ γ) α
(app t ~) (γ~ ,Σ α~) = (t ~) γ~ α~

Πβ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{t : Tm (Γ ▷ A) B} → _≡_ {A = Tm (Γ ▷ A) B} (app {A = A}{B = B} (lam {A = A}{B = B} t)) t
Πβ = refl

Πη : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{t : Tm Γ (Π A B)} → _≡_ {A = Tm Γ (Π A B)} (lam {A = A}{B = B} (app {A = A}{B = B} t)) t
Πη {Γ = Γ}{A = A}{B = B}{t = t} = Tm= (ap (λ z γ → (λ α → proj₁ (∣ t ∣ γ) α) ,Σ (λ {α}{α'} α~ → z _ ((t ~) (R Γ γ) α~) (proj₂ (∣ t ∣ γ) α~))) (~p B))

{-
Π[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{l}{Θ : Con l}{γ : Tms Θ Γ} →
  (Π A B) [ γ ]T ≡ Π (A [ γ ]T) (B [ γ ^ A ]T)
Π[] {i} {Γ} {j} {A} {k} {B} {l} {Θ} {γ} = Ty='''
  (ap (λ z θₓ → let γ₌ = z _ (R Γ (∣ γ ∣ θₓ)) ((γ ~) (R Θ θₓ)) in Σ
      (((α : ∣ A ∣ (∣ γ ∣ θₓ)) → ∣ B ∣ (∣ γ ∣ θₓ ,Σ α)))
      (λ f → {α α' : ∣ A ∣ (∣ γ ∣ θₓ)} (α~ : (A ~) γ₌ α α') →
         (B ~) (γ₌ ,Σ α~) (f α) (f α'))
  ) (~p Γ))
  {! !}
  {! !}
  {! !}
-}

_⇒_ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty Γ k) → Ty Γ (j ⊔ k)
A ⇒ B = Π A (B [ wk {A = A} ]T)

open import Model.Const

-- we need the following to define ModelStrict.Sigma. these are
-- special cases of app and lam, but are typed slightly differently
-- (the substitutions are in the right place). this is a trick to
-- avoid some transports in ModelStrict.

app-vz : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{Δ : Con k} →
  Tm (Γ ▷ (A ⇒ K Δ) ▷ A [ wk {A = A ⇒ K Δ} ]T) (K Δ)
∣ app-vz ∣ (γₓ ,Σ (f ,Σ _) ,Σ aₓ) = f aₓ
(app-vz ~) {γ = γ₀ ,Σ (f₀ ,Σ _) ,Σ a₀}{γ₁ ,Σ (f₁ ,Σ _) ,Σ a₁} (γ₀₁ ,Σ f₀₁ ,Σ a₀₁) = f₀₁ a₀₁

lamK : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{A : Ty Θ k}{θ : Tms Γ Θ}{l}{Δ : Con l} →
  Tm (Γ ▷ A [ θ ]T) (K Δ) → Tm Γ (A ⇒ K Δ [ θ ]T)
∣ lamK {Γ = Γ}{Θ = Θ}{A = A} t ∣ γₓ =
  (λ aₓ → ∣ t ∣ (γₓ ,Σ aₓ)) ,Σ
  λ {a₀}{a₁} a₀₁ → (t ~) (R Γ γₓ ,Σ transport (λ z → (A ~) z a₀ a₁) (toProp1 (~p Θ) _ _) a₀₁)
(lamK t ~) γ₀₁ a₀₁ = (t ~) (γ₀₁ ,Σ a₀₁)

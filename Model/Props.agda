{-# OPTIONS --without-K #-}

module Model.Props where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core

record TyP {i}(Γ : Con i) j : Set (i ⊔ lsuc j) where
  field
    ∣_∣  : ∣ Γ ∣ → Set j
    ∣p   : isprop1 {A = Con.∣ Γ ∣} ∣_∣
    coeT : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣ γ → ∣_∣ γ'
  infix 4 ∣_∣
open TyP public

TyP=' : ∀{i}{Γ : Con i}{j}
  {∣_∣₀ ∣_∣₁ : ∣ Γ ∣ → Set j}(∣_∣₌ : ∣_∣₀ ≡ ∣_∣₁)
  {∣p₀ : isprop1 {A = Con.∣ Γ ∣} ∣_∣₀}{∣p₁ : isprop1 {A = Con.∣ Γ ∣} ∣_∣₁}
  (∣p₌ : transport (isprop1 {A = Con.∣ Γ ∣}) ∣_∣₌ ∣p₀ ≡ ∣p₁) →
  {coeT₀ : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₀ γ → ∣_∣₀ γ'}
  {coeT₁ : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₁ γ → ∣_∣₁ γ'}
  (coeT₌ : _≡_ {A = {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₁ γ → ∣_∣₁ γ'}
    (transport (λ z → {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → z γ → z γ') ∣_∣₌ coeT₀)
    coeT₁) → 
  _≡_ {A = TyP Γ j}
      (record { ∣_∣ = ∣_∣₀ ; ∣p = ∣p₀ ; coeT = coeT₀ })
      (record { ∣_∣ = ∣_∣₁ ; ∣p = ∣p₁ ; coeT = coeT₁ })
TyP=' refl refl refl = refl

TyP= : ∀{i}{Γ : Con i}{j}
  {∣_∣₀ ∣_∣₁ : ∣ Γ ∣ → Set j}(∣_∣₌ : ∣_∣₀ ≡ ∣_∣₁)
  {∣p₀ : isprop1 {A = Con.∣ Γ ∣} ∣_∣₀}{∣p₁ : isprop1 {A = Con.∣ Γ ∣} ∣_∣₁}
  (∣p₌ : transport (isprop1 {A = Con.∣ Γ ∣}) ∣_∣₌ ∣p₀ ≡ ∣p₁) →
  {coeT₀ : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₀ γ → ∣_∣₀ γ'}
  {coeT₁ : {γ γ' : Con.∣ Γ ∣} → (Γ Con.~) γ γ' → ∣_∣₁ γ → ∣_∣₁ γ'} →
  _≡_ {A = TyP Γ j}
      (record { ∣_∣ = ∣_∣₀ ; ∣p = ∣p₀ ; coeT = coeT₀ })
      (record { ∣_∣ = ∣_∣₁ ; ∣p = ∣p₁ ; coeT = coeT₁ })
TyP= ∣_∣₌ {∣p₀}{∣p₁} ∣p₌ = TyP=' ∣_∣₌ ∣p₌ (toProp (ispropΠi (ispropΠi1 (ispropΠ1 (ispropΠ1 (ap (λ z → λ { (_ ,Σ γ₁ ,Σ _ ,Σ _) → z γ₁ }) ∣p₁ ))))) _ _)

_[_]TP : ∀{i}{Γ : Con i}{j} → TyP Γ j → ∀{k}{Δ : Con k} → Tms Δ Γ → TyP Δ j
∣ A [ γ ]TP ∣ δₓ = ∣ A ∣ (∣ γ ∣ δₓ)
∣p (A [ γ ]TP) = ap (λ z _ a₀ a₁ → z _ a₀ a₁) (∣p A)
coeT (A [ γ ]TP) δ₀₁ a₀ = coeT A ((γ ~) δ₀₁) a₀

[id]TP : ∀{i}{Γ : Con i}{j}{A : TyP Γ j} → A [ id ]TP ≡ A
[id]TP {Γ = Γ}{A = A} = TyP= refl (apid (∣p A))

[][]TP : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Δ : Con k}{l}{A : TyP Δ l}{σ : Tms Θ Δ}{δ : Tms Γ Θ} → (A [ σ ]TP [ δ ]TP) ≡ (A [ σ ∘ δ ]TP)
[][]TP {A = A}{σ = σ}{δ = δ} = TyP= refl (apap (∣p A) ⁻¹)

↑_ : ∀{i}{Γ : Con i}{j} → TyP Γ j → Ty Γ j
∣ ↑ A ∣ = ∣ A ∣
((↑ A) ~) _ _ _ = Lift ⊤
~p (↑ A) = refl
R (↑ A) _ = lift tt
S (↑ A) _ = lift tt
T (↑ A) _ _ = lift tt
coeT (↑ A) = coeT A
cohT (↑ A) _ _ = lift tt

irr : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}(u v : Tm Γ (↑ A)) → u ≡ v
irr {A = A} u v = Tm= (ap (λ z → λ γ → z γ (∣ u ∣ γ) (∣ v ∣ γ)) (∣p A))

⊤' : ∀{i}{Γ : Con i}{j} → TyP Γ j
∣ ⊤' ∣ _ = Lift ⊤
∣p ⊤' = refl
coeT ⊤' _ _ = lift tt

⊤'[] : ∀{i}{Γ : Con i}{j}{k}{Δ : Con k}{γ : Tms Δ Γ} → ⊤' {Γ = Γ}{j = j} [ γ ]TP ≡ ⊤' {Γ = Δ}
⊤'[] = refl

tt' : ∀{i}{Γ : Con i}{j} → Tm Γ (↑ (⊤' {j = j}))
∣ tt' ∣ _ = lift tt
(tt' ~) _ = lift tt

Π : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : TyP (Γ ▷ A) k) → TyP Γ (j ⊔ k)
∣ Π A B ∣ γₓ = (aₓ : ∣ A ∣ γₓ) → ∣ B ∣ (γₓ ,Σ aₓ)
∣p (Π A B) = ispropΠ1 (∣p B)
coeT (Π {Γ = Γ} A B) γ₀₁ f₀ a₁ = coeT B (γ₀₁ ,Σ cohT* A γ₀₁ a₁) (f₀ (coeT* A γ₀₁ a₁))

lam : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : TyP (Γ ▷ A) k}(t : Tm (Γ ▷ A) (↑ B)) → Tm Γ (↑ Π A B)
∣ lam t ∣ γₓ aₓ = ∣ t ∣ (γₓ ,Σ aₓ)
(lam t ~) _ = lift tt

app : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : TyP (Γ ▷ A) k}(t : Tm Γ (↑ Π A B)) → Tm (Γ ▷ A) (↑ B)
∣ app t ∣ (γₓ ,Σ aₓ) = ∣ t ∣ γₓ aₓ
(app t ~) _ = lift tt

Σ' : ∀{i}{Γ : Con i}{j}(A : TyP Γ j){k}(B : TyP (Γ ▷ ↑ A) k) → TyP Γ (j ⊔ k)
∣ Σ' A B ∣ γₓ = Σ (∣ A ∣ γₓ) λ aₓ → ∣ B ∣ (γₓ ,Σ aₓ)
∣p (Σ' A B) = ispropΣ1 (∣p A) (∣p B)
coeT (Σ' A B) γ₀₁ (a₀ ,Σ b₀) = coeT A γ₀₁ a₀ ,Σ coeT B (γ₀₁ ,Σ lift tt) b₀

mk : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{k}{B : TyP (Γ ▷ ↑ A) k} → Tms (Γ ▷ ↑ A ▷ ↑ B) (Γ ▷ ↑ (Σ' A B))
∣ mk ∣ (γₓ ,Σ aₓ ,Σ bₓ) = (γₓ ,Σ (aₓ ,Σ bₓ))
(mk ~) (γ₀₁ ,Σ _ ,Σ _) = γ₀₁ ,Σ lift tt

un : ∀{i}{Γ : Con i}{j}{A : TyP Γ j}{k}{B : TyP (Γ ▷ ↑ A) k} → Tms (Γ ▷ ↑ (Σ' A B)) (Γ ▷ ↑ A ▷ ↑ B)
∣ un ∣ (γₓ ,Σ (aₓ ,Σ bₓ)) = (γₓ ,Σ aₓ ,Σ bₓ)
(un ~) (γ₀₁ ,Σ _) = γ₀₁ ,Σ lift tt ,Σ lift tt

Id : ∀{i}{Γ : Con i}{j}(A : Ty Γ j) → Tm Γ A → Tm Γ A → TyP Γ j
∣ Id {Γ = Γ} A a a' ∣ γₓ = (A ~) (R Γ γₓ) (∣ a ∣ γₓ) (∣ a' ∣ γₓ)
∣p (Id {Γ = Γ} A a a') = ap (λ z → λ γₓ → z (tt ,Σ γₓ ,Σ γₓ ,Σ R Γ γₓ ,Σ ∣ a ∣ γₓ ,Σ ∣ a' ∣ γₓ)) (~p A)
coeT (Id {Γ = Γ} A a a') {γ₀}{γ₁} γ₀₁ e = transport (λ z → (A ~) z (∣ a ∣ γ₁) (∣ a' ∣ γ₁)) (toProp1 (~p Γ) _ _) (TT3 A ((a ~) (S Γ γ₀₁)) e ((a' ~) γ₀₁))

refl' : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}(a : Tm Γ A) → Tm Γ (↑ Id A a a)
∣ refl' {A = A} a ∣ γₓ = R A (∣ a ∣ γₓ)
(refl' a ~) _ = lift tt

transp : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}(P : Ty (Γ ▷ A) k){a a' : Tm Γ A}(a₀₁ : Tm Γ (↑ Id A a a')) →
  Tm Γ (P [ < a > ]T) →
  Tm Γ (P [ < a' > ]T)
∣ transp {Γ = Γ} {A = A} P e t ∣ γₓ = coeT P (R Γ γₓ ,Σ ∣ e ∣ γₓ) (∣ t ∣ γₓ)
(transp {Γ = Γ} {A = A} P e t ~) {γ₀}{γ₁} γ₀₁ = transport (λ z → (P ~) z (coeT P (R Γ γ₀ ,Σ ∣ e ∣ γ₀) (∣ t ∣ γ₀)) (coeT P (R Γ γ₁ ,Σ ∣ e ∣ γ₁) (∣ t ∣ γ₁))) (toProp1 (~p (Γ ▷ A)) _ _)
  (TT3 P (S P (cohT P (R Γ γ₀ ,Σ ∣ e ∣ γ₀) (∣ t ∣ γ₀))) ((t ~) γ₀₁) (cohT P (R Γ γ₁ ,Σ ∣ e ∣ γ₁) (∣ t ∣ γ₁)))

{-
-- not definable
P : ∀{j}{Γ : Con j} i → Ty Γ (lsuc i)
∣ P i ∣ _ = Σ (Set i) λ A → isprop1 {A = ⊤} (λ _ → A)
(P i ~) γ₀₁ (A ,Σ _) (A' ,Σ _) = Lift ((A → A') × (A' → A))
~p (P i) = ispropLift1 (ispropΣ1 (ispropΠ1 {!!}) (ispropΠ1 {!!}))
R (P i) (A ,Σ _) = lift ((λ x → x) ,Σ (λ x → x))
S (P i) (lift (f ,Σ g)) = (lift (g ,Σ f))
T (P i) (lift (f ,Σ g)) (lift (f' ,Σ g')) = lift ((λ x → f' (f x)) ,Σ (λ x → g (g' x)))
coeT (P i) γ₀₁ a = a
cohT (P i) γ₀₁ a = lift ((λ x → x) ,Σ (λ x → x))
-}

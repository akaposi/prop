{-# OPTIONS --without-K #-}

module Model.Sigma where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core

~p-deps-proj₁ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▷ A) k) → ~p-deps Γ (λ γₓ → Σ (∣ A ∣ γₓ) λ aₓ → ∣ B ∣ (γₓ ,Σ aₓ)) → ~p-deps Γ ∣ A ∣
~p-deps-proj₁ A B (tt ,Σ γ₀ ,Σ γ₁ ,Σ γ₀₁ ,Σ (a₀ ,Σ b₀) ,Σ (a₁ ,Σ b₁)) = (tt ,Σ γ₀ ,Σ γ₁ ,Σ γ₀₁ ,Σ a₀ ,Σ a₁)

~p-deps-proj₂ : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▷ A) k) →
  Σ (~p-deps Γ (λ γₓ → Σ (∣ A ∣ γₓ) λ aₓ → ∣ B ∣ (γₓ ,Σ aₓ))) (λ d → (A ~) (proj₂ (proj₁ (proj₁ d))) (proj₁ (proj₂ (proj₁ d))) (proj₁ (proj₂ d))) →
  ~p-deps (Γ ▷ A) ∣ B ∣
~p-deps-proj₂ A B ((tt ,Σ γ₀ ,Σ γ₁ ,Σ γ₀₁ ,Σ (a₀ ,Σ b₀) ,Σ (a₁ ,Σ b₁)) ,Σ a₀₁) = (tt ,Σ (γ₀ ,Σ a₀) ,Σ (γ₁ ,Σ a₁) ,Σ (γ₀₁ ,Σ a₀₁) ,Σ b₀ ,Σ b₁)

Σ' : ∀{i}{Γ : Con i}{j}(A : Ty Γ j){k}(B : Ty (Γ ▷ A) k) → Ty Γ (j ⊔ k)
∣ Σ' A B ∣    = λ γₓ → Σ (∣ A ∣ γₓ) λ aₓ → ∣ B ∣ (γₓ ,Σ aₓ)
Σ' A B ~      = λ { γ₀₁ (a₀ ,Σ b₀) (a₁ ,Σ b₁) → Σ ((A ~) γ₀₁ a₀ a₁) λ a₀₁ → (B ~) (γ₀₁ ,Σ a₀₁) b₀ b₁ }
~p (Σ' A B)   = ispropΣ1 (ap (λ z d → z (~p-deps-proj₁ A B d)) (~p A)) (ap (λ z d → z (~p-deps-proj₂ A B d)) (~p B))
R (Σ' A B)    = λ { (aₓ ,Σ bₓ) → R A aₓ ,Σ R B bₓ }
S (Σ' A B)    = λ { (a₀₁ ,Σ b₀₁) → (S A a₀₁ ,Σ S B b₀₁) }
T (Σ' A B)    = λ { (a₀₁ ,Σ b₀₁) (a₁₂ ,Σ b₁₂) → (T A a₀₁ a₁₂ ,Σ T B b₀₁ b₁₂) }
coeT (Σ' A B) = λ { γ₀₁ (a₀ ,Σ b₀) → coeT A γ₀₁ a₀ ,Σ coeT B (γ₀₁ ,Σ cohT A γ₀₁ a₀) b₀ }
cohT (Σ' A B) = λ { γ₀₁ (a₀ ,Σ b₀) → cohT A γ₀₁ a₀ ,Σ cohT B (γ₀₁ ,Σ cohT A γ₀₁ a₀) b₀ }

_,'_ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k} → (a : Tm Γ A) → Tm Γ (B [ < a > ]T) → Tm Γ (Σ' A B)
∣ a ,' b ∣ γₓ = ∣ a ∣ γₓ ,Σ ∣ b ∣ γₓ
((a ,' b) ~) γ₀₁ = (a ~) γ₀₁ ,Σ (b ~) γ₀₁

proj₁' : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k} → Tm Γ (Σ' A B) → Tm Γ A
∣ proj₁' ab ∣ γₓ  = proj₁ (∣ ab ∣ γₓ)
(proj₁' ab ~) γ₀₁ = proj₁ ((ab ~) γ₀₁)

proj₂' : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}(ab : Tm Γ (Σ' A B)) → Tm Γ (B [ < proj₁' {B = B} ab > ]T)
∣ proj₂' ab ∣ γₓ  = proj₂ (∣ ab ∣ γₓ)
(proj₂' ab ~) γ₀₁ = proj₂ ((ab ~) γ₀₁)

Σβ₁ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{a : Tm Γ A}{b : Tm Γ (B [ < a > ]T)} → proj₁' {B = B} (_,'_ {B = B} a b) ≡ a
Σβ₁ = refl

Σβ₂ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{a : Tm Γ A}{b : Tm Γ (B [ < a > ]T)} → proj₂' {B = B} (_,'_ {B = B} a b) ≡ b
Σβ₂ = refl

Ση : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty (Γ ▷ A) k}{ab : Tm Γ (Σ' A B)} → _,'_ {B = B} (proj₁' {B = B} ab) (proj₂' {B = B} ab) ≡ ab
Ση = refl

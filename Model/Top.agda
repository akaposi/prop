{-# OPTIONS --without-K #-}

module Model.Top where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core

⊤' : ∀{i}{Γ : Con i}{j} → Ty Γ j
∣ ⊤' ∣ _ = Lift ⊤
(⊤' ~) _ _ _ = Lift ⊤
~p ⊤' = refl
R ⊤' _ = lift tt
S ⊤' _ = lift tt
T ⊤' _ _ = lift tt
coeT ⊤' _ b = b
cohT ⊤' _ _ = lift tt

tt' : ∀{i}{Γ : Con i}{j} → Tm Γ (⊤' {j = j})
∣ tt' ∣ _ = lift tt
(tt' ~) _ = lift tt

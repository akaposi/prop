{-# OPTIONS --without-K #-}

module ModelStrict.Bot where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core
open import Model.Bot
open import ModelStrict.CwF

⊥'' : ∀{i}{Γ : Con i}{j} → Ty' Γ j lzero
con ⊥'' = •
tms ⊥'' = ε
ty ⊥'' = ⊥'

⊥[] : ∀{i}{Γ : Con i}{j}{k}{Δ : Con k}{γ : Tms Δ Γ} → ⊥'' {j = j} [ γ ]T' ≡ ⊥''
⊥[] = refl

exfalso' : ∀{i}{Γ : Con i}{j k l}{A : Ty' Γ k l} → Tm' Γ (⊥'' {j = j}) → Tm' Γ A
∣ exfalso' t ∣ γₓ  = ⊥-elim (unlift (∣ t ∣ γₓ))
(exfalso' t ~) {γ₀} γ₀₁ = ⊥-elim (unlift (∣ t ∣ γ₀))

exfalso'[] : ∀{i}{Γ : Con i}{j k l}{A : Ty' Γ k l}{t : Tm' Γ (⊥'' {j = j})}{m}{Δ : Con m}{γ : Tms Δ Γ} →
  _[_]t' {A = A} (exfalso' {A = A} t) γ ≡ exfalso' {A = A [ γ ]T'} (_[_]t' {A = ⊥''} t γ)
exfalso'[] = refl

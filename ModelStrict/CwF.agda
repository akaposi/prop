{-# OPTIONS --without-K #-}

module ModelStrict.CwF where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core

infixl 7 _[_]T'
infixl 8 _[_]t'
infixl 5 _▷'_
infixl 5 _,'_

record Ty' {i}(Γ : Con i) j k : Set (i ⊔ lsuc j ⊔ lsuc k) where
  field
    con : Con k
    tms : Tms Γ con
    ty  : Ty con j
open Ty' public

_[_]T' : ∀{i}{Γ : Con i}{j}{k} → Ty' Γ j k → ∀{l}{Δ : Con l} → Tms Δ Γ → Ty' Δ j k
con (A [ γ ]T') = con A
tms (A [ γ ]T') = tms A ∘ γ
ty (A [ γ ]T') = ty A

[∘]T' : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Δ : Con k}{l m}{A : Ty' Δ l m}{σ : Tms Θ Δ}{δ : Tms Γ Θ} → (A [ σ ∘ δ ]T') ≡ (A [ σ ]T' [ δ ]T')
[∘]T' = refl

[id]T' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k} → A [ id ]T' ≡ A
[id]T' = refl


Tm' : ∀{i}(Γ : Con i){j k}(A : Ty' Γ j k) → Set (i ⊔ j)
Tm' Γ A = Tm Γ (ty A [ tms A ]T)

_[_]t' : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k l}{A : Ty' Δ k l} → Tm' Δ A → (σ : Tms Γ Δ) → Tm' Γ (A [ σ ]T')
t [ σ ]t' = record { ∣_∣ = λ γ → ∣ t ∣ (∣ σ ∣ γ) ; _~ = λ p → (t ~) ((σ ~) p) }

[∘]t' : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Δ : Con k}{l m}{A : Ty' Δ l m}{a : Tm' Δ A}{σ : Tms Θ Δ}{δ : Tms Γ Θ} →
  (_[_]t' {A = A} a (σ ∘ δ)) ≡ (_[_]t' {A = A [ σ ]T'} (_[_]t' {A = A} a σ) δ)
[∘]t' = refl

[id]t' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{a : Tm' Γ A} → _[_]t' {A = A} a id ≡ a
[id]t' = refl

_▷'_ : ∀{i}(Γ : Con i){j k} → Ty' Γ j k → Con (i ⊔ j)
Γ ▷' A = Γ ▷ ty A [ tms A ]T

_,'_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(γ : Tms Δ Γ){k l}{A : Ty' Γ k l} → Tm' Δ (A [ γ ]T') → Tms Δ (Γ ▷' A)
∣ γ ,' a ∣ δₓ = ∣ γ ∣ δₓ ,Σ ∣ a ∣ δₓ
((γ ,' a) ~) δ₀₁ = (γ ~) δ₀₁ ,Σ (a ~) δ₀₁

q' : ∀{i}{Γ : Con i}{k}{A : Ty Γ k} → Tm (Γ ▷ A) (A [ wk {A = A} ]T)
∣ q' ∣ = proj₂
q' ~ = proj₂

p : ∀{i}{Γ : Con i}{k l}{A : Ty' Γ k l} → Tms (Γ ▷' A) Γ
p {A = A} = wk {A = ty A [ tms A ]T}

q : ∀{i}{Γ : Con i}{k l}{A : Ty' Γ k l} → Tm' (Γ ▷' A) (A [ p {A = A} ]T')
∣ q ∣ = proj₂
q ~ = proj₂

▷β₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{γ : Tms Δ Γ}{k l}{A : Ty' Γ k l}{a : Tm' Δ (A [ γ ]T')} →
  p {A = A} ∘ (_,'_ γ {A = A} a) ≡ γ
▷β₁ = refl

▷β₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{γ : Tms Δ Γ}{k l}{A : Ty' Γ k l}{a : Tm' Δ (A [ γ ]T')} →
  _[_]t' {A = A [ p {A = A} ]T'} (q {A = A}) (_,'_ γ {A = A} a) ≡ a
▷β₂ = refl

▷η : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k l}{A : Ty' Γ k l}{γa : Tms Δ (Γ ▷' A)} →
  _,'_ (p {A = A} ∘ γa) {A = A} (_[_]t' {A = A [ p {A = A} ]T'} (q {A = A}) γa) ≡ γa
▷η = refl

_^'_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(δ : Tms Γ Δ){k l}(A : Ty' Δ k l) → Tms (Γ ▷' A [ δ ]T') (Δ ▷' A)
∣ δ ^' A ∣   (γₓ ,Σ aₓ) = ∣ δ ∣ γₓ ,Σ aₓ
((δ ^' A) ~) (γ~ ,Σ a~) = (δ ~) γ~ ,Σ a~

<_>' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k} → Tm' Γ A → Tms Γ (Γ ▷' A)
∣ < t >' ∣ γₓ = γₓ ,Σ ∣ t ∣ γₓ
(< t >' ~) γ₀₁ = γ₀₁ ,Σ (t ~) γ₀₁

Ty'= : ∀{i}{Γ : Con i}{j}{k} →
  {con₀ con₁ : Con k}(con= : con₀ ≡ con₁)
  {tms₀ : Tms Γ con₀}{tms₁ : Tms Γ con₁}(tms= : transport (Tms Γ) con= tms₀ ≡ tms₁)
  {ty₀ : Ty con₀ j}{ty₁ : Ty con₁ j}(ty= : transport (λ z → Ty z j) con= ty₀ ≡ ty₁) →
  _≡_ {A = Ty' Γ j k} (record { con = con₀ ; tms = tms₀ ; ty = ty₀ }) (record { con = con₁ ; tms = tms₁ ; ty = ty₁ })
Ty'= refl refl refl = refl

∣tr∣t''' : ∀{i}{Γ : Con i}{j}{k}{con : Con k}
  {∣_∣₀ ∣_∣₁ : ∣ Γ ∣ → ∣ con ∣}(∣_∣₀₁ : ∣_∣₀ ≡ ∣_∣₁)
  {~s₀ : {γ γ' : ∣ Γ ∣} → (Γ ~) γ γ' → (con ~) (∣_∣₀ γ) (∣_∣₀ γ')}
  {~s₁ : {γ γ' : ∣ Γ ∣} → (Γ ~) γ γ' → (con ~) (∣_∣₁ γ) (∣_∣₁ γ')}
  (~s₀₁ : _≡_ {A = {γ γ' : ∣ Γ ∣} → (Γ ~) γ γ' → (con ~) (∣_∣₁ γ) (∣_∣₁ γ')} (transport (λ ∣_∣ → {γ γ' : Con.∣ Γ ∣} → (Γ ~) γ γ' → (con ~) (∣_∣ γ) (∣_∣ γ')) ∣_∣₀₁ ~s₀) ~s₁) →
  {ty : Ty con j}
  {t : Tm' Γ (record { con = con ; tms = record { ∣_∣ = ∣_∣₀ ; _~ = ~s₀ } ; ty = ty })}
  →
  ∣ transport (Tm' Γ) (Ty'= (refl {x = con}) (Tms=' ∣_∣₀₁ {~s₀}{~s₁} ~s₀₁) (refl {x = ty})) t ∣ ≡ transport (λ z → (γ : ∣ Γ ∣) → ∣ ty ∣ (z γ)) ∣_∣₀₁ ∣ t ∣
∣tr∣t''' refl refl = refl

∣tr∣t'' : ∀{i}{Γ : Con i}{j}{k}{con : Con k}
  {∣_∣₀ ∣_∣₁ : ∣ Γ ∣ → ∣ con ∣}(∣_∣₀₁ : ∣_∣₀ ≡ ∣_∣₁)
  {~s₀ : {γ γ' : ∣ Γ ∣} → (Γ ~) γ γ' → (con ~) (∣_∣₀ γ) (∣_∣₀ γ')}
  {~s₁ : {γ γ' : ∣ Γ ∣} → (Γ ~) γ γ' → (con ~) (∣_∣₁ γ) (∣_∣₁ γ')}
  {ty : Ty con j}
  {t : Tm' Γ (record { con = con ; tms = record { ∣_∣ = ∣_∣₀ ; _~ = ~s₀ } ; ty = ty })}
  →
  ∣ transport (Tm' Γ) (Ty'= (refl {x = con}) (Tms= ∣_∣₀₁ {~s₀}{~s₁}) (refl {x = ty})) t ∣ ≡ transport (λ z → (γ : ∣ Γ ∣) → ∣ ty ∣ (z γ)) ∣_∣₀₁ ∣ t ∣
∣tr∣t'' {con = con}{∣_∣₀}{∣_∣₁} ∣_∣₀₁ = ∣tr∣t''' ∣_∣₀₁ (toProp (ispropΠi $ ispropΠi1 $ ispropΠ1 $ ap (λ z (γ ,Σ γ' ,Σ _) → z (_ ,Σ ∣_∣₁ γ ,Σ ∣_∣₁ γ')) (~p con)) _ _)

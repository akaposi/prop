{-# OPTIONS --without-K #-}

module ModelStrict.Func where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core
open import Model.Const
open import Model.Func
open import Model.Sigma
open import ModelStrict.CwF

Π' : ∀{i}{Γ : Con i}{j k}(A : Ty' Γ j k){l m}(B : Ty' (Γ ▷' A) l m) → Ty' Γ (j ⊔ l) (k ⊔ j ⊔ m)
con (Π' A B) = con A ▷ (ty A ⇒ K (con B))
tms (Π' {Γ = Γ} A B) = _,_ {Γ = Γ}{Δ = con A}(tms A){A = ty A ⇒ K (con B)} (lamK {A = ty A} (mkK (tms B)))
ty (Π' {Γ = Γ} A B) = Π {Γ = con A ▷ ty A ⇒ K (con B)} (ty A [ wk {A = ty A ⇒ K (con B)} ]T) (ty B [ unK (app-vz {Γ = con A}{A = ty A}{Δ = con B}) ]T)

lam' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m} → Tm' (Γ ▷' A) B → Tm' Γ (Π' A B)
∣ lam' {Γ = Γ}{A = A} t ∣ γₓ = (λ aₓ → ∣ t ∣ (γₓ ,Σ aₓ)) ,Σ λ {a₀}{a₁} a₀₁ → (t ~) (R Γ γₓ ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) _ _) a₀₁)
(lam' t ~) {γ₀}{γ₁} γ₀₁ {a₀}{a₁} a₀₁ = (t ~) (γ₀₁ ,Σ a₀₁)

app' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m} → Tm' Γ (Π' A B) → Tm' (Γ ▷' A) B
∣ app' t ∣ (γₓ ,Σ aₓ) = proj₁ (∣ t ∣ γₓ) aₓ
(app' t ~) {γ₀ ,Σ a₀}{γ₁ ,Σ a₁}(γ₀₁ ,Σ a₀₁) = (t ~) γ₀₁ a₀₁

Π'β : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{t : Tm' (Γ ▷' A) B} → app' {A = A}{B = B} (lam' {A = A}{B = B} t) ≡ t
Π'β = refl

Π'η : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{t : Tm' Γ (Π' A B)} → lam' {A = A}{B = B} (app' {A = A}{B = B} t) ≡ t
Π'η {Γ = Γ}{B = B}{t = t} = Tm= (ap (λ z → λ γₓ → (λ aₓ → proj₁ (∣ t ∣ γₓ) aₓ) ,Σ (λ {a₀} {a₁} a₀₁ → z _ ((t ~) (R Γ γₓ) _) (proj₂ (∣ t ∣ γₓ) a₀₁))) (~p (ty B)))

Π'[] : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{n}{Δ : Con n}{γ : Tms Δ Γ} →
  (Π' A B) [ γ ]T' ≡ Π' (A [ γ ]T') (B [ γ ^' A ]T')
Π'[] {Γ = Γ}{A = A}{B = B}{Δ = Δ}{γ} = Ty'=
  refl
  (Tms=
    (ap (λ z → λ δₓ → ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ((λ aₓ → ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ aₓ)) ,Σ (λ {a₀} {a₁} a₀₁ → z
      (tt ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₀) ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₁))
      ((tms B ~) (R Γ (∣ γ ∣ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) (R Γ (∣ γ ∣ δₓ)))) a₀₁))
      ((tms B ~) ((γ ~) (R Δ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) ((γ ~) (R Δ δₓ)))) a₀₁)))))
    (~p (con B))))
  refl


{-# OPTIONS --without-K #-}

module ModelStrict.Func.Subst where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core
open import Model.Const
open import Model.Func
open import Model.Sigma
open import ModelStrict.CwF
open import ModelStrict.Func

-- this is a version of lam' with a substitution built in. this way we
-- can define lam'[] without transport in its type

lam'' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{n}{Δ : Con n}(γ : Tms Δ Γ) →
  Tm' (Δ ▷' A [ γ ]T') (B [ γ ^' A ]T') → Tm' Δ (Π' A B [ γ ]T')
∣ lam'' {A = A}{B = B}{Δ = Δ} γ t ∣ δₓ =
  (λ aₓ → ∣ t ∣ (δₓ ,Σ aₓ)) ,Σ
  λ {a₀}{a₁} a₀₁ → transport
    (λ z → (ty B ~) z (∣ t ∣ (δₓ ,Σ a₀)) (∣ t ∣ (δₓ ,Σ a₁)))
    (toProp1 (~p (con B)) _ _)
    ((t ~) (R Δ δₓ ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) _ _) a₀₁))
(lam'' γ t ~) {δ₀}{δ₁} δ₀₁ {a₀}{a₁} a₀₁ = (t ~) (δ₀₁ ,Σ a₀₁)

{-
-- TODO. this doesn't typecheck because Agda is too slow. another
-- method would be to generalise Π'[], Σ'[] into one more abstract
-- proof and hopefully that would typecheck faster. also we could use
-- abstract for all irrelevant components which might speed up
-- typechecking

lam''= : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{t : Tm' (Γ ▷' A) B}{n}{Δ : Con n}{γ : Tms Δ Γ} →
  transport
    (Tm' Δ)
    (Π'[] {A = A}{B = B}{γ = γ})
    (lam'' {A = A}{B = B} γ (_[_]t' {A = B} t (γ ^' A))) ≡
  lam' {A = A [ γ ]T'}{B = B [ γ ^' A ]T'} (_[_]t' {A = B} t (γ ^' A))
lam''= {Γ = Γ}{A = A}{B = B}{t}{Δ = Δ}{γ} = Tm= (∣tr∣t'' (ap (λ z → λ δₓ → ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ((λ aₓ → ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ aₓ)) ,Σ (λ {a₀} {a₁} a₀₁ → z
      (tt ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₀) ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₁))
      ((tms B ~) (R Γ (∣ γ ∣ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) (R Γ (∣ γ ∣ δₓ)))) a₀₁))
      ((tms B ~) ((γ ~) (R Δ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) ((γ ~) (R Δ δₓ)))) a₀₁)))))
    (~p (con B))) {t = lam'' γ (t [ γ ^' A ]t')} ◾ ?)
-}

lam'[] : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{t : Tm' (Γ ▷' A) B}{n}{Δ : Con n}{γ : Tms Δ Γ} →
  _[_]t' {A = Π' A B} (lam' {Γ = Γ}{A = A}{B = B} t) γ ≡
  lam'' {Γ = Γ}{A = A}{B = B}{Δ = Δ} γ (_[_]t' {A = B} t (γ ^' A))
lam'[] {Γ = Γ}{A = A}{B = B}{t}{Δ = Δ}{γ} = Tm= (
  ap (λ z → λ δₓ → (λ aₓ → ∣ t ∣ (∣ γ ∣ δₓ ,Σ aₓ)) ,Σ λ {a₀}{a₁} a₀₁ → z _
    ((t ~)
            (R Γ (∣ γ ∣ δₓ) ,Σ
             coe
             (ap (λ z → (ty A ~) z a₀ a₁)
              (ap
               (λ z →
                  z (tt ,Σ ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ∣ tms A ∣ (∣ γ ∣ δₓ))
                  (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) (R Γ (∣ γ ∣ δₓ))))
               (~p (con A))))
             a₀₁))
    (coe
            (ap
             (λ z →
                (ty B ~) z (∣ t ∣ (∣ γ ∣ δₓ ,Σ a₀)) (∣ t ∣ (∣ γ ∣ δₓ ,Σ a₁)))
             (ap
              (λ z →
                 z (tt ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₀) ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₁))
                 ((tms B ~)
                  ((γ ~) (R Δ δₓ) ,Σ
                   coe
                   (ap (λ z₁ → (ty A ~) z₁ a₀ a₁)
                    (ap
                     (λ z₁ →
                        z₁ (tt ,Σ ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ∣ tms A ∣ (∣ γ ∣ δₓ))
                        (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) ((γ ~) (R Δ δₓ))))
                     (~p (con A))))
                   a₀₁))
                 ((tms B ~)
                  (R Γ (∣ γ ∣ δₓ) ,Σ
                   coe
                   (ap (λ z₁ → (ty A ~) z₁ a₀ a₁)
                    (ap
                     (λ z₁ →
                        z₁ (tt ,Σ ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ∣ tms A ∣ (∣ γ ∣ δₓ))
                        (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) (R Γ (∣ γ ∣ δₓ))))
                     (~p (con A))))
                   a₀₁)))
              (~p (con B))))
            ((t ~)
             ((γ ~) (R Δ δₓ) ,Σ
              coe
              (ap (λ z → (ty A ~) z a₀ a₁)
               (ap
                (λ z →
                   z (tt ,Σ ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ∣ tms A ∣ (∣ γ ∣ δₓ))
                   (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) ((γ ~) (R Δ δₓ))))
                (~p (con A))))
              a₀₁))))
  (~p (ty B)))

{-
-- this would be the direct proof of substitution for lam'
lam'[] : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{t : Tm' (Γ ▷' A) B}{n}{Δ : Con n}{γ : Tms Δ Γ} →
  transport (Tm' Δ) (Π'[] {Γ = Γ}{A = A}{B = B}{Δ = Δ}{γ}) (_[_]t' {A = Π' A B} (lam' {Γ = Γ}{A = A}{B = B} t) γ) ≡
  lam' {Γ = Δ}{A = A [ γ ]T'}{B = B [ γ ^' A ]T'} (_[_]t' {A = B} t (γ ^' A))
lam'[] {Γ = Γ}{A = A}{B = B}{t}{Δ = Δ}{γ} = Tm= (
  ∣tr∣t''
    {Γ = Δ}
    (ap
      (λ z → λ δₓ → ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ((λ aₓ → ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ aₓ)) ,Σ (λ {a₀} {a₁} a₀₁ → z
        (tt ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₀) ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₁))
        ((tms B ~) (R Γ (∣ γ ∣ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) (R Γ (∣ γ ∣ δₓ)))) a₀₁))
        ((tms B ~) ((γ ~) (R Δ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) ((γ ~) (R Δ δₓ)))) a₀₁)))))
      (~p (con B)))
    {t = _[_]t' {A = Π' A B} (lam' {Γ = Γ}{A = A}{B = B} t) γ} ◾
  {!!})
-}

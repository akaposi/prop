{-# OPTIONS --without-K #-}

module ModelStrict.Props where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core
open import Model.Props
open import Model.Func
open import Model.Const
open import ModelStrict.CwF
open import ModelStrict.Func

infixl 7 _[_]TP'

record TyP' {i}(Γ : Con i) j k : Set (i ⊔ lsuc j ⊔ lsuc k) where
  field
    con : Con k
    tms : Tms Γ con
    typ : TyP con j
open TyP' public

TyP'= : ∀{i}{Γ : Con i}{j}{k} →
  {con₀ con₁ : Con k}(con= : con₀ ≡ con₁)
  {tms₀ : Tms Γ con₀}{tms₁ : Tms Γ con₁}(tms= : transport (Tms Γ) con= tms₀ ≡ tms₁)
  {typ₀ : TyP con₀ j}{typ₁ : TyP con₁ j}(typ= : transport (λ z → TyP z j) con= typ₀ ≡ typ₁) →
  _≡_ {A = TyP' Γ j k} (record { con = con₀ ; tms = tms₀ ; typ = typ₀ }) (record { con = con₁ ; tms = tms₁ ; typ = typ₁ })
TyP'= refl refl refl = refl

_[_]TP' : ∀{i}{Γ : Con i}{j}{k} → TyP' Γ j k → ∀{l}{Δ : Con l} → Tms Δ Γ → TyP' Δ j k
con (A [ γ ]TP') = con A
tms (A [ γ ]TP') = tms A ∘ γ
typ (A [ γ ]TP') = typ A

[∘]TP' : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Δ : Con k}{l m}{A : TyP' Δ l m}{σ : Tms Θ Δ}{δ : Tms Γ Θ} → (A [ σ ∘ δ ]TP') ≡ (A [ σ ]TP' [ δ ]TP')
[∘]TP' = refl

[id]TP' : ∀{i}{Γ : Con i}{j k}{A : TyP' Γ j k} → A [ id ]TP' ≡ A
[id]TP' = refl

↑'_ : ∀{i}{Γ : Con i}{j k} → TyP' Γ j k → Ty' Γ j k
con (↑' A) = con A
tms (↑' A) = tms A
ty (↑' A) = ↑ (typ A)

↑'[] : ∀{i}{Γ : Con i}{j k}{A : TyP' Γ j k}{l}{Δ : Con l}{γ : Tms Δ Γ} → (↑' A) [ γ ]T' ≡ ↑' (A [ γ ]TP')
↑'[] = refl

irr' : ∀{i}{Γ : Con i}{j k}{A : TyP' Γ j k}(u v : Tm' Γ (↑' A)) → u ≡ v
irr' {A = A} u v = Tm= (ap (λ z → λ γₓ → z (∣ tms A ∣ γₓ) (∣ u ∣ γₓ) (∣ v ∣ γₓ)) (∣p (typ A)))

⊤'' : ∀{i}{Γ : Con i}{j} → TyP' Γ j lzero
con ⊤'' = •
tms ⊤'' = ε
typ ⊤'' = ⊤'

⊤''[] : ∀{i}{Γ : Con i}{j}{k}{Δ : Con k}{γ : Tms Δ Γ} → ⊤'' {Γ = Γ}{j} [ γ ]TP' ≡ ⊤''
⊤''[] = refl

tt'' : ∀{i}{Γ : Con i}{j} → Tm' Γ (↑' (⊤'' {j = j}))
tt'' = tt'

ΠP' : ∀{i}{Γ : Con i}{j k}(A : Ty' Γ j k){l m}(B : TyP' (Γ ▷' A) l m) → TyP' Γ (j ⊔ l) (k ⊔ j ⊔ m)
con (ΠP' A B) = con A ▷ (ty A ⇒ K (con B))
tms (ΠP' {Γ = Γ} A B) = _,_ {Γ = Γ}{Δ = con A}(tms A){A = ty A ⇒ K (con B)} (lamK {A = ty A} (mkK (tms B)))
typ (ΠP' {Γ = Γ} A B) = Model.Props.Π {Γ = con A ▷ ty A ⇒ K (con B)} (ty A [ wk {A = ty A ⇒ K (con B)} ]T) (typ B [ unK (app-vz {Γ = con A}{A = ty A}{Δ = con B}) ]TP)

ΠP'[] : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : TyP' (Γ ▷' A) l m}{n}{Δ : Con n}{γ : Tms Δ Γ} →
  (ΠP' A B) [ γ ]TP' ≡ ΠP' (A [ γ ]T') (B [ γ ^' A ]TP')
ΠP'[] {Γ = Γ}{A = A}{B = B}{Δ = Δ}{γ = γ} = TyP'=
  refl
  (Tms= (ap
    (λ z → λ δₓ → ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ((λ aₓ → ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ aₓ)) ,Σ (λ {a₀} {a₁} a₀₁ → z
      (tt ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₀) ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₁))
      ((tms B ~) (R Γ (∣ γ ∣ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) (R Γ (∣ γ ∣ δₓ)))) a₀₁))
      ((tms B ~) ((γ ~) (R Δ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) ((γ ~) (R Δ δₓ)))) a₀₁))
      )))
    (~p (con B)) ))
  refl


lamP' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : TyP' (Γ ▷' A) l m} → Tm' (Γ ▷' A) (↑' B) → Tm' Γ (↑' ΠP' A B)
∣ lamP' t ∣ γₓ aₓ = ∣ t ∣ (γₓ ,Σ aₓ)
(lamP' t ~) _ = lift tt

appP' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : TyP' (Γ ▷' A) l m} → Tm' Γ (↑' ΠP' A B) → Tm' (Γ ▷' A) (↑' B)
∣ appP' t ∣ (γₓ ,Σ aₓ) = ∣ t ∣ γₓ aₓ
(appP' t ~) _ = lift tt

Σ'' : ∀{i}{Γ : Con i}{j k}(A : TyP' Γ j k){l m}(B : TyP' (Γ ▷' ↑' A) l m) → TyP' Γ (j ⊔ l) (k ⊔ j ⊔ m)
con (Σ'' A B) = con A ▷ ((↑ typ A) ⇒ K (con B))
tms (Σ'' {Γ = Γ} A B) = _,_ {Γ = Γ}{Δ = con A}(tms A){A = (↑ typ A) ⇒ K (con B)} (lamK {A = ↑ typ A} (mkK (tms B)))
typ (Σ'' {Γ = Γ} A B) = Model.Props.Σ' {Γ = con A ▷ (↑ typ A) ⇒ K (con B)} (typ A [ wk {A = (↑ typ A) ⇒ K (con B)} ]TP) (typ B [ unK (app-vz {Γ = con A}{A = ↑ typ A}{Δ = con B}) ]TP)

_,''_ : ∀{i}{Γ : Con i}{j k}{A : TyP' Γ j k}{l m}{B : TyP' (Γ ▷' ↑' A) l m} → (a : Tm' Γ (↑' A)) → Tm' Γ (↑' (B [ <_>' {A = ↑' A} a ]TP')) → Tm' Γ (↑' Σ'' A B)
∣ a ,'' b ∣ γₓ = ∣ a ∣ γₓ ,Σ ∣ b ∣ γₓ
((a ,'' b) ~) _ = lift tt

proj₁'' : ∀{i}{Γ : Con i}{j k}{A : TyP' Γ j k}{l m}{B : TyP' (Γ ▷' ↑' A) l m} → Tm' Γ (↑' Σ'' A B) → Tm' Γ (↑' A)
∣ proj₁'' t ∣ γₓ = proj₁ (∣ t ∣ γₓ)
(proj₁'' t ~) _ = lift tt

proj₂'' : ∀{i}{Γ : Con i}{j k}{A : TyP' Γ j k}{l m}{B : TyP' (Γ ▷' ↑' A) l m}(t : Tm' Γ (↑' Σ'' A B)) → Tm' Γ (↑' (B [ <_>' {A = ↑' A} (proj₁'' {A = A}{B = B} t) ]TP'))
∣ proj₂'' t ∣ γₓ = proj₂ (∣ t ∣ γₓ)
(proj₂'' t ~) _ = lift tt

Σ''[] : ∀{i}{Γ : Con i}{j k}{A : TyP' Γ j k}{l m}{B : TyP' (Γ ▷' ↑' A) l m}{n}{Δ : Con n}{γ : Tms Δ Γ} →
  (Σ'' A B) [ γ ]TP' ≡ Σ'' (A [ γ ]TP') (B [ γ ^' (↑' A) ]TP')
Σ''[] {Γ = Γ}{A = A}{B = B}{Δ = Δ}{γ = γ} = TyP'=
  refl
  (Tms= (ap
    (λ z → λ δₓ → ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ((λ aₓ → ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ aₓ)) ,Σ (λ {a₀} {a₁} a₀₁ → z
      (tt ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₀) ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₁))
      ((tms B ~) (R Γ (∣ γ ∣ δₓ) ,Σ transport (λ z → Lift ⊤) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) (R Γ (∣ γ ∣ δₓ)))) a₀₁))
      ((tms B ~) ((γ ~) (R Δ δₓ) ,Σ transport (λ z → Lift ⊤) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) ((γ ~) (R Δ δₓ)))) a₀₁))
      )))
    (~p (con B))))
  refl

_^^_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(γ : Tms Δ Γ){k}(A : Ty Γ k) → Tms (Δ ▷ A [ γ ]T ▷ A [ γ ∘ wk {A = A [ γ ]T} ]T) (Γ ▷ A ▷ A [ wk {A = A} ]T)
∣ γ ^^ A ∣ (δₓ ,Σ aₓ ,Σ a'ₓ) = ∣ γ ∣ δₓ ,Σ aₓ ,Σ a'ₓ
((γ ^^ A) ~) (δ₀₁ ,Σ a₀₁ ,Σ a'₀₁) = (γ ~) δ₀₁ ,Σ a₀₁ ,Σ a'₀₁

Id'' : ∀{i}{Γ : Con i}{j k}(A : Ty' Γ j k) → TyP' (Γ ▷' A ▷' A [ p {A = A} ]T') j (j ⊔ k)
con (Id'' A) = con A ▷ ty A ▷ ty A [ wk {A = ty A} ]T
tms (Id'' A) = tms A ^^ ty A
typ (Id'' A) = Id {Γ = con A ▷ ty A ▷ ty A [ wk {A = ty A} ]T}
  (ty A [ wk {A = ty A} ]T [ wk {A = ty A [ wk {A = ty A} ]T} ]T)
  (vz {A = ty A} [ wk {A = ty A [ wk {A = ty A} ]T} ]t)
  (vz {A = ty A [ wk {A = ty A} ]T})

Id' : ∀{i}{Γ : Con i}{j k}(A : Ty' Γ j k) → Tm' Γ A → Tm' Γ A → TyP' Γ j (j ⊔ k)
Id' {Γ = Γ} A a a' = Id'' A [ _,'_  (_,'_ (id {Γ = Γ}) {A = A} a) {A = A [ p {A = A} ]T'} a' ]TP'

Id'[] : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{a a' : Tm' Γ A}{l}{Δ : Con l}{γ : Tms Δ Γ} → Id' A a a' [ γ ]TP' ≡ Id' (A [ γ ]T') (_[_]t' {A = A} a γ) (_[_]t' {A = A} a' γ)
Id'[] = refl

refl'' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}(a : Tm' Γ A) → Tm' Γ (↑' Id' A a a)
∣ refl'' {A = A} a ∣ γₓ = R (ty A) (∣ a ∣ γₓ)
(refl'' a ~) _ = lift tt

transp' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}(P : Ty' (Γ ▷' A) l m){a a' : Tm' Γ A}(e : Tm' Γ (↑' Id' A a a')) →
  Tm' Γ (P [ <_>' {A = A} a ]T') →
  Tm' Γ (P [ <_>' {A = A} a' ]T')
∣ transp' {Γ = Γ} {A = A} P {a}{a'} e t ∣ γₓ = coeT (ty P) ((tms P ~) (R Γ γₓ ,Σ transport (λ z → (ty A ~) z (∣ a ∣ γₓ) (∣ a' ∣ γₓ)) (toProp1 (~p (con A)) _ _) (∣ e ∣ γₓ))) (∣ t ∣ γₓ) 
(transp' {Γ = Γ} {A = A} P {a}{a'} e t ~) {γ₀}{γ₁} γ₀₁ = 
  transport
    (λ z → (ty P ~) z (coeT (ty P) ((tms P ~) (R Γ γ₀ ,Σ _)) (∣ t ∣ γ₀)) (coeT (ty P) ((tms P ~) (R Γ γ₁ ,Σ _)) (∣ t ∣ γ₁)))
    ((toProp1 (~p (con P)) _ _))
    (TT3 (ty P) (S (ty P) (cohT (ty P) ((tms P ~) (R Γ γ₀ ,Σ _)) (∣ t ∣ γ₀))) ((t ~) γ₀₁) (cohT (ty P) (((tms P) ~) (R Γ γ₁ ,Σ _)) (∣ t ∣ γ₁))) -- TODO

transp'[] : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{P : Ty' (Γ ▷' A) l m}{a a' : Tm' Γ A}{e : Tm' Γ (↑' Id' A a a')}{t : Tm' Γ (P [ <_>' {A = A} a ]T')}{n}{Δ : Con n}{γ : Tms Δ Γ} →
  _[_]t' {A = P [ <_>' {A = A} a' ]T'} (transp' {Γ = Γ}{A = A} P e t) γ ≡
  transp' {Γ = Δ}{A = A [ γ ]T'} (P [ γ ^' A ]T') (_[_]t' {A = ↑' Id' A a a'} e γ) (_[_]t' {A = P [ <_>' {A = A} a ]T'} t γ)
transp'[] {Γ = Γ}{A = A}{P = P}{a}{a'}{e}{t}{Δ = Δ}{γ} = Tm= ( ap
  (λ z → λ δₓ → coeT (ty P) (z _ ((tms P ~) (R Γ (∣ γ ∣ δₓ) ,Σ coe (ap (λ z → (ty A ~) z (∣ a ∣ (∣ γ ∣ δₓ)) (∣ a' ∣ (∣ γ ∣ δₓ))) (ap (λ z → z (tt ,Σ ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ∣ tms A ∣ (∣ γ ∣ δₓ)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) (R Γ (∣ γ ∣ δₓ)))) (~p (con A)))) (∣ e ∣ (∣ γ ∣ δₓ)))) ((tms P ~) ((γ ~) (R Δ δₓ) ,Σ coe (ap (λ z → (ty A ~) z (∣ a ∣ (∣ γ ∣ δₓ)) (∣ a' ∣ (∣ γ ∣ δₓ))) (ap (λ z → z (tt ,Σ ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ∣ tms A ∣ (∣ γ ∣ δₓ)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) ((γ ~) (R Δ δₓ)))) (~p (con A)))) (∣ e ∣ (∣ γ ∣ δₓ))))) (∣ t ∣ (∣ γ ∣ δₓ)))
  (~p (con P)) )

transp'β : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{P : Ty' (Γ ▷' A) l m}{a : Tm' Γ A}{t : Tm' Γ (P [ <_>' {A = A} a ]T')} →
  Tm' Γ (↑' (Id' (P [ <_>' {A = A} a ]T') (transp' {Γ = Γ}{A = A} P (refl'' {A = A} a) t) t))
∣ transp'β {Γ = Γ}{A = A}{P = P}{a}{t} ∣ γₓ = transport
  (λ z → (ty P ~) z (coeT (ty P) ((tms P ~) (R Γ γₓ ,Σ coe (ap (λ z → (ty A ~) z (∣ a ∣ γₓ) (∣ a ∣ γₓ)) (ap (λ z → z (tt ,Σ ∣ tms A ∣ γₓ ,Σ ∣ tms A ∣ γₓ) (R (con A) (∣ tms A ∣ γₓ)) ((tms A ~) (R Γ γₓ))) (~p (con A)))) (R (ty A) (∣ a ∣ γₓ)))) (∣ t ∣ γₓ)) (∣ t ∣ γₓ))
  (toProp1 (~p (con P)) _ _)
  (S (ty P) (cohT (ty P) ((tms P ~) (R Γ γₓ ,Σ coe (ap (λ z → (ty A ~) z (∣ a ∣ γₓ) (∣ a ∣ γₓ)) (ap (λ z → z (tt ,Σ ∣ tms A ∣ γₓ ,Σ ∣ tms A ∣ γₓ) (R (con A) (∣ tms A ∣ γₓ)) ((tms A ~) (R Γ γₓ))) (~p (con A)))) (R (ty A) (∣ a ∣ γₓ)))) (∣ t ∣ γₓ)))
(transp'β ~) _ = lift tt

funext : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{f g : Tm' Γ (Π' A B)} →
  Tm' Γ (Π' A (↑' Id' B (app' {Γ = Γ}{A = A}{B = B} f) (app' {Γ = Γ}{A = A}{B = B} g))) → Tm' Γ (↑' (Id' (Π' A B) f g))
∣ funext {Γ = Γ} {A = A} {B = B} {f} {g} e ∣ γₓ {a₀}{a₁} a₀₁ = transport
  (λ z → (ty B ~) z (proj₁ (∣ f ∣ γₓ) a₀) (proj₁ (∣ g ∣ γₓ) a₁))
  (toProp1 (~p (con B)) _ _)
  (T (ty B) (proj₁ (∣ e ∣ γₓ) a₀) ((g ~) (R Γ γₓ) (transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ γₓ)) ((tms A ~) (R Γ γₓ))) a₀₁)))
(funext {Γ = Γ} {A = A} {B = B} {f} {g} e ~) _ = lift tt

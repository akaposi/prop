{-# OPTIONS --without-K #-}

module ModelStrict.Sigma where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core
open import Model.Const
open import Model.Func
open import Model.Sigma
open import ModelStrict.CwF

Σ'' : ∀{i}{Γ : Con i}{j k}(A : Ty' Γ j k){l m}(B : Ty' (Γ ▷' A) l m) → Ty' Γ (j ⊔ l) (k ⊔ j ⊔ m)
con (Σ'' A B) = con A ▷ (ty A ⇒ K (con B))
tms (Σ'' {Γ = Γ} A B) = _,_ {Γ = Γ}{Δ = con A}(tms A){A = ty A ⇒ K (con B)} (lamK {A = ty A} (mkK (tms B)))
ty (Σ'' {Γ = Γ} A B) = Σ' {Γ = con A ▷ ty A ⇒ K (con B)} (ty A [ wk {A = ty A ⇒ K (con B)} ]T) (ty B [ unK (app-vz {Γ = con A}{A = ty A}{Δ = con B}) ]T)

_,''_ : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m} → (a : Tm' Γ A) → Tm' Γ (B [ <_>' {A = A} a ]T') → Tm' Γ (Σ'' A B)
∣ a ,'' b ∣ γₓ = ∣ a ∣ γₓ ,Σ ∣ b ∣ γₓ
((a ,'' b) ~) γ₀₁ = (a ~) γ₀₁ ,Σ (b ~) γ₀₁

proj₁'' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m} → Tm' Γ (Σ'' A B) → Tm' Γ A
∣ proj₁'' ab ∣ γₓ  = proj₁ (∣ ab ∣ γₓ)
(proj₁'' ab ~) γ₀₁ = proj₁ ((ab ~) γ₀₁)

proj₂'' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}(ab : Tm' Γ (Σ'' A B)) → Tm' Γ (B [ <_>' {A = A} (proj₁'' {Γ = Γ}{A = A}{B = B} ab) ]T')
∣ proj₂'' ab ∣ γₓ  = proj₂ (∣ ab ∣ γₓ)
(proj₂'' ab ~) γ₀₁ = proj₂ ((ab ~) γ₀₁)

Σβ₁' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{a : Tm' Γ A}{b : Tm' Γ (B [ <_>' {A = A} a ]T')} →
  proj₁'' {Γ = Γ}{A = A}{B = B} (_,''_  {Γ = Γ}{A = A}{B = B} a b) ≡ a
Σβ₁' = refl

Σβ₂' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{a : Tm' Γ A}{b : Tm' Γ (B [ <_>' {A = A} a ]T')} →
  proj₂'' {Γ = Γ}{A = A}{B = B} (_,''_  {Γ = Γ}{A = A}{B = B} a b) ≡ b
Σβ₂' = refl

Ση' : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{ab : Tm' Γ (Σ'' A B)} →
  _,''_ {Γ = Γ}{A = A}{B = B} (proj₁'' {Γ = Γ}{A = A}{B = B} ab) (proj₂'' {Γ = Γ}{A = A}{B = B} ab) ≡ ab
Ση' = refl

Σ'[] : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{n}{Δ : Con n}{γ : Tms Δ Γ} →
  (Σ'' A B) [ γ ]T' ≡ Σ'' (A [ γ ]T') (B [ γ ^' A ]T')
Σ'[] {Γ = Γ}{A = A}{B = B}{Δ = Δ}{γ} = Ty'=
  refl
  (Tms=
    (ap (λ z → λ δₓ → ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ((λ aₓ → ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ aₓ)) ,Σ (λ {a₀} {a₁} a₀₁ → z
      (tt ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₀) ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₁))
      ((tms B ~) (R Γ (∣ γ ∣ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) (R Γ (∣ γ ∣ δₓ)))) a₀₁))
      ((tms B ~) ((γ ~) (R Δ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) ((γ ~) (R Δ δₓ)))) a₀₁)))))
    (~p (con B))))
  refl

,'[] : ∀{i}{Γ : Con i}{j k}{A : Ty' Γ j k}{l m}{B : Ty' (Γ ▷' A) l m}{a : Tm' Γ A}{b : Tm' Γ (B [ <_>' {A = A} a ]T')}{n}{Δ : Con n}{γ : Tms Δ Γ} →
  transport (Tm' Δ) (Σ'[] {Γ = Γ}{A = A}{B = B}{Δ = Δ}{γ}) (_[_]t' {A = Σ'' A B} (_,''_ {Γ = Γ}{A = A}{B = B} a b) γ) ≡
  _,''_ {Γ = Δ}{A = A [ γ ]T'}{B = B [ γ ^' A ]T'} (_[_]t' {A = A} a γ) (_[_]t' {A = B [ <_>' {A = A} a ]T'} b γ)
,'[] {Γ = Γ}{A = A}{B = B}{a = a}{b = b}{Δ = Δ}{γ} = Tm= (
    ∣tr∣t''
      (ap
        (λ z → λ δₓ → ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ((λ aₓ → ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ aₓ)) ,Σ (λ {a₀} {a₁} a₀₁ → z
          (tt ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₀) ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₁))
          ((tms B ~) (R Γ (∣ γ ∣ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) (R Γ (∣ γ ∣ δₓ)))) a₀₁))
          ((tms B ~) ((γ ~) (R Δ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) ((γ ~) (R Δ δₓ)))) a₀₁)))))
        (~p (con B)))
      {t = _[_]t' {A = Σ'' A B} (_,''_  {Γ = Γ}{A = A}{B = B} a b) γ} ◾
    trap
      {P = λ (z : (δₓ : ∣ Δ ∣) → ∣ con A ▷ (ty A ⇒ K (con B)) ∣) → (δₓ : ∣ Δ ∣) → Σ (∣ ty A ∣ (proj₁ (z δₓ))) (λ aₓ → ∣ ty B ∣ (proj₁ (proj₂ (z δₓ)) aₓ))}
      {f = (λ z → λ δₓ → ∣ tms A ∣ (∣ γ ∣ δₓ) ,Σ ((λ aₓ → ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ aₓ)) ,Σ (λ {a₀} {a₁} a₀₁ → z
        (tt ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₀) ,Σ ∣ tms B ∣ (∣ γ ∣ δₓ ,Σ a₁))
        ((tms B ~) (R Γ (∣ γ ∣ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) (R Γ (∣ γ ∣ δₓ)))) a₀₁))
        ((tms B ~) ((γ ~) (R Δ δₓ) ,Σ transport (λ z → (ty A ~) z a₀ a₁) (toProp1 (~p (con A)) (R (con A) (∣ tms A ∣ (∣ γ ∣ δₓ))) ((tms A ~) ((γ ~) (R Δ δₓ)))) a₀₁)))))}
     (~p (con B))
     {λ γₓ → ∣ a ∣ (∣ γ ∣ γₓ) ,Σ ∣ b ∣ (∣ γ ∣ γₓ)} ◾
   trconst
     {B = (δₓ : ∣ Δ ∣) → Σ (∣ ty A ∣ (∣ tms A ∣ (∣ γ ∣ δₓ))) (λ aₓ → ∣ ty B ∣ (∣ tms B ∣ (∣ γ ∣ δₓ ,Σ aₓ)))}
     (~p (con B))
     {λ γₓ → ∣ a ∣ (∣ γ ∣ γₓ) ,Σ ∣ b ∣ (∣ γ ∣ γₓ)})

{-# OPTIONS --without-K #-}

module ModelStrict.Top where

open import lib
open import Agda.Primitive

open import Model.Decl
open import Model.Core
open import Model.Top
open import ModelStrict.CwF

⊤'' : ∀{i}{Γ : Con i}{j} → Ty' Γ j lzero
con ⊤'' = •
tms ⊤'' = ε
ty  ⊤'' = ⊤'

⊤[] : ∀{i}{Γ : Con i}{j}{k}{Δ : Con k}{γ : Tms Δ Γ} → ⊤'' {j = j} [ γ ]T' ≡ ⊤''
⊤[] = refl

tt'' : ∀{i}{Γ : Con i}{j} → Tm' Γ (⊤'' {j = j})
∣ tt'' ∣ _ = lift tt
(tt'' ~) _ = lift tt

⊤η : ∀{i}{Γ : Con i}{j}{t : Tm' Γ (⊤'' {j = j})} → t ≡ tt''
⊤η = refl

tt[] : ∀{i}{Γ : Con i}{j}{k}{Δ : Con k}{γ : Tms Δ Γ} → _[_]t' {A = ⊤''} (tt'' {j = j}) γ ≡ tt''
tt[] = refl

{-# OPTIONS --without-K #-}

module hModel.Core where

open import lib
open import Agda.Primitive

open import hModel.Decl

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t

• : Con lzero
• = record
  { ∣_∣ = ⊤
  ; _~  = λ _ _ → ⊤
  ; ~p = λ _ _ → refl
  }

_▷_ : ∀{i}(Γ : Con i){j} → Ty Γ j → Con (i ⊔ j)
_▷_ Γ A = record
  { ∣_∣ = Σ ∣ Γ ∣ ∣ A ∣
  ; _~ = λ { (γ ,Σ α)(γ' ,Σ α') → Σ ((Γ ~) γ γ') (λ z → (A ~) z α α') }
  ; ~p = λ { (γ~ ,Σ α~) (γ~' ,Σ α~') → ,Σ= (~p Γ γ~ γ~') (~p A _ α~') }
  ; R = λ { (γ ,Σ α) → R Γ γ ,Σ R A α }
  ; S = λ { (p ,Σ r) → S Γ p ,Σ S A r }
  ; T = λ { (p ,Σ r)(p' ,Σ r') → T Γ p p' ,Σ T A r r' }
  }

_[_]T : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k} → Ty Δ k → Tms Γ Δ → Ty Γ k
_[_]T {Γ = Γ}{Δ = Δ} A σ = record
  { ∣_∣  = λ γ → ∣ A ∣ (∣ σ ∣ γ)
  ; _~   = λ p α α' → (A ~) ((σ ~) p) α α'
  ; ~p   = λ {γ}{γ'}{γ~}{α}{α'} → ~p A {∣ σ ∣ γ}{∣ σ ∣ γ'}{(σ ~) γ~}{α}{α'}
  ; R    = λ {γ} α → transport (λ z → (A ~) z α α) (~p Δ _ _) (R A α)
  ; S    = λ {γ}{γ'}{γ~}{α}{α'} α~ → transport (λ z → (A ~) z α' α) (~p Δ _ _) (S A α~)
  ; T    = λ {γ}{γ'}{γ''}{γ~}{γ~'}{α}{α'}{α''} α~ α~' → transport (λ z → (A ~) z α α'') (~p Δ _ _) (T A α~ α~')
  ; coeT = λ p → coeT A ((σ ~) p)
  ; cohT = λ p → cohT A ((σ ~) p)
  }

id : ∀{i}{Γ : Con i} → Tms Γ Γ
id = record
  { ∣_∣ = λ γ → γ
  ; _~  = λ p → p
  }

_∘_ : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Δ : Con k} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
σ ∘ ν = record
  { ∣_∣ = λ γ → ∣ σ ∣ (∣ ν ∣ γ)
  ; _~  = λ p → (σ ~) ((ν ~) p)
  }

ε : ∀{i}{Γ : Con i} → Tms Γ •
ε = record
  { ∣_∣ = λ _ → tt
  }

_,_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Tms Γ Δ){b}{A : Ty Δ b} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
σ , t = record { ∣_∣ = λ γ → ∣ σ ∣ γ ,Σ ∣ t ∣ γ ; _~ = λ p → (σ ~) p ,Σ (t ~) p }

π₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k} → Tms Γ (Δ ▷ A) →  Tms Γ Δ
π₁ σ = record { ∣_∣ = λ γ → proj₁ (∣ σ ∣ γ) ; _~ = λ p → proj₁ ((σ ~) p) }

_[_]t : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = record { ∣_∣ = λ γ → ∣ t ∣ (∣ σ ∣ γ) ; _~ = λ p → (t ~) ((σ ~) p) }

π₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ σ = record { ∣_∣ = λ γ → proj₂ (∣ σ ∣ γ) ; _~ = λ p → proj₂ ((σ ~) p) }

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T ≡ A
[][]T : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Δ : Con k}{l}{A : Ty Δ l}{σ : Tms Θ Δ}{δ : Tms Γ Θ} → (A [ σ ]T [ δ ]T) ≡ (A [ σ ∘ δ ]T)
idl   : ∀{i}{Γ : Con i}{j}{Δ : Con j}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ
idr   : ∀{i}{Γ : Con i}{j}{Δ : Con j}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ
ass   : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Ψ : Con k}{l}{Δ : Con l}{σ : Tms Ψ Δ}{δ : Tms Θ Ψ}{ν : Tms Γ Θ} →
  ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
,∘    : ∀{i}{Γ : Con i}{j}{Θ : Con j}{k}{Δ : Con k}{σ : Tms Θ Δ}{δ : Tms Γ Θ}{l}{A : Ty Δ l}{t : Tm Θ (A [ σ ]T)} →
  (_,_ σ {A = A} t) ∘ δ ≡ _,_ (σ ∘ δ) {A = A} (transport (Tm Γ) ([][]T {A = A}{σ = σ}{δ}) (t [ δ ]t))
π₁β   : ∀{i}{Γ : Con i}{j}{Δ : Con j}{δ : Tms Γ Δ}{k}{A : Ty Δ k}{a : Tm Γ (A [ δ ]T)} → (π₁ {A = A}(_,_ δ {A = A} a)) ≡ δ
πη    : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}{δ : Tms Γ (Δ ▷ A)} → (_,_ (π₁ {A = A} δ) {A = A} (π₂ {A = A} δ)) ≡ δ
εη    : ∀{i}{Γ : Con i}{σ : Tms Γ •} → σ ≡ ε
π₂β   : ∀{i}{Γ : Con i}{j}{Δ : Con j}{δ : Tms Γ Δ}{k}{A : Ty Δ k}{a : Tm Γ (A [ δ ]T)} → (π₂ {A = A}(_,_ δ {A = A} a)) ≡ a

open import lib.funext

[id]T {A = A} = Ty=' refl
  (funexti (λ γ → funext λ α → ~p A _ _))
  (funexti λ γ → funexti λ γ' → funexti λ γ~ → funexti λ α → funexti λ α' → funext λ α~ → ~p A _ _)
  (funexti λ γ → funexti λ γ' → funexti λ γ'' → funexti λ γ~ → funexti λ γ~' → funexti λ α → funexti λ α' → funexti λ α'' → funext λ α~ → funext λ α~' → ~p A _ _)
[][]T {A = A}{σ = σ}{δ = δ} = Ty=' refl
  (funexti (λ γ → funext λ α → ~p A _ _))
  (funexti λ γ → funexti λ γ' → funexti λ γ~ → funexti λ α → funexti λ α' → funext λ α~ → ~p A _ _)
  (funexti λ γ → funexti λ γ' → funexti λ γ'' → funexti λ γ~ → funexti λ γ~' → funexti λ α → funexti λ α' → funexti λ α'' → funext λ α~ → funext λ α~' → ~p A _ _)
idl   = refl
idr   = refl
ass   = refl
,∘ {Γ = Γ}{σ = σ}{δ}{A = A}{t} = Tms=' (funext λ γ → ap (λ z → ∣ σ ∣ (∣ δ ∣ γ) ,Σ z) {!!}) {!!}
-- Tms= (ap (λ z γ → ∣ σ ∣ (∣ δ ∣ γ) ,Σ z γ) (∣tr∣t (apap (~p A) ⁻¹) {t = t [ δ ]t}) ⁻¹)
π₁β   = refl
πη    = refl
εη    = refl
π₂β   = refl

[id]T' : ∀{i j} → _≡_ {A = (Γ : Con i)(A : Ty Γ j) → Ty Γ j} (λ Γ A → A [ id ]T) λ Γ A → A
[id]T' {i}{j} = {!!}

wk : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tms (Γ ▷ A) Γ
wk {A = A} = π₁ {A = A} id

vz : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ wk {A = A} ]T)
vz {A = A} = π₂ {A = A} id

vs : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{k}{B : Ty Γ k} → Tm Γ A → Tm (Γ ▷ B) (A [ wk {A = B} ]T) 
vs {B = B} x = x [ wk {A = B} ]t

<_> : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm Γ A → Tms Γ (Γ ▷ A)
<_> {i}{Γ}{j}{A} t = _,_ id {A = A} (transport (Tm Γ) ([id]T {A = A} ⁻¹) t)

infix 4 <_>

_^_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Tms Γ Δ){k}(A : Ty Δ k) → Tms (Γ ▷ A [ σ ]T) (Δ ▷ A)
_^_ {Γ = Γ}{Δ = Δ} σ {b} A = _,_ (σ ∘ wk {A = A [ σ ]T}) {A = A} (transport (Tm (Γ ▷ A [ σ ]T)) ([][]T {A = A}{σ = σ}{δ = wk {Γ = Γ}{A = A [ σ ]T}}) (vz {A = A [ σ ]T}))

infixl 5 _^_

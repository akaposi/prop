{-# OPTIONS --without-K #-}

module lib.funext where

open import lib

module _
  {ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{f g : (x : A) → B x}
  where

  fun-elim : (w : f ≡ g) → ((x : A) → f x ≡ g x)
  fun-elim w = λ x → ap (λ z → z x) w

  postulate
    funext : (w : (x : A) → f x ≡ g x) → f ≡ g
    fun-β  : (w : (x : A) → f x ≡ g x) → fun-elim (funext w) ≡ w
    fun-η  : (w : f ≡ g)               → funext (fun-elim w) ≡ w

funext-refl
  : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{f : (x : A) → B x}
  → funext {ℓ}{ℓ'}{A}{B}{f}{f}(λ x → refl) ≡ refl
funext-refl {A = A}{B}{f} = fun-η refl

module _
  {ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}
  where

  toi : (f : (x : A) → B x) → ({x : A} → B x)
  toi f {x} = f x

  fromi : (f : {x : A} → B x) → ((x : A) → B x)
  fromi f x = f {x}

  module _
    {f g : {x : A} → B x}
    where

    fun-elimi : (w : (λ {x} → f {x}) ≡ g) → ((x : A) → f {x} ≡ g {x})
    fun-elimi w = fun-elim (ap fromi w)

    funexti : ((x : A) → f {x} ≡ g {x}) → _≡_ {A = {x : A} → B x} f g
    funexti p = ap toi (funext {f = fromi f}{fromi g} p)

    fun-βi : (w : (x : A) → f {x} ≡ g {x}) → fun-elimi (funexti w) ≡ w
    fun-βi w = ap (λ z → fun-elim z) (apap {f = toi}{g = fromi}(funext w) ⁻¹)
             ◾ ap fun-elim (apid (funext w))
             ◾ fun-β w

funexti-refl
  : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{f : {x : A} → B x}
  → funexti {ℓ}{ℓ'}{A}{B}{f}{f}(λ x → refl) ≡ refl
funexti-refl {f = f} = ap (ap (λ z {x} → z x)) (funext-refl {f = λ x → f {x}})

→=
  : ∀{ℓ ℓ'}
    {A₀ A₁ : Set ℓ}(A₂ : A₀ ≡ A₁)
    {B₀ : A₀ → Set ℓ'}{B₁ : A₁ → Set ℓ'}(B₂ : {x₀ : A₀}{x₁ : A₁}(x₂ : x₀ ≡[ A₂ ]≡ x₁) → B₀ x₀ ≡ B₁ x₁)
  → ((x₀ : A₀) → B₀ x₀) ≡ ((x₁ : A₁) → B₁ x₁)
→= {ℓ}{ℓ'}{A} refl {B₀}{B₁} B₂
  = J {A = A → Set ℓ'}{B₀}
      (λ {B₁} B₂ → ((x₀ : A) → B₀ x₀) ≡ ((x₁ : A) → B₁ x₁))
      refl
      {B₁} (funext (λ x → B₂ {x} refl))

fun-2 : {A : Set}{B : A → Set}{f : (x : A) → B x}
      → ((x : A)(p : f x ≡ f x) → p ≡ refl) → (q : f ≡ f) → q ≡ refl
fun-2 w q = coe (ap (λ x → x ≡ refl) (fun-η q))
                (coe (ap (λ x → funext (fun-elim q) ≡ x)
                         (fun-η refl))
                         (ap funext (funext (λ x → w x (fun-elim q x)))))

setCodomain : {A : Set}{B : A → Set}(w : {a : A}{b : B a}{α : b ≡ b} → α ≡ refl)
            → {f g : (x : A) → B x}{α β : f ≡ g} → α ≡ β
setCodomain w {α = α}{refl} = fun-2 (λ x p → w {α = p}) α

module _ {ℓ ℓ' ℓ''}{A : Set ℓ}{B : A → Set ℓ'}{C : {x : A} → B x → Set ℓ''} where

  curry : ((w : Σ A B) → C (proj₂ w)) → ({x : A}(y : B x) → C y)
  curry f {x} y = f (x ,Σ y)

  uncurry : ({x : A}(y : B x) → C y) → ((w : Σ A B) → C (proj₂ w))
  uncurry f (x ,Σ y) = f {x} y

  curryuncurry : (f : {x : A}(y : B x) → C y) → (λ {x} → curry (uncurry f) {x}) ≡ f
  curryuncurry f = funexti λ x → funext λ y → refl

  uncurrycurry : (f : (w : Σ A B) → C (proj₂ w)) → uncurry (curry f) ≡ f
  uncurrycurry f = funext λ { (x ,Σ y) → refl }
